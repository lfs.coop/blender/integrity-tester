import bpy
import json
import requests

# Ajouter une enum pour déterminer si on pick depuis un file ou le JSON en ligne
# Pour la clé en ligne, l'URL est dans les préférences et une enum déroulante permet de choisir une des clés du fichier

keys_found = []
ignored_tests = {}

def flush_keys():
	global keys_found
	keys_found.clear()
	keys_found = [("NONE", "No key", "Don't use a key")]

def get_keys(self, context):
	global keys_found
	return keys_found

def get_ignored_tests():
	global ignored_tests
	return ignored_tests

def fetch_url():
	addon_prefs = bpy.context.preferences.addons[__name__.split('.')[0]].preferences
	try:
		url = addon_prefs.url
		if "/raw/" not in url:
			print("ERROR. The URL doesn't point to a raw file.")
		content = requests.get(url, allow_redirects=True).text
	except:
		print("error")
		return "{}"
	else:
		return content


def extract_keys(raw):
	try:
		full = json.loads(raw)
	except:
		print("error extract")
		return {}

	valid = {}

	for key, item in full.items():
		if type(item) != type([]):
			print('skipped')
			continue

		if key in valid.keys():
			print('skipped')
			continue

		valid[key] = item

	return valid


def get_from_url():
	global ignored_tests
	global keys_found
	content = fetch_url()
	keys = extract_keys(content)
	ignored_tests = keys
	keys_found = to_items(keys)


def to_items(keys):
	items = []
	items.append(("NONE", "No key", "Don't use a key"))

	for key, item in keys.items():
		items.append((key, key.capitalize(), "Skip the test {}".format(key)))

	return items


def get_from_file():
	global keys_found
	global ignored_tests
	addon_prefs = bpy.context.preferences.addons[__name__.split('.')[0]].preferences
	try:
		file_path = addon_prefs.filepath
		file = open(file_path, 'r')
		content = file.read()
		file.close()
	except:
		print("error get file: {}".format(file_path))
		keys_found = [("NONE", "No key", "Don't use a key")]
	else:
		keys = extract_keys(content)
		ignored_tests = keys
		keys_found = to_items(keys)