import re

special_rotations = {
    'c_neck.x': 'ZXY',
    'c_head.x': 'ZXY',
    'c_root_master.x': 'ZXY',
    'c_traj': 'YZX',
    'c_pos': 'YZX',
    'c_foot_ik.SIDE': 'YZX', 
    'c_thigh_fk.SIDE': 'YZX',
    'c_shoulder.SIDE': 'YZX',
    'c_arm_fk.SIDE': 'YZX',
    'c_hand_fk.SIDE': 'ZYX', 
    'c_hand_rot_ik.SIDE': 'ZYX',
    'c_spine_0[1,6].x': 'YZX',
    'c_spine_fk_bend.x': 'YZX',
    'c_root.x': 'XYZ'
}

sides = ['l', 'r']

def parse_side(dico):
    dico2 = {}
    for key, item in dico.items():
        if 'SIDE' in key:
            pieces = key.split('SIDE')
            for side in sides:
                key2 = side.join(pieces)
                dico2[key2] = item
        else:
            dico2[key] = item
            
    return dico2
            
def parse_ranges(dico):
    dico2 = {}
    for key, item in dico.items():
        res = re.search("(.+)\[([0-9]+),([0-9]+)\](.*)", key)
        if res is not None:
            base = res[1]
            start = int(res[2])
            end = int(res[3])
            cap = res[4]
            for i in range(start, end+1):
                dico2[base + str(i) + cap] = item
        else:
            dico2[key] = item
            
    return dico2

def build_dict(dico):
    dico = parse_side(dico)
    dico = parse_ranges(dico)
    return dico

special_rotations = build_dict(special_rotations)