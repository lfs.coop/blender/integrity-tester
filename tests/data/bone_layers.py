layer_bones = {
    0:["c_root.x", "c_root_master.x", "c_spine_01.x","c_spine_02.x", "c_spine_03.x","c_spine_04.x","c_spine_05.x", "c_shoulder.l", "c_shoulder.r", "c_neck.x", "c_head.x"],
    1:["c_stretch_arm_pin.l", "c_stretch_arm.l","c_stretch_arm_pin.r", "c_stretch_arm.r", "c_stretch_leg_pin.l", "c_stretch_leg.l","c_stretch_leg_pin.r", "c_stretch_leg.r", "c_thigh_b.l", "c_thigh_b.r"],
    3:["c_hand_ik.r", "c_arms_pole.r", "c_arm_ik.r"],
    4:["c_hand_fk.r", "c_forearm_fk.r", "c_arm_fk.r"],
    5:["c_hand_ik.l", "c_arms_pole.l", "c_arm_ik.l"],
    6:["c_hand_fk.l", "c_forearm_fk.l", "c_arm_fk.l"],
    16:["c_pos"],
    17:["c_fly", "c_traj"],
    20:["c_foot_ik.r", "c_leg_pole.r", "c_thigh_ik.r"],
    19:["c_foot_fk.r", "c_leg_fk.r", "c_thigh_fk.r"],
    22:["c_foot_ik.l", "c_leg_pole.l", "c_thigh_ik.l"],
    21:["c_foot_fk.l", "c_leg_fk.l", "c_thigh_fk.l"],
    23:["c_jawbone.x","c_lips_roll_bot.x", "c_lips_roll_top.x", "c_teeth_bot_master.x", "c_teeth_top_master.x","c_lips_corner_mini.l", "c_lips_smile.l", "c_lips_top_01.l","c_lips_top.l", "c_lips_top.x", 
    "c_lips_corner_mini.r", "c_lips_smile.r", "c_lips_top_01.r","c_lips_top.r", "c_lips_bot_01.l","c_lips_bot.l", "c_lips_bot_01.r","c_lips_bot.r","c_lips_bot.x", 
    "c_cheek_smile.l", "c_cheek_smile.r", "c_teeth_bot_master.x", "c_teeth_top_master.x", "c_tong_01.x", "c_tong_02.x", "c_tong_03.x","c_ear_01.l", "c_ear_02.l", "c_ear_01.r", "c_ear_02.r",
    "c_eyebrow_03.l", "c_eyebrow_02.l", "c_eyebrow_01.l", "c_eyebrow_01_end.l", "c_eyebrow_03.r", "c_eyebrow_02.r", "c_eyebrow_01.r", "c_eyebrow_01_end.r", "c_eyebrow_full.l", "c_eyebrow_full.r",
    "c_eyelid_bot.l", "c_eyelid_top.l", "c_eyelid_bot.l", "c_eyelid_top.r" ]
}