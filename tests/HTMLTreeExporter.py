from .sqarf.session import Session
from .sqarf.html_tree_export import test_to_html_tree, html_tree, HtmlTreeExporter
from .sqarf.exporter import _BasicExporter
import bpy
import getpass

version = (1, 67)

BASE = """
<!DOCTYPE html>

<html>
    <head>
        <!-- icons from https://remixicon.com/ -->
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
        <link rel="icon" href="https://upload.wikimedia.org/wikipedia/commons/0/0c/Blender_logo_no_text.svg">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Mono">
        <title>{0}</title>
        <div id="version-bl" style="background-color: {3};">{1}</div>
        <div id="user" style="display: none;">{2}</div>
        <script type="text/javascript" language="javascript">

            see_passed = true;

            function resetColor(){{
                var button = document.getElementById("button_copy");
                button.style.transition = 'background-color 0.8s'; 
                button.style.transition = 'color 0.8s'; 
                button.style.transition = 'border 0.8s';

                button.style.backgroundColor = 'rgba(255, 255, 255, 0.7)';
                button.style.color = 'black';
                button.style.border = 'solid 4px rgba(0, 0, 0, 0.7)';
            }}

            function copyFunction() {{
                var copyText = document.getElementById("asset_name").innerHTML;
                var button = document.getElementById("button_copy");
                
                var el = document.createElement('textarea');
                el.value = copyText;
                el.innerText = copyText;
                el.setAttribute('readonly', '');
                el.style = {{position: 'absolute', left: '-9999px'}};
                document.body.appendChild(el);

                el.select();

                document.execCommand("copy");

                button.style.transition = 'background-color 0.0s';
                button.style.transition = 'color 0.0s';
                button.style.transition = 'border 0.0s';

                button.style.backgroundColor = '#9fedac';
                button.style.color = '#2d8a43';
                button.style.border = 'solid 4px #2d8a43';

                setTimeout(resetColor, 50)

                document.body.removeChild(el);
            }}

            function onlyFailed(){{
                see_passed = !see_passed;

                passed = document.getElementsByClassName('PASSED');
                //key_passed = document.getElementsByClassName('KEY_PASSED');
                subs = document.getElementsByClassName('sub');
                button = document.getElementById('button_view');
                uls = document.getElementsByTagName('ul');

                tree = document.getElementsByClassName('tree')[0]

                for(i = 0 ; i < passed.length ; i++){{
                    if(see_passed){{
                        passed[i].style.display = "unset";
                    }}
                    else{{
                        passed[i].style.display = "none";
                    }}
                }}
                /*for(i = 0 ; i < key_passed.length ; i++){{
                    if(see_passed){{
                        key_passed[i].style.display = "unset";
                    }}
                    else{{
                        key_passed[i].style.display = "none";
                    }}
                }}*/
                for(i = 0 ; i < subs.length ; i++){{
                    if(see_passed){{
                        subs[i].style.display = "";
                        subs[i].style.opacity = "0.1";
                        subs[i].style.filter = "blur(1px)";
                    }}
                    else{{
                        subs[i].style.display = "none";
                    }}
                }}
                for(i = 0 ; i < uls.length ; i++){{
                    if(see_passed){{
                        uls[i].style.paddingLeft = "10px";
                    }}
                    else{{
                        uls[i].style.paddingLeft = "0px";
                    }}
                }}

                if(see_passed){{
                    button.style.backgroundColor = 'rgba(255, 255, 255, 0.7)';
                    button.style.color = 'black';
                    button.style.border = 'solid 4px rgba(0, 0, 0, 0.7)';
                    tree.style.marginLeft = "-10px";
                }}
                else{{
                    button.style.backgroundColor = '#ff9991';
                    button.style.color = '#a63838';
                    button.style.border = 'solid 4px #a63838';
                    tree.style.marginLeft = "0px";
                }}

            }}

        </script>
"""

BASE_CSS = """
<style>
    html, body{
        margin: 0; padding: 0;
        text-align: center;
        /*background-color: #efefef;*/
        background-color: #202226;
        color: #202226;
        -ms-overflow-style: none;  /* IE and Edge */
        scrollbar-width: none;  /* Firefox */
    }
    html::-webkit-scrollbar {
        display: none;
    }
    .sub{
        opacity: 0.1;
        filter: blur(1px);
    }
    .sub:hover{
        opacity: 1.0;
        filter: none;
    }
    .bloc{
        /*margin: 2px;
        border-radius: 2px;*/
        text-align: left;
    }
    .IRRELEVANT{
        display: none;
    }
    .key_passed-bckg{
        background-color: #e2bdff;
    }
    .prerequisites-bckg, .failed-bckg{
        background-color: #ffa6a6;
    }
    .passed-bckg{
        background-color: #9fedac;
    }
    .fixed-bckg{
        background-color: #9fd8ed;
    }
    .error-bckg{
        background-color: red;
    }
    .irrelevant-bckg{
        background-color: #202226;
    }
    .title-bar{
    }
    .icon{
        font-size: 35px;
        vertical-align: middle;
    }
    .ri-spam-2-fill{
        color: #a63838;
    }
    .ri-settings-5-fill{
        color: #a63838;
    }
    .ri-check-double-fill{
        color: #2d8a43;
    }
    .ri-error-warning-line{
        color: #3874a6;
    }
    .ri-key-fill{
        color: #705585;
    }
    .ri-bug-fill{
        color: cyan;
    }
    .title{
        vertical-align: middle;
        font-size: 25px;
        margin-left: 5px;
        font-family: "Roboto Slab", serif;
        letter-spacing: 0.03em;
    }
    .prerequisites-title, .failed-title{
        color: #a63838;
    }
    .passed-title{
        color: #2d8a43;
    }
    .key_passed-title{
        color: #705585;
    }
    .fixed-title{
        color: #3874a6;
    }
    .error-title{
        color: cyan;
    }
    .irrelevant-title{
        color: #3d3d3d;
    }
    .ri-skip-forward-line{
        color: #3d3d3d;
    }
    .status{
        margin-left: 30px;
        margin-top: 5px;
        margin-bottom: 5px;
        display: block;
        color: #363636;
    }
    summary{
        outline: none;
        padding: 4px;
        padding-left: 15px;
        cursor: pointer;
        -webkit-user-select: none; /* Safari */        
        -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* IE10+/Edge */
        user-select: none; /* Standard */
    }
    details{
        color: #383838b4;
        font-family: "Open Sans";
        font-size: 1.2em;
    }
    .description{
        vertical-align: middle;
        font-size: 18px;
        font-weight: 100;
        font-family: "Open Sans";
        color: #00000099;
        letter-spacing: 0.01em;
        left: 50%;
        margin-top: 5px;
        transform: translateX(-50%);
        position: absolute;
    }
    .code{
        font-family: "Roboto Mono";
        background-color: #ffffff7d;
        border-radius: 4px;
        padding-left: 3px;
        padding-right: 3px;
        padding-top: 0px;
        padding-bottom: 0px;
        font-size: 1.0em;
    }
    #version-bl{
        font-size: 1.3em;
        position: relative;
        height: 1.6em;
        padding-top: 6px;
        /*margin-bottom: 3px;*/
        color: black;
        font-family: "Roboto Slab";
        font-weight: bold;
    }
    .result{
        background-color: #ffffff64;
        padding: 5px;
        /*margin-left: -10px;*/
    }
    .header{
        /*margin-top: 8px;*/
        margin-bottom: 20px;
        padding: 15px;
        border-bottom: solid 3px white;
        font-family: "Open Sans";
        font-size: 1.2em;
        background-color: #0073ba;
        background-image: url("https://gitlab.com/lfs.coop/blender/integrity-tester/-/raw/master/misc/stripes.png");
    }
    ul{
        list-style: none;
        padding-left: 10px;
    }
    .tree{
        margin-left: -10px;
    }
    .ri-time-line{
        vertical-align: middle;
    }
    .ri-alert-fill{
      color: #805d03;
    }
    .warning-title{
      color: #805d03;
    }
    .warning-bckg{
      background-color: #f2c54e;
    }
    .skipped-bckg{
        /*display: none;*/
    }
    #button_copy{
        postion: relative;
        width: 45px;
        height: 45px;
        font-size: 25px;
        cursor: pointer;
        padding: 6px;
        border-radius: 30px;
        border: solid 4px rgba(0, 0, 0, 0.7);
        background-color: rgba(255, 255, 255, 0.7);
        transition: background-color .5s;
        text-align: center;
        vertical-align: middle;
        outline: none;

    }

    #button_view{
        postion: relative;
        width: 45px;
        height: 45px;
        font-size: 25px;
        cursor: pointer;
        padding: 6px;
        border-radius: 30px;
        border: solid 4px rgba(0, 0, 0, 0.7);
        background-color: rgba(255, 255, 255, 0.7);
        transition: background-color .5s;
        transition: color .5s;
        transition: border .5s;
        text-align: center;
        vertical-align: middle;
        outline: none;
    }


    
</style>
</head>
<body>
"""

END_PAGE = """
    <!--<img style="display: none; height: 60px; width: auto;" src="https://lh3.googleusercontent.com/proxy/93bSLLWOcOO2qprEFi6kbEgrD5Q0m889UU0sYg8TkK70Bn-PZ8ly9PhwBIbAfWagRRa0dLMHArd6ESdjq_YlyaWgStfAeq9yEAJAl3dGHn_BsdQBRRSNmGxUOibiGoES9ro5QwVreBTE06RbIihChcxs"></img>-->
    </body>
    <script type="text/javascript" language="javascript">
        onlyFailed();
    </script>
</html>
"""

def status_to_icon(status):
    if status == 'FAILED':
        return "ri-spam-2-fill"
    elif status == 'PREREQUISITES':
        return "ri-spam-2-fill"
    elif status == 'PASSED':
        return "ri-check-double-fill"
    elif status == 'FIXED':
        return "ri-error-warning-line"
    elif status == 'ERROR':
        return "ri-bug-fill"
    elif status == 'IRRELEVANT':
        return "ri-skip-forward-line"
    elif status == "WARNING":
        return "ri-alert-fill"
    elif status == "KEY_PASSED":
        return "ri-key-fill"
    elif status == "FIX FAILED":
        return "ri-settings-5-fill"
    else:
        return ""

def reformat(r):
    if len(r) > 1:
        temp = r.strip()
        #temp = "∙ " + temp
        temp = temp.replace('{', '<b>')
        temp = temp.replace('}', '</b>')
        temp = temp.replace('[', '<span class="code">')
        temp = temp.replace(']', '</span>')
        temp = '<span class="status">' + temp
        temp = temp + '</span>'
        return temp
    else:
        return None

def format_title(r):
    if r and (len(r) > 1):
        temp = r.strip()
        temp = temp.replace('{', '<b>')
        temp = temp.replace('}', '</b>')
        temp = temp.replace('[', '<span class="code">')
        temp = temp.replace(']', '</span>')
        return temp
    else:
        return None

def format_summary(summary):
    formatted = summary.split('*')
    result = []

    for r in formatted:
        temp = reformat(r)
        if temp:
            result.append(temp)
    return result

def detect_warning(summary):
    if summary.startswith("<warning>"):
        return summary.replace("<warning>", ""), True
    else:
        return summary, False

def test_to_html_tree(test, root_timestamp):
    """
    Returns the html table-tree for the given test json export.
    """
    lines = []
    test_name = test["test_name"]
    short = test["short_description"]
    long = test["long_description"]
    start_time = test["timestamp"] - root_timestamp
    run_time = test["result"]["timestamp"] - test["timestamp"]
    result = test["result"]
    status = result["status"]
    summary = result["summary"] or ""
    error = result["error"]
    trace = result["trace"]
    flags = result["flags"]
    sub = "not"

    if summary == "Sub Test Failed":
        sub = "sub"
    #summary, warning = detect_warning(summary)

    if flags is not None:
        if 'WARNING' in flags:
            status = "WARNING"

    # The recursive call is in charge of the new <ul>
    lines = []
    lines.append('<li id="{}" class="{} {}">'.format(test_name, status, sub))
    lines.append('<div class="bloc {}-bckg">'.format(status.lower()))
    lines.append('<details> <summary>')
    lines.append('<i class="icon {}"></i>'.format(status_to_icon(status)))
    lines.append('<span class="title {}-title">{}</span>'.format(status.lower(), test_name or ""))
    lines.append('<span class="description">{}</span>'.format(format_title(short) or "-"))
    lines.append('</summary> <div class="result">')

    if error:
        lines.append(str(error))
        lines.append(str(trace))
    else:
        lines.extend(format_summary(summary))

    lines.append("</div></details></div></li>")
    lines.append('</li>')

    # sub tests:
    for sub_test in test["sub_tests"]:
        lines.append("<ul>")
        lines.extend(test_to_html_tree(sub_test, root_timestamp))
        lines.append("</ul>")

    return lines


def html_tree(session_dict_list, config):
    global version
    lines = []
    color = ""
    color_icon = ""

    if session_dict_list[0]["result"]["status"] == 'PASSED':
        color = "#1db534"
        color_icon = "#1db534"
    else:
        color = "#c74234"
        color_icon = "#c74234"

    #print('CONTEXT: ', session_dict_list[0]['context'])

    lines.append(BASE.format(config['name'], str(version).replace(',', '.').replace(' ', '').replace('(', '').replace(')', ''), getpass.getuser(), color))
    lines.append(BASE_CSS)
    for run in session_dict_list:
        title = """
        <div class="header">
            <h1>Scene Sanity Check</h1>
            <h2 style="font-weight: 100;">{1}: <b id="asset_name">{0}</b> - <span style="font-weight: 100; font-style: italic;">Version {2}</span> </h2>
            <h2 style="vertical-align: middle;"><i style="position: relative; font-size: 1.2em; vertical-align: middle; color: {4}; margin-right: 5px;" class="ri-bookmark-fill"></i><span>{3}</span></h2>
            
            <button title="Copy asset name to clipboard" id="button_copy" onclick="copyFunction()">
                <i class="ri-file-copy-2-fill"></i>
            </button>

            <button title="Hide successful tests" id="button_view" onclick="onlyFailed()">
                <i class="ri-eye-fill"></i>
            </button>
        </div>
        """.format(config["name"], config["category"].capitalize(), config['version'], run['context']['phase'], color_icon)
        lines.append(title)
        lines.append('<ul class="tree">')
        lines.extend(test_to_html_tree(run, run["timestamp"]))
        lines.append("</ul>")
    lines.append(END_PAGE)
    lines = "\n".join(lines)
    return lines



@Session.register_exporter
class HtmlTreeExporterCustom(_BasicExporter):

    EXPORTER_NAME = "HTMLTreeCustom"

    def get_export_content(self, session_dict_list):
        return html_tree(session_dict_list, self.config)
