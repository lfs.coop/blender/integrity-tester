import bpy
from ..sqarf.qatest import QATest
import gazu

def test_shading_done():

    class TestShadingDone(QATest):

        """ Is the shading [DONE] or [VALID] for this asset ? """

        assets = []
        name_assets = []
        shading = None
        done = None
        valid = None

        def relevant_for(self, context):
            if not context['kitsu']['login']:
                return False, "Impossible to perform test without Kitsu"
            else:
                return True, "Tests with Kitsu"


        def build_context(self, context):
            self.assets = context['assets']

            for asset in self.assets:
                self.name_assets.append(asset['object'].name.split('.')[0].lower().replace(' ', '_').replace("_proxy", ""))

            self.name_assets = set(self.name_assets)

            self.shading = gazu.task.get_task_type_by_name("shading")
            self.done = gazu.task.get_task_status_by_short_name("done")
            self.valid = gazu.task.get_task_status_by_short_name("VALID")

            if (self.valid is None):
                return False, "Failed to fetch VALID property from Kitsu" 

            if (self.done is None):
                return False, "Failed to fetch DONE property from Kitsu" 

            if (self.shading is None):
                return False, "Failed to fetch SHADING property from Kitsu"

            return True, "Context built"


        def test(self, context):
            errors = set()
            fails = set()

            for asset_name in self.name_assets:
                asset = gazu.asset.get_asset_by_name(context['prod_id'], asset_name)
                if asset is None:
                    fails.add("[{}]".format(asset_name))
                    continue

                task = gazu.task.get_task_by_name(asset, self.shading)
                if task is None:
                    fails.add("[{}]".format(asset_name))
                    continue

                is_done = (task['task_status_id'] == self.done['id']) or (task['task_status_id'] == self.valid['id'])

                if not is_done:
                    errors.add("[{}]".format(asset_name))

            if len(errors) > 0:
                return False, "Some assets shading status' is not VALID or DONE: {}".format(", ".join(errors))
            elif len(fails) > 0:
                self.raise_flag('warning')
                return False, "Failed to check shading status of some assets: {}".format(", ".join(fails))
            else:
                return True, "All assets have their shading VALID or DONE"


    return [TestShadingDone]