import bpy
from ..sqarf.qatest import QATest

from .detect_versions import test_last_version
from .test_shading_done import test_shading_done
from .test_casting import test_casting

def test_assets():

    class TestUnitScale(QATest):

        """ Is the scale of every armature equals to [(1.0, 1.0, 1.0)] """

        assets = []

        def build_context(self, context):
            self.assets = context['assets']
            return True, "Context built"

        def is_exception(self, name):
            if name.startswith('cs_') or name.startswith('lib_sets'):
                return True

            if name in ['label_t', '_char_name', 'picker_background', 'label_']:
                return True

            return False

        def test(self, context):
            errors = set()
            for asset in self.assets:
                armature = asset['armature']
                if armature is not None:
                    if tuple(armature.scale) != (1.0, 1.0, 1.0):
                        errors.add("[{}]".format(armature.name))

            if len(errors) > 0:
                return False, "Some armatures have a scale different than [(1.0, 1.0, 1.0)]: {}".format(", ".join(errors))
            else:
                return True, "All armatures have a correct scale"

    class TestAssetLastVersion(QATest):

        """ Is each asset to its last version in the scene """

        assets = []

        def necessary(self):
            return True

        def get_sub_test_types(self):
            return []

        def build_context(self, context):
            self.assets = context['assets']
            return True, "Context built"

        def test(self, context):
            errors = test_last_version(self.assets)
            
            if len(errors['updates']) > 0:
                return False, "Following assets are not up to date: {}".format(", ".join(["[{}]".format(item) for item in errors['updates']]))

            elif len(errors['warnings']) > 0:
                self.raise_flag('warning')
                return True, "Check of version has failed for some assets: {}".format(", ".join(["[{}]".format(item) for item in errors['warnings']]))

            elif len(errors['paths']) > 0:
                self.raise_flag('warning')
                return True, "Check of version has failed for some assets: {}".format(", ".join(["[{}]".format(item) for item in errors['paths']]))

            else:
                return True, "All assets are up to date"


    class TestAssets(QATest):

        """ Pool of tests to perform on assets """

        def get_sub_test_types(self):
           return test_casting() + [TestAssetLastVersion, TestUnitScale] + test_shading_done()


    return [TestAssets]