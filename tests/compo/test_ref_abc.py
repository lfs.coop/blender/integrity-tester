import bpy
from ..sqarf.qatest import QATest


def test_ref_abc():

    class TestAlambicGhosts(QATest):

        """ Do all assets have an alambic ghost? """

        visibility = False

        def can_fix(self, context):
            return self.visibility, "Not necessary fixable"

        def fix(self, context):
            ref = bpy.context.scene.collection.children.get('Ref')
            vl_ref = bpy.context.scene.view_layers[0].layer_collection.children.get('Ref')

            vl_ref.exclude    = False
            ref.hide_select   = True
            ref.hide_viewport = False
            ref.hide_render   = True

            return True, "Visibility of the [Ref] collection was fixed"

        def test(self, context):
            ref = bpy.context.scene.collection.children.get('Ref')
            vl_ref = bpy.context.scene.view_layers[0].layer_collection.children.get('Ref')

            if ref is None:
                return False, "Alambic ghosts don't have been built"

            chars = bpy.context.scene.collection.children.get('Chars')
            props = bpy.context.scene.collection.children.get('Props')

            if (chars is not None) or (props is not None):
                if len(ref.all_objects) > 0:
                    if vl_ref.exclude or (not ref.hide_select) or ref.hide_viewport or (not ref.hide_render):
                        self.visibility = True
                        return False, "The [Ref] collection is hidden"
                    return True, "Alambic ghosts have been built"
                else:
                    return False, "Alambic ghosts don't have been built"
            else:
                return True, "Alambic ghosts are not required for this shot"


    return [TestAlambicGhosts]