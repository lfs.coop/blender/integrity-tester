import bpy
import os
import re

def locate(path):
    path = path.replace('\\', '/')
    path = bpy.path.abspath(path)
    splitted_path = path.split('/')

    try:
        index_lib = splitted_path.index('lib')
    except:
        return None

    return splitted_path


def gather_assets():
    # Inside build_context
    categories = ('Chars', 'Props')
    collections = [item for item in [bpy.data.collections.get(c) for c in categories] if item is not None]
    assets = []
    errors = []

    for collection in collections:
        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
        #    RETREIVING PROXIES                                             #
        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
        proxy_dict = {}
        for obj in collection.objects:
            if obj.type == 'ARMATURE':
                if obj.data.library is None:
                    errors.append(obj)
                else:
                    proxy_dict.setdefault(obj.data.library, {'armature': None, 'object': None, 'name': None})
                    proxy_dict[obj.data.library]['armature'] = obj
            else:
                if (obj.type != 'EMPTY') or (obj.instance_collection is None) or (obj.instance_collection.library is None):
                    errors.append(obj)
                else:
                    proxy_dict.setdefault(obj.instance_collection.library, {'armature': None, 'object': None, 'name': None})
                    proxy_dict[obj.instance_collection.library]['object'] = obj
                    proxy_dict[obj.instance_collection.library]['name'] = obj.name.split('.')[0]

        for lib, objects in proxy_dict.items():
            assets.append({
                'type': 'PROXY',
                'armature': objects['armature'],
                'object': objects['object'],
                'name': objects['name'],
                'category': collection.name,
                'data': objects['object'].instance_collection,
                'library': objects['object'].instance_collection.library
            })
            
            
        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
        #    RETREIVING OVERRIDES                                           #
        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
        for child in collection.children:
            if (child.override_library is None) or (child.override_library.reference is None):
                errors.append(child)
            else:
                rig = [c for c in child.children if (c.name.split('.')[0].endswith('_rig'))]
                if len(rig) != 1:
                    errors.append(child)
                    continue
                arma = rig[0].objects[0]
                if arma.type != 'ARMATURE':
                    errors.append(child)
                    continue
                assets.append({
                    'type': 'OVERRIDE',
                    'armature': arma,
                    'object': child,
                    'name': child.name.split('.')[0],
                    'category': collection.name,
                    'data': child.override_library.reference,
                    'library': child.override_library.reference.library
                })
    
    if len(errors) > 0:
        print('Some errors occured on objects:', ', '.join([f"[{e.name}]" for e in errors]))    
    
    return assets


def check_last_version(path_items):
    new_path = path_items.copy()
    new_path.pop()
    new_path.pop()

    dirs = [d for d in os.listdir("/".join(new_path)) if (os.path.isdir("/".join(new_path)+'/'+d)) and (d.startswith('v')) and (d[1:].isdigit())]
    dirs.sort()

    new_path.append(dirs[-1])

    try:
        file = [f for f in os.listdir("/".join(new_path)) if (os.path.isfile("/".join(new_path)+'/'+f)) and (f.endswith('.blend'))][0]
    except:
        return ("EMPTY_NEW_VERSION", None)

    new_path.append(file)
    return ("VERSION_FOUND", new_path)


def latest_version_available(path):
    if not os.path.isdir(path):
        return None
    v = [item for item in os.listdir(path) if re.fullmatch('(v|V)[0-9]{3}', item)]
    v.sort()

    if len(v) == 0:
        return None
    else:
        return v[-1]


def test_last_version(assets):
    results = set()
    warnings = set()
    paths = set()

    for asset in assets:
        path_error = False
        
        try:
            path_items = locate(asset['data'].library.filepath)
        except:
            paths.add("{}".format(asset['object'].name))
            continue

        
        status, new_path = check_last_version(path_items)
        if status != "VERSION_FOUND":
            warnings.add("{}".format(asset['object'].name))
            continue

        if new_path[-2] != path_items[-2]:
            results.add("{}".format(asset['object'].name))
            
    return {'updates': results, 'warnings': warnings, 'paths': paths}
        