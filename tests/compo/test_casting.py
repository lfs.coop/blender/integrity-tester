import bpy
from ..sqarf.qatest import QATest
import gazu

def reformat(input_str):
    output_str = []
    for c in input_str:
        if c.isalpha():
            output_str.append('-')
        if c.isdigit():
            output_str.append('x')
    output_str = "".join(output_str)
    return output_str


def test_casting():

    class TestCompaCasting(QATest):

        """ Is the casting Kitsu similar to this scene content ? """

        sc = ""
        sq = ""
        kitsu_prod_id = ""
        kitsu_sq = None
        kitsu_sc = None
        kitsu_cast = None
        kitsu_assets_names = {}
        assets_names = []

        def relevant_for(self, context):
            if not context['kitsu']['login']:
                return False, "Impossible to perform test without Kitsu"
            else:
                return True, "Tests with Kitsu"
                

        def build_context(self, context):
            self.sq, self.sc = context['name_asset'].split(' ')
            self.kitsu_prod_id = "a56c3c55-7065-42d6-b793-56dc6bf43d59"

            self.kitsu_sq = gazu.shot.get_sequence_by_name(self.kitsu_prod_id, self.sq)
            if self.kitsu_sq is None:
                return False, "Failed to fetch sequence from Kitsu"

            self.kitsu_sc = gazu.shot.get_shot_by_name(self.kitsu_sq, self.sc)
            if self.kitsu_sc is None:
                return False, "Failed to fetch shot from Kitsu"

            self.kitsu_cast = gazu.casting.get_shot_casting(self.kitsu_sc)
            if self.kitsu_cast is None:
                return False, "Failed to fetch cast from Kitsu"

            for asset in self.kitsu_cast:
                asset_data = gazu.asset.get_asset(asset['asset_id'])
                if asset_data is None:
                    return False, "Failed to fetch cast from Kitsu"

                if reformat(asset_data['name']) != "--xx--xxxx":
                    self.kitsu_assets_names[asset_data['name']] = asset['nb_occurences']

            self.assets_names = [o['object'].name for o in context['assets']]

            return True, "Context built"


        def test(self, context):
            assets_scene = {} # Number of occurences by asset in the scene
            assets_kitsu = self.kitsu_assets_names # Number of occurences by asset in the Kitsu cast

            # Counting occurences in the scene
            for asset in self.assets_names:
                key = asset.split('.')[0]
                assets_scene.setdefault(key, 0)
                assets_scene[key] += 1

            missing = set(assets_kitsu.keys()) - set(assets_scene.keys()) # Asset in the cast but not in the scene
            excess = set(assets_scene.keys()) - set(assets_kitsu.keys()) # Asset present in the scene but not in the cast
            bad_nb = []

            for asset_name, nb_occ in assets_scene.items():
                # We don't need to count this asset if it's missing from one side or the other
                if (asset_name in missing) or (asset_name in excess):
                    continue
                else:
                    if nb_occ != assets_kitsu[asset_name]:
                        bad_nb.append("[{}] is supposed to be present <b>{}</b> time(s) in the scene but was found <b>{}</b> time(s)".format(asset_name, assets_kitsu[asset_name], nb_occ))

            msg = ""
            if len(missing) > 0:
                msg += "* <b>-</b> Some assets are in the cast Kitsu but not in the scene: {}".format(", ".join(["[{}]".format(a) for a in missing]))
            if len(excess) > 0:
                msg += "* <b>-</b> Some assets are in the scene but not in the cast Kitsu: {}".format(", ".join(["[{}]".format(a) for a in excess]))
            if len(bad_nb) > 0:
                msg += "* <b>-</b> {}".format(", ".join(bad_nb))

            if (len(missing) > 0) or (len(excess) > 0) or (len(bad_nb) > 0):
                return False, msg
            else:
                return True, "Cast Kitsu matches scene content"

    return [TestCompaCasting]