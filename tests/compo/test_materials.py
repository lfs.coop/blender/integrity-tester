import bpy
from ..sqarf.qatest import QATest

from .detect_versions import gather_assets

def test_materials():

    class TestPolkaDots(QATest):

        """ Is there some Polka dots remaining ? """

        assets = []

        def build_context(self, context):
            self.assets = context['assets']
            return True, "Context built"


        def format_errors(self, errors):
            formated = []
            for mat_name, users in errors.items():
                str_users = ["[{}]".format(usr) for usr in users]
                formated.append("[{}] from {}".format(mat_name, ", ".join(str_users)))
                
            return formated
                
        def test(self, context):
            errors = {}

            for asset in self.assets:
                if asset['type'] == 'OVERRIDE':
                    objects = asset['object'].all_objects
                else:
                    objects = asset['data'].all_objects

                for obj in objects:
                    if not obj.name.startswith('cs_'):
                        for slot in obj.material_slots:
                            m = slot.material
                            if (m is not None) and (m.use_nodes):
                                for node in m.node_tree.nodes:
                                    if "polka" in node.name.lower():
                                        errors.setdefault(m.name, [])
                                        errors[m.name].append(obj.name)

            formated = self.format_errors(errors)

            if len(errors) > 0:
                return False, "Some materials are still using Polka dots: {}".format(", ".join(formated))
            else:
                return True, "No Polka Dots material is in use"



    class TestMaterials(QATest):

        """ Pool of tests to perform on materials """

        def get_sub_test_types(self):
            return [TestPolkaDots]


    return [TestMaterials]