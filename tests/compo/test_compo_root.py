import bpy
from ..sqarf.qatest import QATest
from .test_materials import test_materials
from .test_assets import test_assets
from .test_settings import test_settings
from .detect_versions import gather_assets
from .scene_hierarchy import test_scene_hierarchy
from .test_ref_abc import test_ref_abc

def common_confo():
    return test_scene_hierarchy() + test_assets()

def confo_layout():
    return test_ref_abc()

def confo_animation():
    return test_materials() + test_settings()

def test_compo_root():

    class TestCompoRoot(QATest):
        
        """ Pool of tests to perform on scenes before rendering """

        def necessary(self):
            return True

        def build_context(self, context):
            context['assets'] = gather_assets()
            return True, "Context built"

        def get_sub_test_types(self):
            if bpy.context.scene.integrity_tester.from_scene == 'LAYOUT':
                return common_confo() + confo_layout()
            else:
                return common_confo() + confo_animation()


    return [TestCompoRoot]