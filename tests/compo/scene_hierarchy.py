import bpy
import os
import re
from ..sqarf.qatest import QATest
from .detect_versions import latest_version_available

def clean_abs_path(path):
    path = path.replace('\\', '/')
    path = path.split('/')
    clean = []
    for item in path:
        if item == '..':
            try:
                clean.pop()
            except:
                return None
        else:
            clean.append(item)
    return "/".join(clean)


def max_channel():
    maxi = -1
    for sq in bpy.context.scene.sequence_editor.sequences:
        if sq.channel > maxi:
            maxi = sq.channel
    return maxi


def find_sound(pathaudio, file_name):
    l = [seq for seq in bpy.context.scene.sequence_editor.sequences if (seq.type == 'SOUND') and ('_misc_audio' in seq.name)]
    if len(l) < 1:
        return bpy.context.scene.sequence_editor.sequences.new_sound(
            file_name, 
            pathaudio, 
            max_channel(), 
            101
            )
    else:
        return l[0]

def test_scene_hierarchy():

    class TestStoryboardAudio(QATest):

        """ Are the storyboard and the audio up to date? """

        camera = None
        active = False
        storyboard = None
        frame = True
        path = None
        alert = False
        size = True

        def build_context(self, context):
            self.camera = bpy.context.scene.camera
            if self.camera is None:
                return False, "Failed to build context"
            else:
                return True, "Context built"

        def can_fix(self, context):
            return not self.alert, "Will try to fix sb and audio"

        def fix(self, context):
            msg = ""
            if not self.camera.data.show_background_images:
                msg += f"* Background image was activated on camera [{self.camera.name}]"
                self.camera.data.show_background_images = True

            if len(self.camera.data.background_images) < 1:
                img = self.camera.data.background_images.new()
                img.source = 'MOVIE_CLIP'
                msg += f"* A new background image was created in camera [{self.camera.name}]"
            i = self.camera.data.background_images[0]

            path = clean_abs_path(bpy.path.abspath('//../../../misc/board_mp4/'))
            path_aud = clean_abs_path(bpy.path.abspath('//../../../misc/audio_wav/'))
            path = f"{path}{latest_version_available(path)}"
            path_aud = f"{path_aud}{latest_version_available(path_aud)}"
            mp4s = [file for file in os.listdir(path) if file.endswith('.mp4')]
            wavs = [file for file in os.listdir(path_aud) if file.endswith('.wav')]
            if len(mp4s) != 1:
                return False, "Failed to fix storyboard automatically"
            path = os.path.join(path, mp4s[0])

            if not os.path.isfile(path):
                return False, "Failed to locate last version of storyboard"

            path = bpy.path.relpath(path)

            if i.source == 'MOVIE_CLIP':
                if i.clip is None:
                    clip = bpy.data.movieclips.load(filepath=path)
                    msg += f"* A new clip was added to the camera: [{path}]"
                    i.clip = clip

                if i.clip.filepath != path:
                    msg += f"* The storyboard clip is now using: [{path}]"
                    i.clip.filepath = path

                if clip.frame_start != 101:
                    msg += "* Frame start of clip set to 101"
                    clip.frame_start = 101

            else:
                if len(wavs) != 1:
                    return False, "Failed to fix storyboard automatically"
                path_aud = os.path.join(path_aud, wavs[0])
                path_aud = bpy.path.relpath(path_aud)

                if i.image is None:
                    img = bpy.data.images.load(filepath=path)
                    msg += f"* A new image sequence was added to the camera: [{path}]"
                    i.image = img

                if i.image.filepath != path:
                    msg += f"* The storyboard clip is now using: [{path}]"
                    i.image.filepath = path
                    i.image.reload()

                    seq = find_sound(path_aud, wavs[0])

                    msg += f"* Loaded sound for storyboard: [{path_aud}]"

                    if seq.sound is None:
                        seq.sound = bpy.data.sounds.load(filepath=path_aud)

                    if seq.sound.filepath != path_aud:
                        seq.sound.filepath = path_aud


                if self.camera.data.background_images[0].image_user.frame_start != 101:
                    self.camera.data.background_images[0].image_user.frame_start = 101
                    msg += "* Frame start of image sequence set to 101"


            if (self.camera.data.background_images[0].alpha != 0.75) or (self.camera.data.background_images[0].scale != 0.25) or (self.camera.data.background_images[0].offset[0] != -0.375) or (self.camera.data.background_images[0].offset[1] != -0.375) or (self.camera.data.background_images[0].display_depth != 'FRONT'):
                msg += "* Fixed position and scale of the storyboard"
                self.camera.data.background_images[0].alpha = 0.75
                self.camera.data.background_images[0].scale = 0.25
                self.camera.data.background_images[0].offset[0] = -0.375
                self.camera.data.background_images[0].offset[1] = -0.375
                self.camera.data.background_images[0].display_depth = 'FRONT'

            return True, msg



        def test(self, context):
            if not self.camera.data.show_background_images:
                self.active = False
                return False, "No storyboard present in [Camera_render]"
            
            if len(self.camera.data.background_images) < 1:
                return False, "No storyboard present in [Camera_render]"
            
            if self.camera.data.background_images[0].source == 'MOVIE_CLIP':
                if self.camera.data.background_images[0].clip is None:
                    return False, "No storyboard present in [Camera_render]"
                self.storyboard = self.camera.data.background_images[0].clip
                if self.storyboard.frame_start != 101:
                    self.frame = False
                    return False, "The storyboard doesn't start at the correct frame"
            else:
                if self.camera.data.background_images[0].image is None:
                    return False, "No storyboard present in [Camera_render]"
                self.storyboard = self.camera.data.background_images[0].image
                if self.camera.data.background_images[0].image_user.frame_start != 101:
                    self.frame = False
                    return False, "The storyboard doesn't start at the correct frame"

            try:
                abs_p = clean_abs_path(bpy.path.abspath(self.storyboard.filepath))
            except:
                return False, "The path of the storyboard doesn't match any file"

            if (abs_p is None) or (not os.path.isfile(abs_p)):
                return False, "The path of the storyboard doesn't match any file"

            if not os.path.isdir(bpy.path.abspath('//../../../misc/board_mp4/')):
                self.alert = True
                return False, "Storyboard doesn't exist for that shot"

            theorical_path = clean_abs_path(bpy.path.abspath('//../../../misc/board_mp4/'))

            if not abs_p.startswith(theorical_path):
                return False, "The path of the storyboard doesn't point on the correct location"

            current = abs_p.split('/')[-2] # Dirty way to get the version
            if not re.fullmatch('(v|V)[0-9]{3}', current):
                return False, "The path of the storyboard doesn't point to a valid version"

            v = latest_version_available(theorical_path)

            self.path = theorical_path
            if v != current:
                return False, "Storyboard not up to date"


            if (self.camera.data.background_images[0].alpha != 0.75) or (self.camera.data.background_images[0].scale != 0.25) or (self.camera.data.background_images[0].offset[0] != -0.375) or (self.camera.data.background_images[0].offset[1] != -0.375) or (self.camera.data.background_images[0].display_depth != 'FRONT'):
                self.size = False
                return False, "Storyboard not at the correct location"


            return True, "Storyboard and audio are up to date"


    class TestCameraRender(QATest):

        """ Is there a [Camera_render] and is it active? """

        collection = None
        camera = None

        def necessary(self):
            return True

        def get_sub_test_types(self):
            return [TestStoryboardAudio]

        def can_fix(self, context):
            return True, "Can try to fix"

        def fix(self, context):
            msg = ""

            if self.collection is None:
                candidates = [c for c in bpy.data.collections if ("render" in c.name.lower())]
                if len(candidates) != 1:
                    return False, "Found a number of candidates different than 1 for [Camera_render] collection"
                else:
                    self.collection = candidates[0]
                    if self.collection.name not in bpy.context.scene.collection.children:
                        msg += f"* Collection [{self.collection.name}] was linked to the scene"
                        bpy.context.scene.collection.children.link(self.collection)
                    msg += f"* Collection [{self.collection.name}] was renamed [Camera_render]"
                    self.collection.name = 'Camera_render'

            if self.camera is None:
                self.camera = self.collection.objects.get('Camera_render') # Just in case the new collection contains what we were looking for
                if self.camera is None:
                    candidates = [o for o in self.collection if o.type == 'CAMERA']
                    if len(candidates) != 1:
                        return False, "Found a number of candidates different than 1 for [Camera_render] object"
                    else:
                        self.camera = candidates[0]
                        msg += f"* Object [{self.camera.name}] was renamed [Camera_render]"
                        self.camera.name = 'Camera_render'

            if not (self.camera is bpy.context.scene.camera):
                msg += "* Scene active camera is now [Camera_render]"
                bpy.context.scene.camera = self.camera

            return True, msg


        def test(self, context):
            self.collection = bpy.context.scene.collection.children.get('Camera_render')
            if self.collection is None:
                return False, "[Camera_render] collection not found"
            self.camera = self.collection.objects.get('Camera_render')
            if self.camera is None:
                return False, "[Camera_render] object not found"

            if not (self.camera is bpy.context.scene.camera):
                return False, "The rendering camera is not the good one"

            return True, "The current [Camera_render] is correctly setup"



    class TestAutoKeying(QATest):

        """ Is the auto-keying disabled? """

        def get_sub_test_types(self):
            return [TestCameraRender]

        def can_fix(self, context):
            return True, "Auto-keying will be disabled"

        def fix(self, context):
            bpy.context.scene.tool_settings.use_keyframe_insert_auto = False
            return True, "Auto-keying turned off"

        def test(self, context):
            if bpy.context.scene.tool_settings.use_keyframe_insert_auto:
                return False, "Auto-keying should be disabled"
            else:
                return True, "Auto-keying not engaged"


    class TestTopLevelContent(QATest):
        
        """ Is the top level only composed of the allowed content? """

        allowed_content = {'Chars', 'Props', 'Ref', 'Sets', 'Widgets', 'Shadow_catcher', 'Camera_render'}

        def necessary(self):
            return True

        def get_sub_test_types(self):
            return [TestAutoKeying]

        def test(self, context):
            err_col = [f"[{c.name}]" for c in bpy.context.scene.collection.children if (c.name not in self.allowed_content) and (not c.name.startswith('__'))]
            err_obj = [f"[{o.name}]" for o in bpy.context.scene.collection.objects]
            war_col = [f"[{c.name}]" for c in bpy.context.scene.collection.children if (c.name not in self.allowed_content) and (c.name.startswith('__'))]

            err_general = err_obj or err_col
            msg   = ""
            w_msg = ""

            if len(err_obj) > 0:
                msg += f"* Objects {', '.join(err_obj)} should not be at the top level of the scene"
            if len(err_col) > 0:
                msg += f"* Collections {', '.join(err_col)} should not be at the top level of the scene"
            if len(war_col) > 0:
                w_msg += f"* Collections {', '.join(war_col)} don't part of the usual content of a scene"

            if err_general:
                return False, msg
            else:
                if len(war_col) > 0:
                    self.raise_flag('warning')
                    return True, w_msg
                else:
                    return True, "The content at the top level is the one expected"

    return [TestTopLevelContent]