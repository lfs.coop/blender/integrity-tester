import bpy
from ..sqarf.qatest import QATest

def test_settings():


    class TestResolution(QATest):

        """ Is the resolution to 100% ? """

        def test(self, context):
            if bpy.context.scene.render.resolution_percentage == 100.0:
                return True, "Correct resolution percentage"
            else:
                return False, "Resolution percentage should be to 100%"


    class TestSettings(QATest):

        """ Pool of tests to perform on settings """

        def get_sub_test_types(self):
            return [TestResolution]

    return [TestSettings]