import bpy as bpy
from addon_utils import check,paths,enable

def get_all_addons():
    import sys

    paths_list = paths()
    addon_list = {}
    
    for path in paths_list:
        #_bpy.path.ensure_ext(path)
        for mod_name, mod_path in bpy.path.module_names(path):
            is_enabled, is_loaded = check(mod_name)
            addon_list[mod_name] = {'is_enabled': is_enabled, 'is_loaded': is_loaded}
            
    return(addon_list)
