import bpy
from ..sqarf.qatest import QATest

def test_requirements(req, coll):
	for attr, visi in req.items():
		if getattr(coll, attr) != visi:
			return False
	return True



class TestVisibility(QATest):

	requirements = {}
	desired_name = ""
	collection_type = ""
	collection = None

	def build_context(self, context):
		self.desired_name = context['name_'+self.collection_type+'_collection']
		self.collection = bpy.data.collections[self.desired_name]

		if self.collection is not None:
			return True, "Context built"
		else:
			return False, "Failed to build context [{}] collection missing".format(self.desired_name)


	def can_fix(self, context):
		return  True, "Will change visibility and selectability"

	def fix(self, context):

		for attr, visi in self.requirements.items():
			setattr(self.collection, attr, visi)

		return True, "[{}] collection is now visible in <b>viewport</b>: [{}], in <b>render</b>: [{}] and is <b>selectable</b>: [{}]".format(self.desired_name, not self.requirements['hide_viewport'], not self.requirements['hide_render'], not self.requirements['hide_select'])


	def test(self, context):
	
		valid = test_requirements(self.requirements, self.collection)

		if valid:
			return True, "Visibility and selectability of [{}] respected".format(self.desired_name)
		else:

			return False, "[{}] collection should have a different visibility or selectability".format(self.desired_name)