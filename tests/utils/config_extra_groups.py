import re
import os

extra_bones_integrity = True

def color_to_tuple(match):
    return "({}, {}, {})".format(match.group(1), match.group(2), match.group(3))


def make_dico(path):
	try:
		file = open(path)
		text = file.read()
		file.close()
	except:
		print("File not found for extra groups")
		return False, {}
	else:
		regex_float = "[0-9]{1}\.[0-9]*"
		regex = "\"({})\/({})\/({})\"".format(regex_float, regex_float, regex_float)

		text = text.replace('true', 'True').replace('false', 'False')
		text = re.sub(regex, color_to_tuple, text)

	try:
		dico = eval(text)
	except:
		return False, {}
	else:
		return True, dico

def bones_by_group(dico, test=False):
	sorted_bones = {}
	integrity = True
	errors = {}

	for group in dico['GroupTypes'][0]['groups']:
		sorted_bones[group['name']] = group['bones']

		if (len(group['active_bone']) > 0) and (group['active_bone'] not in group['bones']):
			integrity = False
			errors[group['name']] = group['active_bone']

	extra_bones_integrity = integrity

	if not test:
		return sorted_bones
	else:
		return errors, sorted_bones


# For each bone, the list of group in the one it is
def reversed_dico(dico):
	reversed_dico = {}

	for key, item in dico.items():
		for bone in item:
			if bone not in reversed_dico.keys():
				reversed_dico[bone] = []

			reversed_dico[bone].append(key)

	return reversed_dico


FILE = ""

if __name__ == "__main__":
	# For developping
	FILE = "../data/extra_group_base.txt"
	print("Developping session: ", FILE)
	status_EG, _config_extra_groups = make_dico(FILE)
	integrity, dico = bones_by_group(_config_extra_groups, True)
	print("INTEGRITY: ", integrity)

else:
	# Regular usage
	FILE = os.path.dirname(os.path.dirname(__file__)) + os.sep + "data" + os.sep + "extra_group_base.txt"
	print("Testing session: ", FILE)
	status_EG, _config_extra_groups = make_dico(FILE)

