import os
import bpy

# Function to rename an object of the opposite Blender way: The object renamed has the good name, the .001 is for the other
def rename_obj(obj, new_name):
    if new_name in bpy.data.objects:
        obj_old = bpy.data.objects.get(new_name)
        obj_old.name = new_name + ".001"
    obj.name = new_name

def explore_parents(obj, stop=''):
    if obj:
        if obj.type == stop:
            return True
        else:
            return explore_parents(obj.parent, stop)
    else:
        return False

def abs_path_to_infos(full_path):
    path = os.path.dirname(full_path).lower().replace('\\', '/')
    filename = os.path.basename(full_path)

    items_path = path.split('/')
    id = items_path.index('lib')
    items_path = items_path[id:]
    to_keep = []
    i = 0

    while(items_path[i] in filename):
        to_keep.append(items_path[i])
        i += 1


    asset_name = ""
    status = True
    if len(to_keep) > 0:
        asset_name = to_keep[-1]
    else:
        status = False
        return {'correct': False}

    version = ""
    try:
        version = int(items_path[-1][1:])
    except:
        version = items_path[-1]

    extension = full_path.split('.')

    return {'name': asset_name, 'category': items_path[1], 'subcategory': items_path[2], 'type':extension[-1], 'correct':status, 'version': version}


def infos_from_path_compo(full_path):
    full_path = full_path.lower().replace('\\', '/')
    path_items = full_path.split('/')

    try:
        index = path_items.index('siren')
    except:
        return {'correct': False}

    data = {'correct': True}

    path_items = path_items[index:]
    data['type'] = path_items.pop().split('.')[-1] # Remove the name of the .blend file

    version = path_items.pop() # Remove versionning directory
    try:
        data['version'] = int(version[1:])
    except:
        data['version'] = version

    path_items.pop() # Get rid of the 'render_blend'

    data['category'] = path_items.pop()

    data['name'] = path_items.pop()
    data['name'] = path_items.pop() + ' ' + data['name']

    return data


def bfs(root, result, hierarchy=[]):

    current = root
    to_visit = []
    to_visit.append(root)
    etage_courant = 1
    etage_suivant = 0
    numero_etage = 0
    compteur = 0
    hierarchy.append([])

    while (len(to_visit) > 0):

        if(compteur == etage_courant): # On change d'étage
            numero_etage += 1
            etage_courant = etage_suivant
            etage_suivant = 0
            compteur = 0
            hierarchy.append([])
        compteur += 1

        current = to_visit.pop(0)
        result.append(current)
        hierarchy[-1].append(current)

        for c in current.children:
            to_visit.append(c)
        etage_suivant += len(current.children)

def tree_from_collection(collection, flat=False):
    tree = {}
    objects = collection.objects

    for obj in objects:
        tree[obj.name] = []
        for o in obj.children:
            tree[obj.name].append(o)

    flat_list = [item.name for sublist in tree.values() for item in sublist]
    tree['tree_roots'] = [item.name for item in objects if item.name not in flat_list]
    flat_list += tree['tree_roots']

    if flat:
        return flat_list

    return tree


def locate_collection(desired_name, LUT):
    # Sub collections of the asset_name collection
    all_colls = [c.name for c in bpy.data.collections]
    all_colls_formated = [c.lower().replace(' ', '').replace('-', '_').replace(',', '') for c in all_colls]

    collection = bpy.data.collections.get(desired_name)
    if collection is not None:
        return True, collection

    else:
        if desired_name in all_colls_formated:
            collection = bpy.data.collections.get(all_colls[all_colls_formated.index(desired_name)])
            return True, collection

        for possible_name in LUT:
            for coll_name in all_colls_formated:
                if possible_name in coll_name:
                    collection = bpy.data.collections.get(all_colls[all_colls_formated.index(coll_name)])
                    return True, collection

    return False, None


def correct_collection(desired_name, collection, flag, main_collection):
    # Correcting name according conventions
    old_name = collection.name
    collection.name = desired_name

    # Moving collection in correct place
    for coll in bpy.data.collections:
        if desired_name in coll.children:
            coll.children.unlink(collection)

    if desired_name in bpy.context.scene.collection.children:
        bpy.context.scene.collection.children.unlink(collection)

    main_collection.children.link(collection)
    return old_name

