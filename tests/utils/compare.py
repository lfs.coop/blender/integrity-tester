bl_info = {
    "name": "Compare Materials",
    "description": "Compare Materials",
    "author": "Clément H. Benedetti",
    "version": (1, 0),
    "blender": (2, 91),
}


import bpy
import os

os.system('clear')

# Simply outputs a list, nothing done. Should contain all the conditions from 'specific_compare'
def specific_nodes():
    return ['TEX_IMAGE', 'BSDF_PRINCIPLED', 'OUTPUT_MATERIAL']

# Compares the inner properties of a given type of nodes
def specific_compare(n1, n2):
    if n1.type == 'TEX_IMAGE':
        i1 = n1.interpolation == n2.interpolation
        i2 = n1.projection == n2.projection
        i3 = n1.extension == n2.extension
        i4 = n1.image is n2.image
        return i1 and i2 and i3 and i4
    elif n1.type == 'BSDF_PRINCIPLED':
        i1 = n1.distribution == n2.distribution
        i2 = n1.subsurface_method == n2.subsurface_method
        return i1 and i2
    elif n1.type == 'OUTPUT_MATERIAL':
        return n1.target == n2.target
    else:
        return True

# Checks that on nodes, links are present at the same place, and unlinked socket have the same value
# Sockets not handled: SHADER
def same_links(n1, n2):
    for i in n1.inputs:
        if i.is_linked:
            if n2.inputs[i.name].is_linked:
                continue
            else:
                return False
        else:
            if i.type in ['RGBA', 'RGB', 'VECTOR']:
                t1 = tuple(i.default_value)
                t2 = tuple(n2.inputs[i.name].default_value)
                if t1 != t2:
                    return False
                
            elif i.type == 'VALUE':
                t1 = i.default_value
                t2 = n2.inputs[i.name].default_value
                if t1 != t2:
                    return False
                
            else:
                return True
    
    return specific_compare(n1, n2)

# Test that 2 nodes are the same
def equals(n1, n2):
    if n1.type != n2.type:
        return False
    else:
        return same_links(n1, n2)
        
# Locate the root of a node tree (the output node, which must be unique)
def locate_root(m):
    outputs = []
    for node in m.node_tree.nodes:
        if node.type == 'OUTPUT_MATERIAL':
            outputs.append(node)
    if len(outputs) == 1:
        return outputs[0]
    else:
        print("Impossible to find the root of the node tree")
        return None

# Detects if some flying nodes are present within the node tree
def flying_nodes(m):
    flying = []
    for node in m.node_tree.nodes:
        inputs  = [input.is_linked  for input  in node.inputs]
        outputs = [output.is_linked for output in node.outputs]
        
        if not (any(inputs) or any(outputs)):
            flying.append(node)

    return flying

# Recursion to browse the node tree. stops if nodes are different 
def compare_nodes(n1, n2):
    if equals(n1, n2):
        results = []
        for i1, i2 in zip(n1.inputs, n2.inputs):
            # In case the socket is not linked, the equals() function is responsible for the test.
            # This instruction is just useful to launch the recursion
            if i1.is_linked and i2.is_linked:
                results.append(compare_nodes(i1.links[0].from_node, i2.links[0].from_node))
        return all(results)
    
    else:
        return False
        

def compare_trees(material1, material2):
    if (not material1.use_nodes) or (not material2.use_nodes):
        print("One of the material doesn't use nodes")
        return False

    if (material1.node_tree is None) or (material2.node_tree is None):
        return False

    if (len(flying_nodes(material1)) > 0) and (len(flying_nodes(material2)) > 0):
        print("Can't work on a non-tree structure")
        return False
    
    if len(material1.node_tree.nodes) != len(material2.node_tree.nodes):
        return False
    
    #! Add a security detecting if there is several output nodes (there must not be several roots)
    r1 = locate_root(material1)
    r2 = locate_root(material2)
    
    if (r1 is None) or (r2 is None):
        print("Root not found")
        return False        
    
    return compare_nodes(r1, r2)

