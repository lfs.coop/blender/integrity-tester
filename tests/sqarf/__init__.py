from ._version import get_versions

from .qatest import QATest
from .session import Session

# this is to register default exporters
from .exporter import *
from .html_table_export import *
from .html_tree_export import *

__version__ = get_versions()["version"]
del get_versions
