import bpy
from ..sqarf.qatest import QATest

def test_cfist():

    class TestCFistScale(QATest):

        """ Do [c_fist] controlers have their scale locked ? """

        errors = []
        name_armature = ""
        armature = None
        c_fists = []

        def build_context(self, context):
            self.name_armature = context['name_rig_collection']
            self.armature = bpy.data.objects.get(self.name_armature)
            self.c_fists.append(self.armature.pose.bones.get('c_fist.l'))
            self.c_fists.append(self.armature.pose.bones.get('c_fist.r'))

            if None in self.c_fists:
                return False, "Failed to build context, one on [c_fist] missing"

            return True, "Context built"

        def can_fix(self, context):
            return True, "Will lock scales"

        def fix(self, context):
            msg = []

            for error in self.errors:
                error[0].lock_scale[error[1]] = True
                msg.append("*On [{}], slot [{}] locked to scale".format(error[0].name, error[1]))

            return True, "Some scales have been locked: {}".format("".join(msg))


        def test(self, context):

            for fist in self.c_fists:
                if not fist.lock_scale[0]:
                    self.errors.append((fist, 0))

                if not fist.lock_scale[2]:
                    self.errors.append((fist, 2))

            if len(self.errors) == 0:
                return True, "[c_fist] controlers have their scale locked"
            else:
                return False, "Slots 0 et 2 of [c_fist] controlers should have their scale locked"




    class TestCFist(QATest):

        """ Are [c_fist.l/.r] controlers present in the main rig """

        errors = []
        name_armature = ""
        armature = None

        def necessary(self):
            return True

        def build_context(self, context):
            self.name_armature = context['name_rig_collection']
            self.armature = bpy.data.objects.get(self.name_armature)
            if self.armature is None:
                return False, "Failed to build context, armature missing [{}]".format(self.armature_name)
            return True, "Context built"

        def get_sub_test_types(self):
            return [TestCFistScale]

        def relevant_for(self, context):
            if (context['phase'] == 'ANIM') and (context['category'] == 'chars') and (context['subcategory'] != 'animal'):
                return True, "Working on character"
            else:
                return False, "Not working on character"

        def test(self, context):

            for s in ['.l', '.r']:
                fist = self.armature.pose.bones.get("c_fist" + s)

                if not fist:
                    self.errors.append("[c_fist" + s + "]")

            if len(self.errors) > 0:
                return False, "Some controlers are missing: {}. They have to be created with Auto Rig Pro".format(", ".join(self.errors))
            else:
                return True, "All controlers [c_fist] are correct"


    return [TestCFist]
