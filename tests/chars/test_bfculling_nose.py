import bpy
from ..sqarf.qatest import QATest

def test_bfculling_nose():

    class TestBFCullingNose(QATest):

        """ Is the backface culling activated on the material [mat_headgraphics] """

        obj_target = None
        mat_target = None

        def build_context(self, context):
            self.obj_target = bpy.data.objects.get('nose_lines')
            if self.obj_target is None:
                return False, "Failed to build context"
            else:
                self.mat_target = self.obj_target.material_slots[0].material
                if self.mat_target is None:
                    return False, "Failed to build context"
                else:
                    return True, "Context built"

        def can_fix(self, context):
            return True, "Will activate backface culling"

        def fix(self, context):
            self.mat_target.use_backface_culling = True
            return True, "Backface culling has been activated on the material [mat_headgraphics]"

        def test(self, context):
            if self.mat_target.use_backface_culling:
                return True, "Backface culling activated on the material [mat_headgraphics]"
            else:
                return False, "Backface culling should be activated on the material [mat_headgraphics]"


    class TestMaterialNoseLines(QATest):

        """ Is there a [mat_headgraphics] material on the [nose_lines] object """

        desired_name = ""
        target = None

        def get_sub_test_types(self):
            return [TestBFCullingNose]


        def necessary(self):
            return True


        def build_context(self, context):
            self.desired_name = "mat_headgraphics"
            self.target = bpy.data.objects.get('nose_lines')

            if self.target is None:
                return False, "Failed to build context, [nose_lines] not found"
            else:
                return True, "Context build"


        def can_fix(self, context):
            if len(self.target.material_slots) == 1:
                return True, "Will fix"
            else:
                return False, "Can't fix"

        def fix(self, context):
            old_name = self.target.material_slots[0].material.name
            self.target.material_slots[0].material.name = self.desired_name
            new_name = self.target.material_slots[0].material.name
            return True, "Name of the [{}] material was changed to [{}]".format(old_name, new_name)

        def test(self, context):
            if len(self.target.material_slots) == 0 or len(self.target.material_slots) > 1:
                return False, "[nose_lines] should have a unique material named [{}]".format(self.desired_name)
            else:
                name_mat = self.target.material_slots[0].material.name
                if name_mat == self.desired_name:
                    return True, "[nose_lines] has a unique material named [{}]".format(self.desired_name)
                else:
                    return False, "[nose_lines] has a unique material badly named: [{}]. Only [{}] should be present".format(name_mat, self.desired_name)



    class TestNameNoseLines(QATest):

        """ Is there a [nose_lines] object in [asset_name_high] ? """

        high_collection = None
        objects = []
        nose_lines = None
        desired_name = ""

        def necessary(self):
            return True

        def relevant_for(self, context):
            if (context['category'] == 'chars') and (context['subcategory'] != 'animal'):
                return True, "Valid context"
            else:
                return False, "Not relevant for props or animals"

        def get_sub_test_types(self):
            return [TestMaterialNoseLines]

        def build_context(self, context):
            self.desired_name = "nose_lines"
            self.high_collection = bpy.data.collections.get(context['name_high_collection'])

            if self.high_collection is None:
                return False, "Failed to build context, high collection not found"
            else:
                self.objects = [o.name for o in self.high_collection.objects]
                return True, "Context built"

        def can_fix(self, context):
            keywords = [('nose', 1), ('noze', 1), ('line', 0.5), ('shadow', 0.5), ('light', 0.5)]
            formated_names = [n.lower() for n in self.objects]
            potentials = {}

            for name in formated_names:
                for key in keywords:
                    if key[0] in name:
                        potentials.setdefault(name, 0)
                        potentials[name] += key[1]

            nb_occurs = 0
            if len(potentials.values()) > 0:
                max_occur = max(potentials.values()) # The biggest number of points accumulated
                nb_occurs = list(potentials.values()).count(max_occur) # How many objects have the max amount of points ?
            
            if nb_occurs == 0:
                return False, "Can't locate [{}] collection".format(self.desired_name)
            elif nb_occurs > 1:
                return False, "Too many candidates can be nose lines"
            else: # We change the name only if we have one result
                if max_occur < 1.5:
                    return False, "Can't fix, not sure about which object is [nose_lines]"
                else: # The score must me high enough to perform the change

                    closest_name = [key for key, item in potentials.items() if (item == max_occur)][0]
                    self.nose_lines = bpy.data.objects.get(self.objects[formated_names.index(closest_name)])
                    return True, "Will rename"

        def fix(self, context):
            old_name = self.nose_lines.name
            self.nose_lines.name = self.desired_name
            return True, "Object [{}] was renamed [{}]".format(old_name, self.desired_name)

        def test(self, context):
            if self.desired_name in self.objects:
                return True, "Object [{}] found in [{}] collection".format(self.desired_name, context['name_high_collection'])
            else:
                return False, "[{}] object not found in [{}] collection".format(self.desired_name, context['name_high_collection'])


    return [TestNameNoseLines]