import bpy
from ..sqarf.qatest import QATest

def locate_neck_shadow(objects_collection):
    objects = [o.name for o in objects_collection]
    keywords = [('neck', 1), ('shadow', 0.5)]
    formated_names = [n.lower() for n in objects]
    potentials = {}

    for name in formated_names:
        for key in keywords:
            if key[0] in name:
                potentials.setdefault(name, 0)
                potentials[name] += key[1]

    nb_occurs = 0
    if len(potentials.values()) > 0:
	    max_occur = max(potentials.values()) # The biggest number of points accumulated
	    nb_occurs = list(potentials.values()).count(max_occur) # How many objects have the max amount of points ?

    if nb_occurs == 0:
        return False, None
    elif nb_occurs > 1:
        return False, None
    else: # We change the name only if we have one result
        if max_occur < 1.5:
            return False, None
        else: # The score must me high enough to perform the change
            closest_name = [key for key, item in potentials.items() if (item == max_occur)][0]
            neck_shadow = bpy.data.objects.get(objects[formated_names.index(closest_name)])
            return True, neck_shadow


def test_neck_shadow():

    class TestDesactivateShadow(QATest):

        """ Is the property [auto_shadow] present in [c_head_shadow] ? """

        desired_bone_name = ""
        desired_ppty_name = ""
        bone = None
        armature = None

        def relevant_for(self, context):
            return 'neck_shadow' in context.to_dict().keys(), "Found"

        def build_context(self, context):
            self.desired_bone_name = "c_head_shadow"
            self.desired_ppty_name = "auto_shadow"
            self.armature = bpy.data.objects.get(context['name_rig_collection'])
            self.bone = self.armature.pose.bones.get(self.desired_bone_name)
            return True, "Context built"

        def test(self, context):
            if self.bone is None:
                return True, "Bone not found"
            else:
                if self.desired_ppty_name in self.bone.keys():
                    return True, "Property [{}] found in [{}]".format(self.desired_ppty_name, self.desired_bone_name)
                else:
                    return False, "Property [{}] should be in [{}]".format(self.desired_ppty_name, self.desired_bone_name)


    class TestNeckShadow(QATest):

        """ Is the setup of the chin shadow correct ? """

        high_collection = None
        main_armature = None
        neck_shadow = None
        desired_name = ""
        desired_bone = ""

        def necessary(self):
            return True

        def get_sub_test_types(self):
            return [TestDesactivateShadow]

        def relevant_for(self, context):
            if (context['category'] == 'chars') and (context['subcategory'] != 'animal'):
                return True, "Valid context"
            else:
                return False, "Not relevant for props or animals"


        def build_context(self, context):
            self.high_collection = bpy.data.collections.get(context['name_high_collection'])
            self.desired_name = "neck_shadow"
            self.desired_bone = "c_head_shadow"

            if self.high_collection is None:
                return False, "Failed to build context"

            self.main_armature = bpy.data.objects.get(context['name_rig_collection'])

            if self.main_armature is None:
                return False, "Failed to build context"
            
            return True, "Context built"



        def test(self, context):
            succes, self.neck_shadow = locate_neck_shadow(self.high_collection.objects)
            bone_shadow = self.main_armature.pose.bones.get(self.desired_bone)
            if succes:
            	self.desired_name = self.neck_shadow.name

            if self.neck_shadow is None:
                return True, "Mesh [{}] not present in the scene".format(self.desired_name)
            else:
                if bone_shadow is None:
                    return False, "Mesh [{}] is present in [{}] but there is no bone [{}] in [{}]".format(self.desired_name, self.high_collection.name, self.desired_bone, self.main_armature.name)
                else:
                    context['neck_shadow'] = True
                    return True, "Mesh [{}] is present in [{}] and the bone [{}] is present in [{}]".format(self.desired_name, self.high_collection.name, self.desired_bone, self.main_armature.name)

    return [TestNeckShadow]