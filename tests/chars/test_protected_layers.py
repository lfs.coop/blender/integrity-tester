import bpy
from ..sqarf.qatest import QATest
import numpy as np

def index_layer(bone):
    for i, layer in enumerate(bone.layers):
        if layer:
            return i
    return -1

def must_skip(bone, allowed):
    for word in allowed:
        if word in bone.name:
            return True
    return False


def test_protected_layers():

    class TestConstraintsProtectedLayers(QATest):

        """ Is there bone constraints on unprotected layers """

        armatures = []
        allowed = ["pole", "_ik", "c_ring1_base", "c_stretch_leg", "c_spine", "c_neck", "c_stretch_arm", "c_head_shadow", "c_jawbone", "c_cheek_smile", "c_middle1_base", "c_thumb", "c_fist", "c_pinky1_base", "c_foot_01", "c_foot_roll_cursor", "c_arm_fk", "c_eyelid_top", "c_eyelid_bot"]
        types_allowed = ['CHILD_OF', 'COPY_TRANSFORMS', 'ACTION', 'LIMIT_ROTATION', 'LIMIT_LOCATION', 'LIMIT_SCALE', 'LIMIT_DISTANCE']
        errors = {}
        left_side_layers = set(np.arange(0, 8)).union(set(np.arange(16, 24)))


        def build_context(self, context):
            self.armatures = [p for p in bpy.data.objects if (p.type == 'ARMATURE') if (p.data is not None)]
            return True, "Context built"


        def test(self, context):
            for arma in self.armatures:
                protected = [i for i, layer in enumerate(arma.data.layers_protected) if layer]
                pose = arma.data
                for bone in pose.bones:
                    if not must_skip(bone, self.allowed):
                        index = index_layer(bone)
                        if (index in self.left_side_layers) and (index not in protected):
                            bone_arma = arma.pose.bones[bone.name]
                            for constraint in bone_arma.constraints:
                                if not (constraint.type in self.types_allowed):
                                    self.errors.setdefault(arma.name, [])
                                    self.errors[arma.name].append((bone.name, constraint.name))

            if len(self.errors) > 0:
                msg = ""
                for key, item in self.errors.items():
                    msg += "* <b>In armature: {} </b></br>".format(key)
                    errs = ["{}(<i>{}</i>)".format(i[1], i[0]) for i in item]
                    msg += "Some bones on unprotected layers have bone constraints: {}".format(", ".join(errs))
                return False, msg
            else:
                return True, "All bones on constraints are correct"



    return [TestConstraintsProtectedLayers]