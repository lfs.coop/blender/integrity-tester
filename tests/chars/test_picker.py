import bpy
from ..sqarf.qatest import QATest

def test_picker():

	class TestPickerTexture(QATest):

		""" Does the picker [asset_name_picker] have a name ending with [_picker_bg.extension] ? """

		picker = None

		def build_context(self, context):
			self.picker = bpy.data.objects.get(context['name_texture_picker'])

			if self.picker is None:
				return False, "Failed to build context. Picker [{}] missing in [rig_ui]".format(context['name_texture_picker'])
			else:
				return True, "Context built"


		def can_fix(self, context):
			if self.picker.data is None:
				return False, "Can't fix"
			else:
				return True, "Will rename"

		def fix(self, context):
			old_name = self.picker.data.name

			new_name = context['name_asset'] + "_picker_bg." + self.picker.data.file_format.lower()
			self.picker.data.name = new_name

			return True, "[{}]'s name' was changed to [{}]".format(old_name, new_name)

		def test(self, context):

			if self.picker.data is None:
				return False, "The background picker [{}] should have a texture [(...)_picker_bg.png]".format(self.picker.name)
			else:
				if "_picker_bg." in self.picker.data.name:
					return True, "Texture in picker correctly placed and named"
				else:
					return False, "The name of the object inside [{}] should end with [_picker_bg]".format(self.picker.name)


	class TestImagePicker(QATest):

		""" An image named [asset_name_picker] should be child of [rig_ui] """

		rig_ui = None
		picker = None

		def get_sub_test_types(self):
			return [TestPickerTexture]

		def necessary(self):
			return True

		def build_context(self, context):
			self.rig_ui = bpy.data.objects.get("rig_ui")

			if self.rig_ui is None:
				return False, "Failed to build context. [rig_ui] object missing"
			else:
				return True, "Context built"


		def can_fix(self, context):
			children = [o.name for o in self.rig_ui.children if o.type == 'EMPTY']

			if len(children) == 0:
				return False, "An empty named [{}] should be present as child of [rig_ui]".format(context['name_texture_picker'])
			elif len(children) == 1:
				self.picker = bpy.data.objects.get(children[0])
				return True, "Empty will be renamed"
			else:
				return False, "A unique empty [{}] should be present as child of [rig_ui]".format(context['name_texture_picker'])

		def fix(self, context):
			if self.picker is not None:
				old_name = self.picker.name
				self.picker.name = context['name_texture_picker']
				return True, "Object [{}] was renamed [{}]".format(old_name, context['name_texture_picker'])
			else:
				return False, "Failed to fix"

		def test(self, context):
			try:
				children = [o.name for o in list(self.rig_ui.children)]
				index = children.index(context['name_texture_picker'])
			except:
				self.picker = None
			else:
				self.picker = self.rig_ui.children[index]

			if self.picker is None:
				return False, "[{}] object not found as child of [{}] in the armature [{}]".format(context['name_texture_picker'], "rig_ui", context['name_rig_collection'])
			else:
				return True, "[{}] object correctly placed and named".format(context['name_texture_picker'])
				



	class TestRigUI(QATest):

		""" An empty named [rig_ui] is parented to the main armature """

		rig_collection = None
		armature = None
		rig_ui = None
		fixing_data = None

		def get_sub_test_types(self):
			return [TestImagePicker]

		def necessary(self):
			return True

		def relevant_for(self, context):
			if (context['category'] == 'ANIM') and (context['category'] == 'chars') and (context['subcategory'] != 'animal'):
				return True, "We are working on a character"
			else:
				return False, "We are working on a [{}], not on a [char]".format(context['category'])


		def build_context(self, context):
			self.rig_collection = bpy.data.collections.get(context['name_rig_collection'])

			if self.rig_collection is None:
				return False, "Impossible to build context, [{}] is missing".format(context['name_rig_collection'])
			else:
				self.armature = self.rig_collection.objects.get(context['name_rig_collection'])

				if self.armature is None:
					return False, "Impossible to build context, armature [{}] is missing".format(context['name_rig_collection'])
				else:
					return True, "Context built"


		def can_fix(self, context):
			children = [o.name for o in self.armature.children if o.type == 'EMPTY']

			if len(children) == 0:
				return False, "Can't fix. The armature [{}] should have an empty [{}] in its children".format(context['name_rig_collection'], "rig_ui")
			
			elif len(children) == 1:
				self.fixing_data = bpy.data.objects.get(children[0])
				return True, "[{}] will be renamed".format(children[0])
			
			else:
				return False, "Can't fix. A unique empty named [rig_ui] should be child of the [{}] armature".format(context['name_rig_collection'])


		def fix(self, context):
			if self.fixing_data is not None:
				old_name = self.fixing_data.name
				self.fixing_data.name = "rig_ui"
				return True, "In armature [{}], the object [{}] was renamed [{}]".format(context['name_rig_collection'], old_name, "rig_ui")
			else:
				return False, "Fixing failed"


		def test(self, context):
			
			try:
				children = [o.name for o in self.armature.children]
				index = children.index('rig_ui')
			except:
				self.rig_ui = None
			else:
				self.rig_ui = self.armature.children[index]

			if self.rig_ui is None:
				return False, "The armature [{}] should have an empty named [{}] in its children".format(context['name_rig_collection'], "rig_ui")
			else:
				if self.rig_ui.type == 'EMPTY':
					return True, "[{}] object found as child of the [{}] armature".format("rig_ui", context['name_rig_collection'])
				else:
					return False, "[{}] object found as child of the [{}] armature *It should be an ['EMPTY'] but found a ['{}']".format("rig_ui", context['name_rig_collection'], self.rig_ui.type)


	return [TestRigUI]