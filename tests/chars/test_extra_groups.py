import bpy
from ..sqarf.qatest import QATest
import json
import mathutils
import numpy as np

from ..utils.addons import get_all_addons
from ..utils.config_extra_groups import status_EG, _config_extra_groups, FILE, bones_by_group, reversed_dico, extra_bones_integrity
import addon_utils

left_side_layers = set(np.arange(0, 8)).union(set(np.arange(16, 24)))

def index_of(arma, group_name):
    for idx, grp in enumerate(arma.jueg_grouptypelist[0]['group_ids']):
        if grp['name'] == group_name:
            return idx
    return -1

def is_on_left_side(bone):
    for idx, layer in enumerate(bone.bone.layers):
        if layer and (idx in left_side_layers):
            return True
    return False

def is_exception(name, collec):
    for item in collec:
        if item in name:
            return True
    return False

def test_extra_groups():

    class TestIn2Groups(QATest):
        
        """ Each bone is in both [all] and another group """

        errors = {}
        just_all = True
        exceptions = set(['c_eye_target'])

        def can_fix(self, context):
            if self.just_all:
                return True, "Will fix"
            else:
                return False, "can't fix"

        def fix(self, context):
            armatures = [a for a in bpy.data.objects if a.type == 'ARMATURE']

            for arma in armatures:
                override = bpy.context.copy()

                for window in bpy.context.window_manager.windows:
                    screen = window.screen
                    for area in screen.areas:
                        if area.type == 'VIEW_3D':
                            for region in area.regions:
                                if region.type == 'WINDOW':
                                    override['window'] = window 
                                    override['screen'] = screen
                                    override['area']   = area
                                    override['object'] = arma
                                    override['active_object'] = arma
                                    override['region'] = region #'WINDOW'
                                    override['region_data'] = region
                                    override['workspace'] = window.workspace
                                    break

                bpy.ops.object.mode_set(override, mode='POSE')
                idx = index_of(arma, 'all')
                arma.jueg_grouptypelist[0]['active_bonegroup'] = idx
                bpy.ops.pose.select_all(override, action='DESELECT')
                for bone in self.errors[arma.name]['all']:
                    arma.pose.bones.get(bone).bone.select = True
                bpy.ops.pose.jueg_bonegroup_assign(override)
                bpy.ops.object.mode_set(override, mode='OBJECT')

            return True, "Bone of all armatures have been assigned to the [all] group"


        def test(self, context):
            armatures = [o for o in bpy.data.objects if o.type == 'ARMATURE']

            dict_bones_by_group = {}
            dict_groups_by_bone = {}

            i = 0
            while i < len(armatures):
                if armatures[i].pose is None:
                    armatures.remove(armatures[i])
                else:
                    i += 1

            for armature in armatures:
                dict_bones_by_group[armature.name] = {}

                for group in armature.jueg_grouptypelist[0]['group_ids']:
                    dict_bones_by_group[armature.name][group['name']] = []

                    if 'bone_ids' in group.keys():
                        for bone in group['bone_ids']:
                            dict_bones_by_group[armature.name][group['name']].append(bone['name'])
                    else:
                        #self.raise_flag("warning")
                        pass

            for key in dict_bones_by_group.keys():
                # For a bone, the list of groups in which it is
                try:
                    dict_bones_by_group[key].pop('all')
                except:
                    pass

                dict_groups_by_bone[key] = reversed_dico(dict_bones_by_group[key])

            for armature in armatures:
                self.errors[armature.name] = {'all': [], 'group': []}
                idx = index_of(armature, 'all')

                if idx == -1:
                    return False, "No [all] group found in [{}]".format(armature.name)
                
                all_content = set()

                if 'bone_ids' in armature.jueg_grouptypelist[0]['group_ids'][idx]:
                    all_content = set([o['name'] for o in armature.jueg_grouptypelist[0]['group_ids'][idx]['bone_ids']])

                all_bones = set([b.name for b in armature.pose.bones if ('proxy' not in b.name) and (is_on_left_side(b))])

                # Bones that are not in the 'all' extra group
                missing = all_bones - all_content
                missing = [m for m in missing if not is_exception(m, self.exceptions)]
                self.errors[armature.name]['all'] = missing

                not_in_group = []

                for bone in all_bones:
                    if (bone not in dict_groups_by_bone[armature.name].keys()) and (not is_exception(bone, self.exceptions)):
                        not_in_group.append(bone)


                self.errors[armature.name]['group'] = not_in_group

            msg = ""
            error = False


            for arma, errs in self.errors.items():
                fail_arma = False
                fail_all = False
                fail_group = False
                msg_arma = "* <b>In armature [{}]</b>: ".format(arma)
                msg_all = ""
                msg_group = ""

                if len(errs['all']) > 0:
                    fail_arma = True
                    fail_all = True
                    error = True
                    names = ["[{}]".format(o) for o in errs['all']]
                    msg_all += "<b>+</b> Bone(s) {} should be in the [all] extra group".format(", ".join(names))

                if len(errs['group']) > 0:
                    self.just_all = False
                    fail_arma = True
                    fail_group = True
                    error = True
                    names = ["[{}]".format(o) for o in errs['group']]
                    msg_group += "<b>+</b> Bone(s) {} should be in an extra bones group".format(", ".join(names))

                msg_arma += " * " + msg_all
                msg_arma += " * " + msg_group

                if (not fail_group) and (not fail_all):
                    msg_arma = ""

                msg += msg_arma

            
            if error:
                return False, msg
            else:
                return True, "Each bone is in the groups it should be in"

    class TestCorrectGroup(QATest):
        
        """ Is each bone in the group it is supposed to ? """

        sorted_bones = {}
        known_bones = set()
        fixes = {}

        def get_sub_test_types(self):
            return [TestIn2Groups]


        def necessary(self):
            return True


        def build_context(self, context):
            try:
                self.sorted_bones = bones_by_group(_config_extra_groups)
            except:
                return False, "Failed to build context. Impossible to parse ExtraGroups bones"

            if not extra_bones_integrity:
                return False, "The file provided to test extra bones groups is incorrect"

            total = []
            for key, item in self.sorted_bones.items():
                total.extend(item)

            # All bones that are in at least one extra group
            self.known_bones = set(total)
            #print(self.known_bones)
            #print(len(self.sorted_bones['all']), len(self.known_bones))

            return True, "Context built"


        def can_fix(self, context):
            return True, "Will change groups"


        def fix(self, context):

            for armature, reports in self.fixes.items():
                override = bpy.context.copy()
                arma = bpy.data.objects.get(armature)

                if arma is None:
                    return False, "Armature [{}] is None".format(armature)

                for window in bpy.context.window_manager.windows:
                    screen = window.screen
                    for area in screen.areas:
                        if area.type == 'VIEW_3D':
                            for region in area.regions:
                                if region.type == 'WINDOW':
                                    override['window'] = window 
                                    override['screen'] = screen
                                    override['area']   = area
                                    override['object'] = arma
                                    override['active_object'] = arma
                                    override['region'] = region #'WINDOW'
                                    override['region_data'] = region
                                    override['workspace'] = window.workspace
                                    break

                msg = ""
                error = False

                try:
                    bpy.ops.object.mode_set(override, mode='POSE')
                    override['mode'] = 'POSE'
                    bpy.ops.pose.select_all(override, action='DESELECT')
                except:
                    error = True
                    fail = True
                    return False, "Failed to pass in pose mode."

                fail = False
                msg_arma = ""
                msg_arma += "*<b>In the armature [{}]</b> : ".format(armature)

                for group, errors in reports.items():
                    idx = index_of(arma, group)
                    arma.jueg_grouptypelist[0]['active_bonegroup'] = idx

                    group_fail = False
                    msg_group = ""
                    msg_group += "* &nbsp;&nbsp;&nbsp;&nbsp;<b>+</b> In group [{}] : ".format(group)
                    blank = msg_group
                    adds = []
                    remove = []

                    if len(errors['adds']) > 0:
                        bpy.ops.pose.select_all(override, action='DESELECT')
                        error = True

                        for o in errors['adds']:
                            bone = arma.pose.bones.get(o)
                            if bone:
                                bone.bone.select = True

                        bpy.ops.pose.jueg_bonegroup_assign(override)

                        names = ["[{}]".format(o) for o in errors['adds']]
                        adds.extend(names)

                    if len(errors['remove']) > 0:
                        bpy.ops.pose.select_all(override, action='DESELECT')
                        error = True

                        for o in errors['remove']:
                            bone = arma.pose.bones.get(o)
                            if bone:
                                bone.bone.select = True

                        bpy.ops.pose.jueg_bonegroup_bone_remove(override)

                        names = ["[{}]".format(o) for o in errors['remove']]
                        remove.extend(names)

                    if len(adds) > 0:
                        msg_group += "Bone(s) {} were <b style=\"color: green; font-weight: bold;\">added</b> ".format(", ".join(adds))
                        group_fail = True
                        fail = True

                    if len(remove) > 0:
                        if msg_group != blank:
                            msg_group += "<b> ; </b>"
                        msg_group += "Bone(s) {} were <b style=\"color: red; font-weight: bold;\">removed</b> ".format(", ".join(remove))
                        group_fail = True
                        fail = True

                    msg_group += ";"

                    if not group_fail:
                        msg_group = ""

                    msg_arma += msg_group

                if not fail:
                    msg_arma = ""

                msg += msg_arma

                override = bpy.context.copy()

                for window in bpy.context.window_manager.windows:
                    screen = window.screen
                    for area in screen.areas:
                        if area.type == 'VIEW_3D':
                            for region in area.regions:
                                if region.type == 'WINDOW':
                                    override['window'] = window 
                                    override['screen'] = screen
                                    override['area']   = area
                                    override['object'] = arma
                                    override['active_object'] = arma
                                    override['region'] = region #'WINDOW'
                                    override['region_data'] = region
                                    override['workspace'] = window.workspace
                                    override['mode'] = 'POSE'
                                    break

                try:
                    bpy.ops.object.mode_set(override, mode='OBJECT')
                    override['mode'] = 'OBJECT'
                except:
                    error = True
                    fail = True
                    return False, "Failed to pass in object mode."

            if error:
                return True, msg
            else:
                return False, "Nothing to fix !?"


        def test(self, context):
            
            armatures = [a for a in bpy.data.objects if a.type == 'ARMATURE'] # Fetch all armatures from the scene
            dict_bones_by_group = {}
            dict_groups_by_bone = {}
            
            theoric_bones_by_group = bones_by_group(_config_extra_groups) # Fetch reference file for bones by extra group
            theoric_groups_by_bone = reversed_dico(theoric_bones_by_group) # Reverse the dictionnary to get the list of groups for each bone


            for i, armature in enumerate(armatures):
                if armature.pose is None:
                    armatures.remove(armature)

            for armature in armatures:
                dict_bones_by_group[armature.name] = {} # Initialize an entry for each armature

                for group in armature.jueg_grouptypelist[0]['group_ids']: # For each group that we find on this armature
                    dict_bones_by_group[armature.name][group['name']] = [] # In the armature, we initialize a list for this extra group

                    if 'bone_ids' in group.keys(): # If the group is not empty
                        for bone in group['bone_ids']: # For each bone found in the group
                            if armature.pose.bones.get(bone['name']) is not None: # We add the bone if it exists in the armature (security if generated from file)
                                dict_bones_by_group[armature.name][group['name']].append(bone['name']) # Add the bone to the group
                    else:
                        #self.raise_flag("warning")
                        pass

            for key in dict_bones_by_group.keys():
                dict_groups_by_bone[key] = reversed_dico(dict_bones_by_group[key]) # Reversing each armature dictionary

            
            for armature, layout in dict_groups_by_bone.items(): # For each armature and its list of registered groups by bone
                # Check that a known bone is not in a bad known group
                self.fixes[armature] = {}
                for bone, groups in layout.items():
                    if bone in self.known_bones:
                        for group in groups:
                            if group != 'all':
                                if group in theoric_bones_by_group.keys():
                                    #print(bone, group)
                                    # 'group': group in the one 'bone' currently is.
                                    if group not in self.fixes[armature].keys():
                                        self.fixes[armature][group] = {'adds': [], 'remove': []}

                                    if bone in theoric_bones_by_group[group]:
                                        # The bone has to be in that group, and effectively is.
                                        pass
                                    else:
                                        # The bone is in the group, but shouldn't (has to be removed)
                                        self.fixes[armature][group]['remove'].append(bone)
                                    
                
                # Check that a known bone is not missing in any known group
                for bone, groups in theoric_groups_by_bone.items():
                    if bone in bpy.data.objects[armature].pose.bones:
                        # The bone exists
                        for group in groups:
                            if group != 'all':
                                if group not in self.fixes[armature].keys():
                                    self.fixes[armature][group] = {'adds': [], 'remove': []}

                                if bone in layout:
                                    if group in layout[bone]:
                                        # The bone is not missing in that group
                                        pass
                                    else:
                                        # The bone should be in that group (should be added)
                                        self.fixes[armature][group]['adds'].append(bone)


            msg = ""
            error = False

            for armature, reports in self.fixes.items():
                fail = False
                msg_arma = ""
                msg_arma += "*<b>In the armature [{}]</b> : ".format(armature)

                for group, errors in reports.items():
                    group_fail = False
                    msg_group = ""
                    msg_group += "* &nbsp;&nbsp;&nbsp;&nbsp;<b>+</b> In group [{}] : ".format(group)
                    blank = msg_group
                    adds = []
                    remove = []

                    if len(errors['adds']) > 0:
                        error = True
                        names = ["[{}]".format(o) for o in errors['adds']]
                        adds.extend(names)

                    if len(errors['remove']) > 0:
                        error = True
                        names = ["[{}]".format(o) for o in errors['remove']]
                        remove.extend(names)

                    if len(adds) > 0:
                        msg_group += "Bone(s) {} <b style=\"color: green; font-weight: bold;\">should be here</b> ".format(", ".join(adds))
                        group_fail = True
                        fail = True

                    if len(remove) > 0:
                        if msg_group != blank:
                            msg_group += "<b> ; </b>"
                        msg_group += "Bone(s) {} <b style=\"color: red; font-weight: bold;\">shouldn't be here</b> ".format(", ".join(remove))
                        group_fail = True
                        fail = True

                    msg_group += ";"

                    if not group_fail:
                        msg_group = ""

                    msg_arma += msg_group

                if not fail:
                    msg_arma = ""

                msg += msg_arma

            if error:
                return False, msg
            else:
                return True, "All base bones placed in correct ExtraGroups"

    class TestAllGroupsPresent(QATest):

        """ Some Extra Groups are necessary, are they present ? """

        list_groups = set()
        missing = {}

        def get_sub_test_types(self):
            return [TestCorrectGroup]


        def necessary(self):
            return True


        def build_context(self, context):
            if status_EG:
                for item in _config_extra_groups['GroupTypes'][0]['groups']:
                    self.list_groups.add(item['name'])
                #self.list_groups.add('select_all')
                return True, "Context built"
            else:
                return False, "Failed to open or build file for extra groups config"


        def can_fix(self, context):
            
            for key, item in self.missing.items():
                if (len(item) > 0) and (item[0] == 'EXCESS'):
                    return False, "Can't fix"

            return True, "Can try to fix"



        def fix(self, context):
            
            error = False
            msg = ""
            override = bpy.context.copy()            

            # One key == one armature
            for key, item in self.missing.items():
                fail = False

                if len(item) > 0:

                    armature = bpy.data.objects.get(key)

                    if armature is None:
                        error = True
                        fail = True
                        msg += "Failed to fix, armature [{}] is None * ".format(key)
                        continue

                    for window in bpy.context.window_manager.windows:
                        screen = window.screen
                        for area in screen.areas:
                            if area.type == 'VIEW_3D':
                                for region in area.regions:
                                    if region.type == 'WINDOW':
                                        override['window'] = window 
                                        override['screen'] = screen
                                        override['area'] = area
                                        override['region'] = region #'WINDOW'
                                        override['object'] = armature
                                        override['active_object'] = armature
                                        override['pose_object'] = armature
                                        override['region_data'] = region
                                        override['workspace'] = window.workspace
                                        break

                    try:
                        # Go to object mode to switch armature
                        bpy.ops.object.mode_set(override, mode='OBJECT')
                        
                        # Deselect everything
                        bpy.ops.object.select_all(override, action='DESELECT')
                    except:
                        print('======= REPORT ========')
                        for ctx, val in override.items():
                            print(ctx, val)
                        error = True
                        fail = True
                        msg += "Failed to pass in object mode. Try to launch a session in object mode * "
                        continue

                    bpy.context.view_layer.objects.active = armature
                    armature.select_set(True)

                    override = bpy.context.copy()

                    for window in bpy.context.window_manager.windows:
                        screen = window.screen
                        for area in screen.areas:
                            if area.type == 'VIEW_3D':
                                for region in area.regions:
                                    if region.type == 'WINDOW':
                                        override['window'] = window 
                                        override['screen'] = screen
                                        override['area']   = area
                                        override['object'] = armature
                                        override['active_object'] = armature
                                        override['region'] = region #'WINDOW'
                                        override['region_data'] = region
                                        override['workspace'] = window.workspace
                                        break

                    # Go back in pose mode for the poll of the addon
                    try:
                        bpy.ops.object.mode_set(override, mode='POSE')

                        # Selected bones are assigned to new groups, we deselect everything
                        override['mode'] = 'POSE'
                        bpy.ops.pose.select_all(override, action='DESELECT')

                    except:
                        error = True
                        fail = True
                        msg += "Failed to pass in pose mode. Fixing aborted for armature [{}] * ".format(key)
                        continue

                    bpy.ops.jueg.import_from_file(override, filepath=FILE)

                    names = []
                    if item[0] == 'MISSING':
                        names = ["[{}]".format(o) for o in self.list_groups]
                    else:
                        names = ["[{}]".format(o) for o in item]
                    msg += "Extra Groups {} added on the armature [{}] * ".format(", ".join(names), key)

            for window in bpy.context.window_manager.windows:
                screen = window.screen
                for area in screen.areas:
                    if area.type == 'VIEW_3D':
                        for region in area.regions:
                            if region.type == 'WINDOW':
                                override['window'] = window 
                                override['screen'] = screen
                                override['area'] = area
                                override['region'] = region #'WINDOW'
                                override['object'] = armature
                                override['active_object'] = armature
                                override['pose_object'] = armature
                                override['region_data'] = region
                                override['workspace'] = window.workspace
                                break

            try:
                # Go to object mode to switch armature
                bpy.ops.object.mode_set(override, mode='OBJECT')
                
                # Deselect everything
                bpy.ops.object.select_all(override, action='DESELECT')
            except:
                print('======= REPORT ========')
                for ctx, val in override.items():
                    print(ctx, val)
                error = True
                fail = True
                msg += "Failed to go back in object mode after tests "
                

            if error:
                return False, msg
            else:
                return True, msg



        def test(self, context):
            armatures = [o for o in bpy.data.objects if o.type == 'ARMATURE']

            for arma in armatures:
                if len(arma.jueg_grouptypelist) == 0:
                    self.missing[arma.name] = ['MISSING']
                elif len(arma.jueg_grouptypelist) > 1:
                    self.missing[arma.name] = ['EXCESS']
                    for item in arma.jueg_grouptypelist:
                        self.missing[arma.name].append(item.name)
                else:
                    self.missing[arma.name] = list(self.list_groups)
                    # If a group is found, we remove it from the list of missing groups
                    # Only groups remaining by the end are the missing ones
                    for grp in arma.jueg_grouptypelist[0].group_ids:
                        try:
                            self.missing[arma.name].remove(grp.name)
                        except:
                            pass

            error = False
            msg = ""

            for key, item in self.missing.items():
                if len(item) > 0:
                    error = True

                    if item[0] == 'MISSING':
                        msg += "The armature [{}] doesn't have extra bone groups *".format(key)

                    elif item[0] == 'EXCESS':
                        names = ["[{}]".format(o) for o in item[1:]]
                        msg += "The armature [{}] should have only one set of extra groups, found {} *".format(key, ", ".join(names))

                    else:
                        names = ["[{}]".format(o) for o in item]
                        msg += "Extra bones group(s): {} are missing on the armature [{}]".format(", ".join(names), key)

            if error:
                return False, msg
            else:
                return True, "All required extra bones groups have been found"




    class TestExtraGroupsInstalled(QATest):
        
        """ Is the [extra groups] addon installed and enabled ? """

        addons = {}
        extra_groups = {}
        name = ""

        def relevant_for(self, context):
            if context['category'] == 'chars':
                return True, "Working on a char"
            else:
                return False, "We are not working on a character"


        def get_sub_test_types(self):
            return [TestAllGroupsPresent]


        def necessary(self):
            return True


        def build_context(self, context):
            self.addons = get_all_addons()

            if (self.addons is None) or (len(self.addons) == 0):
                return False, "Failed to build addons list"
            else:
                return True, "Context built"


        def can_fix(self, context):
            if self.name != "":
                return True, "Will activate"
            else:
                return False, "Can't fix"


        def fix(self, context):
            mod = bpy.ops.preferences.addon_enable(module=self.name)
            if 'FINISHED' not in mod :
                return False, "Failed to activate module"
            else:
                return True, "Addon [{}] has been enabled".format(self.name)


        def test(self, context):

            # The name of the addon changes according how you installed it (extra_groups <-> ExtraGroups <-> Extra-Groups)
            found = False
            name = ""

            for key, item in self.addons.items():
                key_format = key.lower().replace('_', '').replace('-', '')
                
                if key_format == 'extragroups':
                    self.name = key
                    found = True
                    self.extra_groups = item
                    context['extra_groups'] = self.name
                    break

            if found:

                if self.extra_groups['is_loaded'] and self.extra_groups['is_enabled']:
                    return True, "[Extra Groups] addon found and activated"
                else:
                    return False, "Addon [Extra Groups] found but not activated"
            else:
                return False, "Addon [Extra Groups] is missing. Install it via the [Package Manager]. *This addon requires a token, get it on <a target=\"_blank\" href=\"https://wiki.thesiren.cloud/link/10#bkmrk-hidden-add-ons\">the wiki</a>."


    return [TestExtraGroupsInstalled]