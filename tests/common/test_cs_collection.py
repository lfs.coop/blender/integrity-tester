from ..sqarf.qatest import QATest
import bpy

from ..utils.visibility import test_requirements, TestVisibility
from ..utils.general import locate_collection, correct_collection

def test_cs_collection():

    class TestNameContentCS(QATest):

        """ Do all elements from the [asset_name_cs] collection have a name starting with [cs_] """

        bad_naming = set()
        old_names = []
        desired_name = ""
        cs_collection = None

        def build_context(self, context):
            self.desired_name = context['name_cs_collection']
            self.cs_collection = bpy.data.collections[self.desired_name]
            return True, "Context built"

        def can_fix(self, context):
            return True, "Will rename elements to respect naming convention"

        def fix(self, context):
            new_names = []

            for item in self.bad_naming:
                name = item.name.lower().replace('-', '_').replace(' ', '_').replace("cs_", "").replace("_cs", "").split('.')[0]
                name = "cs_" + name
                item.name = name
                new_names.append("["+name+"]")
                

            return True, "Item(s) {} from [{}] collection renamed to match naming convention * New name(s): {}".format(", ".join(self.old_names), self.desired_name, ", ".join(new_names))

        def test(self, context):
            
            self.bad_naming  = set([obj for obj in self.cs_collection.objects if not obj.name.startswith("cs_")])
            self.bad_naming = self.bad_naming.union(set([obj for obj in self.cs_collection.objects if len(obj.name.split('.')) > 1 and obj.name.split('.')[-1].isnumeric()]))

            if(len(self.bad_naming) == 0):
                return True, "Naming convention respected inside [{}] collection".format(self.desired_name)
            else:
                self.old_names = ["["+obj.name+"]" for obj in self.bad_naming]
                return False, "* Items name inside [{}] collection should start with [cs_] * Found {}".format(self.desired_name, ", ".join(self.old_names))


    class TestUselessCS(QATest):

        """ Is there some unused custom shapes in the scene ? """

        all_cs = set()
        cs_unused = set()
        names_unused = []
        desired_name = ""
        cs_collection = None

        def build_context(self, context):
            self.desired_name = context['name_cs_collection']
            self.cs_collection = bpy.data.collections[self.desired_name]
            return True, "Context built"

        def get_sub_test_types(self):
            return [TestNameContentCS]

        def can_fix(self, context):
            return True, "Unused custom shapes are present"

        def fix(self, context):
            for cs in self.cs_unused:
                bpy.data.objects.remove(cs)
            return True, "{} were removed, they were unused".format(", ".join(self.names_unused))

        def test(self, context):

            #armatures_ref = set()
            #shape_bone = set()

            #meshes = set([o for o in bpy.data.objects if (o.type == 'MESH')]) # All mesh objects from the scene
            cs_objects = self.cs_collection.objects  # All meshes in the cs collection
            self.all_cs = set([o for o in cs_objects if (o.type == 'MESH')]) # Same as cs_objects but as a Set
            armatures = [o for o in bpy.data.objects if o.type == 'ARMATURE']
            cs_found = set()
            #meshes = meshes - self.all_cs # We keep meshes that are not in the cs collection

            for arm in armatures:
                if arm.pose is not None:
                    for bone in arm.pose.bones:
                        cs_found.add(bone.custom_shape)

            self.cs_unused = self.all_cs - cs_found

            if len(self.cs_unused) > 0:
                #self.cs_unused = self.all_cs - shape_bone
                self.names_unused = ["["+o.name+"]" for o in self.cs_unused]

                return False, "Some custom shapes are unused: {}".format(", ".join(self.names_unused))
            else:
                return True, "All custom shapes are being used"


    class TestVisibilityCS(TestVisibility):
        
        """ Does the collection [asset_name_cs] respect visibility and selectability conventions ? """

        requirements = {'hide_viewport': True, 'hide_render': True, 'hide_select': True}
        collection_type = "cs"


    class TestVisiCSItems(QATest):

        """ Checks that everything in [asset_name_cs] is visible in viewport and render """

        cs_collection = None
        errors = []
        names = []

        def get_sub_test_types(self):
            return [TestVisibilityCS]

        def build_context(self, context):
            self.cs_collection = bpy.data.collections.get(context['name_cs_collection'])

            if self.cs_collection is None:
                return False, "Failed to build context. [{}] collection is missing".format(context['name_cs_collection'])
            else:
                return True, "Context built"


        def can_fix(self, context):
            return True, "Will change visibility"


        def fix(self, context):
            for error in self.errors:
                error.hide_render = False
                error.hide_viewport = False
            return True, "Individual items in [{}] collection should be visible. They now are {}.".format(context['name_cs_collection'], ", ".join(self.names))


        def test(self, context):
            for item in self.cs_collection.objects:
                if item.hide_viewport != item.hide_render:
                    self.errors.append(item)
                    self.names.append("["+item.name+"]")

            if len(self.errors) == 0:
                return True, "All CS elements have a correct visibility"
            else:
                return False, "Some individual items should be visible in viewport: {}".format(", ".join(self.names))



    class TestMaterialCS(QATest):

        """ Do some items from [asset_name_cs] collection have materials ? """

        desired_name = ""
        cs_collection = None
        fix_cs_materials = []

        def prerequisites(self, context):
            if bpy.data.collections.get(context['name_cs_collection']) is None:
                return False, "[{}] collection missing".format(context['name_cs_collection'])

            return True, "Prerequisites met"

        def build_context(self, context):
            self.desired_name = context['name_cs_collection']
            self.cs_collection = bpy.data.collections.get(self.desired_name)
            return True, "Context built"

        def can_fix(self, context):
            return True, "Materials will be removed"

        def fix(self, context):
            names = []

            for item in self.fix_cs_materials:
                item.data.materials.clear()
                names.append("["+item.name+"]")

            return True, "Materials have been removed from object(s) {} in the [{}] collection".format(", ".join(names), self.desired_name)

        def test(self, context):
            errors =  []

            for item in self.cs_collection.objects:
                if item.data:
                    if len(item.data.materials) > 0:
                        errors.append(item)

            if len(errors) > 0:
                self.fix_cs_materials = errors
                return False, "Items in the [{}] collection are not supposed to have materials".format(self.desired_name)
            else:
                return True, "No material was found on an item from the [{}] collection".format(self.desired_name)



    class TestBringBackCS(QATest):

        """ Tries to find every [cs_] object and bring it in the collection """

        collection = None
        objects = []
        armatures = []
        other_cs = []
        names = []

        def get_sub_test_types(self):
            return [TestUselessCS]

        def build_context(self, context):
            self.collection = bpy.data.collections.get(context['name_cs_collection'])
            if self.collection is None:
                return False, "Failed to build context. [{}] collection is missing".format(context['name_cs_collection'])

            self.objects = set([o for o in bpy.data.objects if (o.name.lower().startswith('cs_')) and (o.name not in self.collection.objects)])
            self.names = ["["+o.name+"]" for o in self.objects]
            self.armatures = [a for a in bpy.data.objects if a.type == 'ARMATURE']

            for armature in self.armatures:
                for bone in armature.pose.bones:
                    if (bone.custom_shape is not None) and (bone.custom_shape.name not in self.collection.objects):
                        self.other_cs.append(bone.custom_shape)
            self.other_cs = set(self.other_cs)
            self.names.extend(["["+o.name+"]" for o in self.other_cs])

            return True, "Context built"

        def can_fix(self, context):
            return True, "Will move inside correct collection"

        def fix(self, context):
            for o in self.objects.union(self.other_cs):
                parent = None
                for c in bpy.data.collections:
                    if o.name in c.objects:
                        parent = c
                        break
                if parent:
                    parent.objects.unlink(o)
                self.collection.objects.link(o)

            return True, "Objects {} have been moved inside the [{}] collection".format(", ".join(self.names), context['name_cs_collection'])


        def test(self, context):
            if (len(self.objects) == 0) and (len(self.other_cs) == 0):
                return True, "No misplaced object"
            else:
                return False, "Some custom shapes are misplaced: {}".format(self.names)




    class TestCSCollection(QATest):

        """ Is there a [asset_name_cs] collection, present as children of the [asset_name] collection """

        main_collection = None
        desired_name = ""
        fix_cs_collection = ""
        cs_collection = ""
        flag = ''

        def necessary(self):
            return True

        def prerequisites(self, context):
            if context.get('name_cs_collection') is None:
                return False, "Name of [asset_name_cs] collection not defined"
            else:
                return True, "Prerequisites met"

        def build_context(self, context):
            self.desired_name = context['name_cs_collection']
            self.main_collection = bpy.data.collections.get(context['name_asset'])

            if self.main_collection is None:
                return False, "Failed in building context [{}] collection missing".format(context['name_asset'])

            return True, "Context built"


        def get_sub_test_types(self):
            return [TestBringBackCS, TestMaterialCS, TestVisiCSItems]


        def can_fix(self, context):
            names = ['cs', 'custom', 'shape']

            status, self.cs_collection = locate_collection(self.desired_name, names)
            
            if status:
                return True, "Can fix"
            else:
                return False, "Can't fix"


        def fix(self, context):
            old_name = correct_collection(self.desired_name, self.cs_collection, self.flag, self.main_collection)
            return True, "[{}] collection is now called [{}] and is child of the [{}] collection".format(old_name, self.desired_name, self.main_collection.name)

        def test(self, context):

            sub_colls = [c.name for c in self.main_collection.children]

            if self.desired_name in sub_colls:
                return True, "[{}] collection correctly placed and named".format(self.desired_name)
            else:
                return False, "[{}] collection not found".format(self.desired_name)

    return [TestCSCollection]
