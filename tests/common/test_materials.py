import re
from ..sqarf.qatest import QATest
import bpy
from ..utils.compare import compare_trees

regex_name = "^[a-z0-9_]+$"


"""
import bpy

obj = bpy.data.objects["door_C_001_DMSH"]

objs_touched = {}
objs_touched.setdefault(obj, {})
if obj.material_slots is not None:
    for index, s_mat in enumerate(obj.material_slots):
        # If the slot is empty, it's automatically useless
        if s_mat.material is None:
            print('NONE')
        else:
            objs_touched[obj].setdefault(s_mat.material.name, {'active': -1, 'useless': set()})
            # If we already met this material among the slots of this object
            if objs_touched[obj][s_mat.material.name]['active'] > -1:
                objs_touched[obj][s_mat.material.name]['useless'].add(index)
                correct = False
            else:
                objs_touched[obj][s_mat.material.name]['active'] = index
                
print(objs_touched)
"""


def material_is_used_by(material):
    objs = []

    for o in bpy.data.objects:
        for slot in o.material_slots:
            if slot.material == material:
                objs.append(o)

    return objs


def has_transparent_shader(material):
    if material.use_nodes:
        for node in material.node_tree.nodes:
            if node.type == 'BSDF_TRANSPARENT':
                return True
        return False
    else:
        return False


def test_materials():
    class TestExcludeMaterial(QATest):
        EXCLUDE = ["Dots Stroke", "dots_stroke", "Render Result", "render_result"]


    class TestDuplicatedSlots(QATest):

        """ Are materials duplicated over objects ? """

        unused = {}
        objs_touched = {}
        errors = set()

        def can_fix(self, context):
            return True, "Will rebind objects indices"

        def fix(self, context):
            for obj, slot_usage in self.objs_touched.items():
                for poly in obj.data.polygons:
                    for mat_name, idx_slot in slot_usage.items():
                        if poly.material_index in idx_slot['useless']:
                            poly.material_index = idx_slot['active']

            try:
                bpy.ops.object.mode_set(mode='OBJECT')
            except:
                return True, f"Materials duplicated over several slots have been remapped on objects {', '.join(self.errors)}"

            for obj, slot_usage in self.objs_touched.items():
                for mat_name, idx_slot in slot_usage.items():
                    rev = idx_slot['useless']
                    if obj in self.unused:
                        rev = rev.union(self.unused[obj])
                    rev = list(rev)
                    rev.sort()
                    rev.reverse()
                    for idx_useless in rev:
                        obj.active_material_index = idx_useless
                        bpy.ops.object.material_slot_remove({'object': obj})

            return True, f"Materials duplicated over several slots have been remapped on objects {', '.join(self.errors)}. Unused slots have been removed."


        def test(self, context):

            correct = True

            for obj in bpy.data.objects:
                if obj.type == 'MESH':
                    self.objs_touched.setdefault(obj, {})
                    if obj.material_slots is not None:
                        for index, s_mat in enumerate(obj.material_slots):
                            # If the slot is empty, it's automatically useless
                            if s_mat.material is None:
                                self.unused.setdefault(obj, set())
                                self.unused[obj].add(index)
                                self.errors.add(f"[{obj.name}]")
                                correct = False
                            else:
                                self.objs_touched[obj].setdefault(s_mat.material.name, {'active': -1, 'useless': set()})
                                # If we already met this material among the slots of this object
                                if self.objs_touched[obj][s_mat.material.name]['active'] > -1:
                                    self.objs_touched[obj][s_mat.material.name]['useless'].add(index)
                                    self.errors.add(f"[{obj.name}]")
                                    correct = False
                                else:
                                    self.objs_touched[obj][s_mat.material.name]['active'] = index

            if correct:
                return True, "No material is being duplicated among several slots"
            else:
                return False, f"On objects {', '.join(self.errors)} some materials are being duplicated over several slots."



    class TestUniqueMaterials(TestExcludeMaterial):

        """ Is the rule of one texture == one material respected ? """

        unique_materials = {}

        def get_sub_test_types(self):
            return [TestDuplicatedSlots]

        def necessary(self):
            return True

        def can_fix(self, context):
            return True, "Duplicated materials will be remapped and deleted"

        def fix(self, context):
            output = []
            for key, item in self.unique_materials.items():
                if len(item) > 0:
                    removed = []
                    for m in item:
                        m.user_remap(key)
                        removed.append(f"[{m.name}]")
                        bpy.data.materials.remove(m)
                    joined = ", ".join(removed)
                    output.append(f"* Materials {joined} have been replaced by [{key.name}]")
            return True, " ".join(output)

        def test(self, context):
            correct = True
            for material in bpy.data.materials:
                if material.name not in self.EXCLUDE:
                    found = False
                    for key in self.unique_materials.keys():
                        if compare_trees(material, key):
                            found = True
                            correct = False
                            self.unique_materials[key].append(material)
                            break
                    if not found:
                        self.unique_materials.setdefault(material, [])

            if not correct:
                formated_out = ""
                for key, item in self.unique_materials.items():
                    formated_out += f"* The material [{key.name}] is replicated in {', '.join([f'[{m.name}]' for m in item])}"
                return False, formated_out
            else:
                return True, "All materials are unique"


    class TestNamingMaterial(TestExcludeMaterial):

        """ Materials names must not contain any character that could make crash the Alambic export """

        def can_fix(self, context):
            return True, "Will remove all unallowed characters from the names"

        def fix(self, context):
            fixed = []
            for name in context["err_name_materials"]:
                correct = re.sub('[^a-z0-9_]+', '_', name.lower())
                bpy.data.materials[name].name = correct
                fixed.append("([{}] to [{}])".format(name, correct))

            return True, "All name fixed: {}".format(", ".join(fixed))

        def test(self, context):
            errors = []

            for m in bpy.data.materials:
                if m.name not in self.EXCLUDE:
                    res = re.search(regex_name, m.name)
                    if(res == None):
                        errors.append(m.name)

            if(len(errors) == 0):
                return True, "All materials have a correct name"
            else:
                context["err_name_materials"] = errors
                return False, "Some materials have a name that will cause errors: {}".format(", ".join(errors))

    class TestEmissiveMaterial(TestExcludeMaterial):

        """ All materials should have either a Emissive node or an Emission input in a Principled BSDF """

        def test(self, context):
            errors = []

            rig_ui = bpy.data.objects.get("rig_ui")
            safe = []
            users = {}

            if rig_ui is None:
                safe = []
            else:
                safe = list(rig_ui.children)

            for o in safe:
                for slot in o.material_slots:
                    if (slot is not None) and (slot.material is not None):
                        name = slot.material.name

                        if name in users:
                            users[name] += 1
                        else:
                            users[name] = 1

            for m in bpy.data.materials:
                if m.name not in self.EXCLUDE:
                    if m.name.startswith("cs_"):
                        objs = material_is_used_by(m)
                        bad_users = [o for o in objs if (not o.name.startswith('cs_'))]

                        if m.name in users:
                            if m.users == users[m.name]:
                                continue

                        if len(bad_users) == 0:
                            continue

                    if m.use_nodes:
                        if "Emission" in m.node_tree.nodes:
                            if m.node_tree.nodes['Emission'].inputs['Strength'].default_value == 0:
                                errors.append("[{}]".format(m.name))
                        elif "Principled BSDF" in m.node_tree.nodes:
                            if not m.node_tree.nodes['Principled BSDF'].inputs['Emission'].is_linked:
                                errors.append("[{}]".format(m.name))
                        else:
                            pass
                    # else:
                    #     errors.append("[{}]".format(m.name))

            if(len(errors) == 0):
                return True, "All materials are emissive"
            else:
                return False, "* Materials {} are not emissive and not used by an object from the cs collection* They will show up totally black at rendering".format(", ".join(errors))

    class TestPolkaMaterial(TestExcludeMaterial):

        """ Is there Polka dots in some materials ? """

        def test(self, context):
            polkas = []

            for m in bpy.data.materials:
                if m.use_nodes:
                    for n in m.node_tree.nodes:
                        if n.name == "Polka":
                            polkas.append("[{}]".format(m.name))

            if(len(polkas) == 0):
                return True, "No Polka node found in any material"
            else:
                self.raise_flag('warning')
                return True, "Polka nodes found in materials: {}".format(", ".join(polkas))

    class TestBlendingModeMaterial(TestExcludeMaterial):
        
        """ All materials are in blend mode 'OPAQUE' """

        def can_fix(self, context):
            return True, "The blend moed will be changed"

        def fix(self, context):
            for name in context["errors_blend_mode"]:
                print('NAME: ', name)
                bpy.data.materials[name].blend_method = 'OPAQUE'
            names = ["["+o+"]" for o in context["errors_blend_mode"]]

            return True, "Blending method of some materials was changed: {}".format(", ".join(names))

        def test(self, context):
            errors = []
            warning = []

            for m in bpy.data.materials:
                if m.name not in self.EXCLUDE:
                    if m.name.endswith('_oalpha') or has_transparent_shader(m):
                        if m.blend_method != 'BLEND':
                            warning.append(m.name)
                    elif m.blend_method != 'OPAQUE':
                        errors.append(m.name)

            if(len(errors) == 0):
                if len(warning) == 0:
                    return True, "All materials are in OPAQUE blending mode"
                else:
                    self.raise_flag('warning')
                    warnings = ["[{}]".format(o) for o in warning]
                    return True, "All regular materials are in blending mode 'OPAQUE' * Some materials with [_oalpha] in their name are not in [Alpha Blending]: {}".format(", ".join(warnings))
            else:
                context["errors_blend_mode"] = errors
                return False, "Some materials don't have the correct blend mode: {}".format(", ".join(errors))

    class TestMaterials(QATest):
        def get_sub_test_types(self):
            return [TestUniqueMaterials, TestNamingMaterial, TestEmissiveMaterial, TestPolkaMaterial, TestBlendingModeMaterial]

    return [TestMaterials]
