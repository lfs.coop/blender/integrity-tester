import bpy
from ..sqarf.qatest import QATest
import numpy as np

layer_left_side = set(np.arange(0, 8)).union(set(np.arange(16, 24)))

def test_bones_naming():

    class TestNameBones(QATest):

        """ Are names of bones consistent ? """

        armatures = []
        warnings = {}
        errors = {}

        def build_context(self, context):
            self.armatures = [o for o in bpy.data.objects if (o.type == 'ARMATURE')]
            return True, "Context built"

        def fix(self, context):
            return False, "Not implemented yet"

        def can_fix(self, context):
            return False, "Not implemented yet"

        def test(self, context):
            for armature in self.armatures:
                if armature.pose is not None:
                    self.warnings[armature.name] = []
                    self.errors[armature.name] = []

                    for bone in armature.pose.bones:
                        if bone.name.startswith("Bone"):
                            self.raise_flag('warning')
                            self.warnings[armature.name].append("[{}]".format(bone.name))

                        if bone.name.split('.')[-1].isnumeric():
                            self.raise_flag('warning')
                            self.warnings[armature.name].append("[{}]".format(bone.name))

                        is_left_side = False
                        for i, layer in enumerate(bone.bone.layers):
                            if layer:
                                if i in layer_left_side:
                                    is_left_side = True
                                    break
                        if is_left_side and (not bone.name.startswith('c_')):
                            self.errors[armature.name].append("[{}]".format(bone.name))


            error = False
            warning = False

            for arma, results in self.errors.items():
                if len(results) > 0:
                    self.lower_flag('warning')
                    error = True
                    break

            for arma, result in self.warnings.items():
                if len(result) > 0:
                    warning = True
                    break

            msg = ""
            if error:
                for arma, results in self.errors.items():
                    msg_arma = "<b>In armature [{}]</b>: ".format(arma)
                    one = False
                    if len(results) > 0:
                        msg_arma += " * Bones on left side layers {} should have a name starting with [c_]".format(", ".join(results))
                        one = True

                    if one:
                        msg += " * " + msg_arma

                return False, msg

            elif warning:
                for arma, results in self.warnings.items():
                    msg_arma = "<b>In armature [{}]</b>: ".format(arma)
                    one = False
                    if len(results) > 0:
                        msg_arma += " * Bones {} shouldn't begin with [Bone] or end with [.123]".format(", ".join(results))
                        one = True

                    if one:
                        msg += msg_arma

                return True, msg

            else:
                return True, "Bones correctly named after their location"


    return [TestNameBones]