from ..sqarf.qatest import QATest
import bpy
from math import isclose
from mathutils import Vector
from mathutils import Matrix

def mirrored(name1, name2):
    if len(name1) != len(name2):
        return False
    else:
        differences = []
        for a, b in zip(name1.lower(), name2.lower()):
            if (a == b):
                pass
            elif (a == 'r' and b == 'l') or (a == 'l' and b == 'r'):
                differences.append('$')
            else:
                return False
        status = "".join(differences)
        return status == "$"

def find_mirrored(name, list_names):
    for item in list_names:
        if mirrored(name, item):
            return item
    return None

def check_bones_vertex_groups(state_bones):
    assessment = {}

    for armature, users in state_bones.items():
        if armature.pose is None:
            continue
        
        all_bones = [b.name for b in armature.pose.bones]
        assessment.setdefault(armature, set())

        for user in users:
            dico_group = [group.name for group in user.vertex_groups]
            for vertex in user.data.vertices: # We proceed through vertices to ignore the deform if the (weight == 0)
                for group in vertex.groups:
                    if (group.group < len(dico_group)) and (dico_group[group.group] in all_bones):
                        if (group.weight > 0.0) and (armature.data.bones[dico_group[group.group]].use_deform):
                            bone_name = dico_group[group.group]
                            assessment[armature].add(bone_name)

                            m = find_mirrored(bone_name, all_bones)
                            if m is not None:
                                assessment[armature].add(m) 

    return assessment
                        


def check_matrice_value_diff(rest_bone, pose_bone):
    for vector_rest, vector_pos in zip(rest_bone, pose_bone):
        for value_rest, value_pos in zip(vector_rest,vector_pos):
            if isclose(value_rest, value_pos, rel_tol=1e-03):
                return True
    return False


def test_vertices():

    class TestCompaRestPose(QATest):

        """ The position is the same in rest and pose mode """

        errors = []

        def test(self, context):
            armatures = [o for o in bpy.data.objects if o.type == 'ARMATURE']

            for a in armatures:
                for bone in a.pose.bones:
                    if bone.bone.matrix_local != bone.matrix:
                        if check_matrice_value_diff(bone.bone.matrix_local, bone.matrix):
                            self.errors.append("["+bone.name+"]")

            if len(self.errors) == 0:
                return True, "No difference between pose mode and rest mode"
            else:
                return False, "Some bones are different in pose mode and rest mode: {}".format(", ".join(self.errors))


    # From a list of pairs (object, armature) creates a dictionnary of armatures pointing of their users
    def reverse_dict(base_dict):
        final = {}

        for obj, armatures in base_dict.items():
            for arma in armatures:
                final.setdefault(arma, set())
                final[arma].add(obj)

        return final


    class TestDeformBones(QATest):

        """ Each vertex has a weight and weights are normalized """

        state_bones = {}
        errors = []
        names_errors = []

        def build_context(self, context):
            # Vector of meshes 
            meshes = [o for o in bpy.data.objects if (o.type == 'MESH') and (not o.name.startswith("cs_"))]
            armatures_ref = {}

            # All armatures pointed by [armatures modifiers] bound to meshes, and the mesh itself
            for o in meshes:
                for m in o.modifiers:
                    if m.type == 'ARMATURE':
                        if (m.object is not None) and (m.object.type == 'ARMATURE'):
                            armatures_ref.setdefault(o, set())
                            armatures_ref[o].add(m.object)

            # For each armature, the objects that use it, can't contain None
            self.state_bones = reverse_dict(armatures_ref)
            return True, "Context built"


        def get_sub_test_types(self):
            return [] #[TestCompaRestPose]


        def can_fix(self, context):
            return True, "Will desactivate deformation"


        def fix(self, context):
            for def_bone in self.errors:
                bone = def_bone.bone
                bone.use_deform = False

            return True, "Deformation desactivated for bones: {}".format(", ".join(self.names_errors))


        def test(self, context):
            assessment = check_bones_vertex_groups(self.state_bones) # Processes the list of bones that use deform legitimately

            for armature, approved in assessment.items():
                armature.data.show_axes = False
                for bone in armature.pose.bones:
                    if (bone.bone.use_deform) and (bone.name not in approved):
                        self.errors.append(bone)
                        self.names_errors.append("[{}]".format(bone.name))

            self.errors = set(self.errors)
            self.names_errors = set(self.names_errors)

            if len(self.errors) > 0:
                return False, "Some weights are uncorrect on bones: {}".format(", ".join(self.names_errors))
            else:
                return True, "Bones weights are correct"


    return [TestDeformBones]