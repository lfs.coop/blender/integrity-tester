import bpy
from ..sqarf.qatest import QATest
from ..utils.visibility import test_requirements, TestVisibility

def test_deform_collection():

	class TestVisibilityDeform(TestVisibility):
		
		""" The collection [asset_name_deform] respects visibility and selectability convention """

		requirements = {'hide_viewport': True, 'hide_render': True, 'hide_select': True}
		collection_type = "deform"


	class TestDrivenScale(QATest):
		
		""" Have [CorrectiveSmooth] modifiers their scale driven ? """

		deform = None

		def build_context(self, context):
			main = bpy.data.collections.get(context['name_asset'])

			if main is None:
				return False, "Error, main collection not found"
			else:
				self.deform = main.children.get(context['name_deform_collection'])

				if self.deform is None:
					return False, "No [{}] detected".format(context['name_deform_collection'])
				else:
					return True, "[{}] collection detected".format(context['name_deform_collection'])

		def test(self, context):
			errors = []

			for obj in self.deform.objects:
				for m in obj.modifiers:
					if m.type == 'CORRECTIVE_SMOOTH':
						if obj.animation_data is not None:
							driver = obj.animation_data.drivers.find('modifiers["{}"].scale'.format(m.name))
							if driver is None:
								errors.append((m.name, obj.name))
						else:
							errors.append((m.name, obj.name))

			names = ["[{}] from [{}]".format(o[0], o[1]) for o in errors]

			if len(errors) == 0:
				return True, "All CorrectiveSmooth modifiers from [{}] have their scale driven".format(context['name_deform_collection'])
			else:
				return False, "In the [{}] collection, all CorrectiveSmooth modifiers should have their scale driven. *It's not the case for: {}".format(context['name_deform_collection'], ", ".join(names))

	class TestDeformCollection(QATest):
		
		""" Is there an [asset_name_deform] collection in the scene ? """

		def get_sub_test_types(self):
			return [TestVisibilityDeform, TestDrivenScale]

		def necessary(self):
			return True

		def relevant_for(self, context):
			main = bpy.data.collections.get(context['name_asset'])

			if main is None:
				return False, "Error, main collection not found"
			else:
				deform = main.children.get(context['name_deform_collection'])

				if deform is None:
					return False, "No [{}] detected".format(context['name_deform_collection'])
				else:
					return True, "[{}] collection detected".format(context['name_deform_collection'])

	return [TestDeformCollection]