from ..sqarf.qatest import QATest
import bpy
from ..utils.general import bfs, tree_from_collection, locate_collection, correct_collection
from ..utils.visibility import test_requirements, TestVisibility

def test_low_collection():

    class TestGeometryInLow(QATest):

        """ The [asset_name_low] collection contains only geometry """

        desired_name = ""
        name_collection = ""
        low_collection = None
        content = []
        errors = []

        def prerequisites(self, context):
            if bpy.data.collections.get(context['name_low_collection']) is None:
                return False, "[{}] collection not found".format(context['name_low_collection'])

            return True, "Prerequisites met"

        def build_context(self, context):
            self.name_collection = context['name_low_collection']
            self.low_collection = bpy.data.collections.get(self.name_collection)
            try:
                self.content = tree_from_collection(self.low_collection)
            except:
                return False, "Failed to build [{}] collection tree".format(self.name_collection)

            return True, "Context built"


        def test(self, context):
            self.content = tree_from_collection(self.low_collection, True)

            if len(self.content) == 0:
                self.raise_flag('warning')
                return True, "The [{}] collection is empty".format(self.name_collection)

            for item in self.content:
                if bpy.data.objects[item].type != 'MESH':
                    if bpy.data.objects[item].type != 'EMPTY':
                        self.errors.append(item)
                    else:
                        if "eye_" not in item:
                            self.errors.append(item)

            if len(self.errors) == 0:
                return True, "[{}] collection contains only geometry"
            else:
                self.raise_flag('warning')
                names = ["["+item+"]" for item in self.errors]
                return True, "Only geometry and empties for eyes should be in the [{}] collection *Found {}".format(self.name_collection, ", ".join(names))



    class TestVisibilityLow(TestVisibility):

        """ Does the collection [asset_name_low] respect visibility and selectability conventions ? """

        requirements = {'hide_viewport': True, 'hide_render': True, 'hide_select': True}
        collection_type = "low"



    class TestLowCollection(QATest):

        """ Is there an [asset_name_low] collection in the scene ? """

        low_collection = None
        main_collection = None
        desired_name = ""
        flag = ''
        fix_low_collection = ""

        def get_sub_test_types(self):
            return [TestVisibilityLow, TestGeometryInLow]


        def necessary(self):
            return True


        def relevant_for(self, context):
            if (context['phase'] == 'ANIM') and (context['category'] == 'chars'):
                return True, "Relevant for low test"
            else:
                return False, "Irrelevant if not in anim"


        def build_context(self, context):
            self.main_collection = bpy.data.collections.get(context['name_asset'])
            self.desired_name = context['name_low_collection']

            if self.main_collection is None:
                return False, "Failed to build context"
            else:
                return True, "Context built"


        def can_fix(self, context):
            names = ['low']

            status, self.low_collection = locate_collection(self.desired_name, names)
            
            if status:
                return True, "Can fix"
            else:
                return False, "Can't fix"


        def fix(self, context):
            old_name = correct_collection(self.desired_name, self.low_collection, self.flag, self.main_collection)
            return True, "[{}] collection is now called [{}] and is child of the [{}] collection".format(old_name, self.desired_name, self.main_collection.name)


        def test(self, context):
            sub_colls = [c.name for c in self.main_collection.children]

            if self.desired_name in sub_colls:
                return True, "Low resolution mesh collection [{}] correctly placed and named".format(self.desired_name)
            else:
                return False, "Low resolution mesh collection [{}] not found".format(self.desired_name)




    return [TestLowCollection]