from ..sqarf.qatest import QATest
import bpy
import numpy

from ..utils.visibility import test_requirements, TestVisibility

def test_visibilities():


	class TestVisibilityLayers(QATest):

		""" Are left-side layers visible, and right-side ones hidden ? """

		name = ""
		armature = None
		visible = list(numpy.concatenate((numpy.arange(0, 8), numpy.arange(16, 24))))
		hidden = list(numpy.concatenate((numpy.arange(8, 16), numpy.arange(24, 32))))
		errors_visi = []
		errors_hid = []

		def build_context(self, context):
			self.name = context['name_rig_collection']
			self.armature = bpy.data.objects.get(self.name)

			if self.armature is None:
				return False, "Failed to build context. [{}] armature is missing".format(self.name)

			self.armature = self.armature.data
			if self.armature is None:
				return False, "Failed to build context. No data in armature [{}]".format(self.name)

			return True, "Context built"


		def can_fix(self, context):
			return True, "Visibility will be switched"


		def fix(self, context):

			for layer in self.errors_hid:
				self.armature.layers[layer] = False
			
			for layer in self.errors_visi:
				self.armature.layers[layer] = True

			return True, "Layers [{}] are now visible * Layers [{}] are now hidden".format(", ".join([str(i) for i in self.errors_visi]), ", ".join([str(i) for i in self.errors_hid]))


		def test(self, context):

			for layer in self.visible:
				if not self.armature.layers[layer]:
					self.errors_visi.append(layer)

			for layer in self.hidden:
				if self.armature.layers[layer]:
					self.errors_hid.append(layer)

			if (len(self.errors_hid) == 0) and (len(self.errors_visi) == 0):
				return True, "No problem in layers visibility"
			else:
				return False, "Layers [{}] should be visible * Layers [{}] should be hidden".format(", ".join([str(i) for i in self.errors_visi]), ", ".join([str(i) for i in self.errors_hid]))



	return [TestVisibilityLayers]
	#list(numpy.concatenate((numpy.arange(0, 8), numpy.arange(16, 24))))

