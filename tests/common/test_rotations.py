import bpy
from ..sqarf.qatest import QATest

import numpy as np

from ..data.special_rotations import special_rotations

def test_rotations():


    class TestSpecialRotations(QATest):

        """ Checks that special bones have have the correct mode of rotation """

        errors_bones = []
        name_rig = ""
        bones = []
        names = []
        fixes = []

        def build_context(self, context):
            self.name_rig = context['name_rig_collection']
            self.armature = bpy.data.objects.get(self.name_rig)

            if self.armature is None:
                return False, "Failed to build context. Armature [{}] is missing".format(self.name_rig)

            self.bones = self.armature.pose.bones

            return True, "Context built"

        def can_fix(self, context):
            return True, "Will change rotation modes according conventions"

        def fix(self, context):
            for bone in self.errors_bones:
                old = bone.rotation_mode
                bone.rotation_mode = special_rotations[bone.name]
                self.fixes.append((bone.name, old, bone.rotation_mode))

            message = ["On [{}] from [{}] to [{}]".format(b[0], b[1], b[2]) for b in self.fixes]

            return True, "Some rotation modes have been changed:* {}".format("*".join(message))

        def test(self, context):

            for b, rotation in special_rotations.items():
                bone = self.bones.get(b)
                if bone is not None:
                    if bone.rotation_mode != rotation:
                        self.errors_bones.append(bone)
                        self.names.append("["+b+"]")

            if len(self.errors_bones) == 0:
                return True, "All rotations modes are correct for special bones"
            else:
                return False, "Some bones have the wrong rotation mode: {}".format(", ".join(self.names))


    class TestAllEuler(QATest):
        
        """ Are all bones in Euler rotation mode ? """

        to_euler = set()
        euler_names = set()

        def get_sub_test_types(self):
            return [TestSpecialRotations]

        def can_fix(self, context):
            return True, "Will change rotation mode to 'XYZ' (Euler)"

        def fix(self, context):
            for bone in self.to_euler:
                bone.rotation_mode = 'XYZ'

            return True, "Rotation mode of following bones was changed to 'XYZ' (Euler): {}".format(", ".join(self.euler_names))

        def test(self, context):
            armatures_ref = set()
            meshes = [o for o in bpy.data.objects if (o.type == 'MESH') and ("cs_" not in o.name)]
            c_layer_bones = set(np.arange(0, 8)).union(set(np.arange(16, 24)))

            for ob in meshes:
                for m in ob.modifiers:
                    if (m.type == 'ARMATURE') and (m.object is not None) and (m.object not in armatures_ref):
                        armatures_ref.add(m.object)
                        bones = m.object.data.bones
                        for bone in bones:
                            if (m.object.pose.bones[bone.name].rotation_mode == 'QUATERNION') and ("proxy" not in bone.name) and (bone.name.startswith("c_")) and ("c_p_" not in bone.name) and ("c_lips_" not in bone.name):
                                for i in c_layer_bones:
                                    if bone.layers[i]:
                                        self.to_euler.add(m.object.pose.bones[bone.name])
                                        self.euler_names.add("["+bone.name+"]")
                                        break
            if len(self.to_euler) == 0:
                return True, "All bones of main rig set to Euler rotation mode"
            else:
                return False, "Some bones are in Quaternion rotation: {}".format(", ".join(self.euler_names))


    return [TestAllEuler]