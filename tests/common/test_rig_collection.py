from ..sqarf.qatest import QATest
import bpy
from mathutils import Vector
import numpy as np

from ..utils.general import bfs, tree_from_collection, locate_collection, correct_collection, rename_obj
from ..utils.visibility import test_requirements, TestVisibility

from .test_bones_naming import test_bones_naming
from .test_actions import test_actions
from .test_vertices import test_vertices
from ..data.bone_layers import layer_bones
from .test_rotations import test_rotations
from ..chars.test_cfist import test_cfist
from ..chars.test_picker import test_picker
from ..chars.test_extra_groups import test_extra_groups
from .test_visibilities import test_visibilities
from .test_rig_properties import test_rig_properties
from ..chars.test_protected_layers import test_protected_layers

right_side = set(np.arange(8, 16)).union(set(np.arange(24, 32)))

def test_rig_collection():

    class TestBrokenConstraints(QATest):

        """ Are some constraints broken on this asset ? """

        def test(self, context):
            errors = {}
            for obj in bpy.data.objects:
                if obj.type == 'ARMATURE':
                    if obj.pose is not None:
                        for bone in obj.pose.bones:
                            for c in bone.constraints:
                                if not c.is_valid:
                                    errors.setdefault(obj.name, {})
                                    errors[obj.name].setdefault(bone.name, [])
                                    errors[obj.name][bone.name].append(c.name)

            if len(errors) == 0:
                return True, "No broken constraint was found"

            else:
                verbose = ""
                for key, item in errors.items():
                    verbose += f"*<b> On armature [{key}]:</b><br/><ul>"
                    for bone, cons in item.items():
                        verbose += f"<li>On bone [{bone}]: {', '.join([f'[{c}]' for c in cons])}</li>"
                    verbose += "</ul>"
                return False, verbose


    class TestProtectedLayers(QATest):

        """ Are all layers on the right side protected ? """

        armature = None
        indices = []

        def build_context(self, context):
            self.armature = bpy.data.objects.get(context['name_rig_collection'])
            if self.armature is None:
                return False, "Failed to build context"
            else:
                return True, "Context built"

        def can_fix(self, context):
            return True, "Will change protectedness"

        def fix(self, context):
            litterals = []
            for index in self.indices:
                self.armature.data.layers_protected[index] = True
                litterals.append("[{}]".format(index))

            return True, "Following layer(s) are now protected: {}".format(", ".join(litterals))

        def test(self, context):
            for idx, layer in enumerate(self.armature.data.layers_protected):
                if (idx in right_side) and (not layer):
                    self.indices.append(idx)

            if len(self.indices) == 0:
                return True, "All layers on the right side are protected"
            else:
                return False, "Layers on the right side should be protected"

    class TestDupliBones(QATest):

        """ Verifies that bones that should be unique are unique """

        unique_bones = []
        armature = None
        errors_found = []

        def build_context(self, context):
            self.armature = bpy.data.objects.get(context['name_rig_collection'])
            if self.armature is None:
                return False, "Failed to build context, armature not found"
            self.unique_bones = set(['mch_bbone_spine'])
            return True, "Context built"

        def can_fix(self, context):
            return True, "Duplicates will be deleted"

        def fix(self, context):
            bpy.ops.object.mode_set(mode='EDIT')
            errors = []
            for error in self.errors_found:
                bone = self.armature.data.edit_bones.get(error)
                self.armature.data.edit_bones.remove(bone)
                errors.append("[{}]".format(error))
            bpy.ops.object.mode_set(mode='OBJECT')
            return True, "Bones {} were removed".format(", ".join(errors))

        def test(self, context):
            for bone in self.armature.pose.bones:
                for test_name in self.unique_bones:
                    if test_name in bone.name:
                        if len(test_name) < len(bone.name):
                            self.errors_found.append(bone.name)

            if len(self.errors_found) > 0:
                errors_verbose = ["[{}]".format(item) for item in self.errors_found]
                return False, "Some bones are detected as duplicates of others: {}".format(", ".join(errors_verbose))
            else:
                return True, "No duplicated bones detected"



    class TestLocationBones(QATest):

        """ Are heads of [c_pos] and [c_traj] bones in location (0, 0, 0) ? """

        name_armature = ""
        armature = None

        def build_context(self, context):
            self.name_armature = context['name_rig_collection']
            self.armature = bpy.data.objects.get(self.name_armature)

            if self.armature is None:
                return False, "Failed to build context. Armature [{}] is missing".format(self.name_armature)

            return True, "Context built"


        def get_sub_test_types(self):
            return test_cfist()


        def test(self, context):
            c_pos = self.armature.pose.bones['c_pos'].head == Vector((0.0, 0.0, 0.0))
            c_traj = self.armature.pose.bones['c_traj'].head == Vector((0.0, 0.0, 0.0))

            if c_pos and c_traj:
                return True, "Heads of bones [c_pos] and [c_traj] are in (0.0, 0.0, 0.0)"
            else:
                return False, "Heads of bones [c_pos] and [c_traj] should be in (0.0, 0.0, 0.0)"


    class TestBonesLayers(QATest):

        """ Are each bones in their correct layer ? """

        name_armature = ""
        armature = None
        bones = []
        layers_to_true = []
        layers_visibility = []

        def build_context(self, context):
            self.name_armature = context['name_rig_collection']
            self.armature = bpy.data.objects.get(self.name_armature)

            if self.armature is None:
                return False, "Failed to build context, [{}] armature missing".format(self.name_armature)
            else:
                if self.armature.pose:
                    self.bones = self.armature.pose.bones
                else:
                    return False, "Failed to build context, no pose in [{}] armature".format(self.name_armature)

            return True, "Context built"

        def get_sub_test_types(self):
            return [TestLocationBones] + test_rotations() + test_extra_groups() + test_bones_naming() + test_protected_layers()

        def can_fix(self, context):
            if self.armature:
                return True, "Will change bones layers and visibility"
            else:
                return False, "Armature not found"

        def fix(self, context):
            names_moved = []
            names_visi = []

            for bone in self.layers_to_true:
                names_moved.append("["+bone[0]+"]")
                self.armature.data.bones[bone[0]].layers[bone[1]] = True
                layers = set(np.arange(32)) - set([bone[1]])
                for i in layers:
                    self.armature.data.bones[bone[0]].layers[i] = False

            layers_visi = set(np.arange(32)) - set([15])
            for bone in self.layers_visibility:
                names_visi.append("["+bone.name+"]")
                self.armature.data.bones[bone.name].layers[15] = True
                for lay in layers_visi:
                    self.armature.data.bones[bone.name].layers[lay] = False

            return True, "The layer of the following bones was changed: {} * The visibility of following bones was changed: {}".format(", ".join(names_moved), ", ".join(names_visi))

        def test(self, context):

            # Moving bones of layer
            for lay, bones_list in layer_bones.items():
                for b in bones_list:
                    bone = self.bones.get(b) # If the bone exists in the armature
                    if bone:
                        if not self.armature.data.bones[b].layers[lay]:
                            self.layers_to_true.append((b, lay))

            # Changing layers visibility
            visibility = [b for b in self.bones if ("_ref" in b.name) and ("c_eye" not in b.name)]
            for bone in visibility:
                if not self.armature.data.bones[bone.name].layers[15]:
                    self.layers_visibility.append(bone)

            if len(self.layers_to_true) == 0 and len(self.layers_visibility) == 0:
                return True, "Bones layers and visibility are correct"
            else:
                return False, "Some bones are in wrong layers or have a bad visibility"




    class TestPoseContent(QATest):

        """ Is the structure of the armature correct ? """

        name_rig = ""
        rig_collection = None
        fix_rig_content = ""
        armature = None

        def necessary(self):
            return True

        def build_context(self, context):
            self.name_rig  = context['name_rig_collection']
            self.rig_collection = bpy.data.collections[self.name_rig]
            self.armature = self.rig_collection.objects.get(self.name_rig)

            if self.armature is None:
                return False, "Failure while building context. An armature named [{}] should be the single object in the [{}] collection".format(self.name_rig, self.name_rig)

            return True, "Context built"


        def get_sub_test_types(self):
            return test_vertices() + [TestBonesLayers]


        def test(self, context):

            if (self.armature.pose == None):
                return False, "The armature [{}] doesn't have a pose".format(self.name_rig)
            else:
                if (len(self.armature.pose.bones) < 3):
                    return False, "The armature [{}] doesn't have all required bones in its pose".format(self.name_rig)
                else:
                    if "c_pos" in self.armature.pose.bones:
                        list_bones = []
                        hierarchy = []
                        bfs(self.armature.pose.bones["c_pos"], list_bones, hierarchy)

                        if ((hierarchy[0][0].name != "c_pos") or (hierarchy[1][0].name != "c_traj")):
                            return False, "Wrong bones structure in the pose of the armature [{}]".format(self.name_rig)
                        else:
                            return True, "Correct [{}] armature".format(self.name_rig)
                    else:
                        return False, "Wrong bones structure in the pose of the armature [{}] * Expected [{}] > [{}] > [{}]".format(self.name_rig, self.name_rig+"_data", "c_pos", "c_traj")


    class TestSubRigCollection(QATest):

        """ Is there a sub-collection is in [asset_name_rig] ? Is its name [asset_name_subrig] ? """

        name_rig = ""
        name_subrig = ""
        rig_collection = None
        subrig_collection = None
        nb_children = 0
        fix_rig_sub = ""

        def relevant_for(self, context):
            if len(bpy.data.collections.get(context['name_rig_collection']).children) > 0:
                return True, "relevant"
            else:
                return False, "No [{}] collection contained inside [{}]".format(context['name_asset']+"_subrig", context['name_rig_collection'])


        def build_context(self, context):
            self.name_rig = context['name_rig_collection']
            self.name_subrig = context['name_asset'] + "_subrig"
            self.rig_collection = bpy.data.collections.get(self.name_rig)

            self.nb_children = len(self.rig_collection.children)

            if self.nb_children == 1:
                self.subrig_collection = self.rig_collection.children[0]

            else:
                self.subrig_collection = None

            return True, "Context built"
                    

        def can_fix(self, context):
            if self.fix_rig_sub != "":
                return True, "Can be renamed"
            else:
                return False, "Sub rig collection not fixable"

        def fix(self, context):
            if self.fix_rig_sub == 'NAME':
                old_name = self.subrig_collection.name
                self.subrig_collection.name = self.name_subrig
                return True, "The [{}] collection was renamed in [{}]".format(old_name, self.name_subrig)
            else:
                return False, "Unexpected args in fixing"

        def test(self, context):

            if self.nb_children == 1:
                if self.subrig_collection.name == self.name_subrig:
                    return True, "Collection [{}] correctly placed and named".format(self.name_subrig)
                else:
                    self.fix_rig_sub = 'NAME'
                    return False, "Collection [{}] from [{}] should be named [{}]".format(self.subrig_collection.name, self.name_rig, self.name_subrig)
            else:
                return False, "Too many collections detected in the collection [{}]. Only [{}] should be present".format(self.name_rig, self.name_subrig)


    class TestRigContent(QATest):

        """ Is the content of [asset_name_rig] collection correct ? """

        fix_rig = ""
        name_rig = ""
        name_subrig = ""
        rig_collection = None
        subrig_collection = None
        tree = None

        def necessary(self):
            return True

        def prerequisites(self, context):
            if len(bpy.data.collections.get(context['name_rig_collection']).objects) == 0:
                return False, "Can not perform test on empty collection"
            else:
                return True, "Prerequisites met"

        def build_context(self, context):
            self.name_rig = context['name_rig_collection']
            self.name_subrig = context['name_asset'] + '_subrig'
            self.rig_collection = bpy.data.collections.get(self.name_rig)
            self.subrig_collection = self.rig_collection.children.get(self.name_subrig)
            
            if self.rig_collection is None:
                return False, "Failed in building context"

            try:
                self.tree = tree_from_collection(self.rig_collection)
            except:
                return False, "Impossible to build the tree of the [{}] collection".format(self.name_rig)

            return True, "Context built"


        def get_sub_test_types(self):
            return test_actions() + [TestPoseContent] + test_visibilities() + test_rig_properties() + test_picker() + [TestDupliBones, TestProtectedLayers, TestBrokenConstraints]


        def can_fix(self, context):
            
            if (self.fix_rig != ""):
                return True, "Simple naming correction"
            else:
                objects = [o.name.lower() for o in bpy.data.objects]

                if "rig" in objects:
                    self.fix_rig = 'LOCATION'
                    return True, "Bad location"
                else:
                    return False, "Rig structure not respected"


        def fix(self, context):

            if (self.fix_rig == 'NAME'):
                armature = bpy.data.objects.get(self.tree['tree_roots'][0])
                old_name = armature.name
                rename_obj(armature, self.name_rig)
                return True, "Name of armature fixed, from [{}] to [{}]".format(old_name, self.name_rig)

            elif (self.fix_rig == 'LOCATION'):
                objects = [o.name.lower() for o in bpy.data.objects]
                i = objects.index("rig")
                old_name = bpy.data.objects[i].name
                bpy.data.objects[i].name = self.name_rig
                collec = None
                parent = None
                objects = []

                for c in bpy.data.collections:
                    if self.name_rig in c.objects:
                        parent = c
                        break

                if self.name_rig in bpy.context.scene.collection.objects:
                    parent = bpy.context.scene.collection

                bfs(bpy.data.objects[self.name_rig], objects)

                for obj in objects:
                    if parent:
                        parent.objects.unlink(obj)
                    self.rig_collection.objects.link(obj)

                return True, "Object [{}] renamed [{}] and moved as child of the [{}] collection".format(old_name, self.name_rig, self.name_rig)

            elif self.fix_rig == 'TOO MANY':
                names = []
                if self.subrig_collection is None:
                    c = bpy.data.collections.new(self.name_subrig)
                    self.rig_collection.children.link(c)
                    self.subrig_collection = c

                for obj in self.tree['tree_roots']:
                    if obj != self.name_rig:
                        self.rig_collection.objects.unlink(bpy.data.objects[obj])
                        self.subrig_collection.objects.link(bpy.data.objects[obj])
                        names.append("["+obj+"]")

                return True, "Moved object(s) {} inside the [{}] collection".format(", ".join(names), self.name_subrig)


            else:
                return False, "Rigging issue not fixed"

        def test(self, context):

            if(len(self.tree['tree_roots']) == 1):
                # There is exactly one object in the rig collection
                if(self.tree['tree_roots'][0] == self.name_rig):
                    if (bpy.data.objects.get(self.tree['tree_roots'][0]).type == 'ARMATURE'):
                        # This one item is correctly named
                        return True, "Rig [{}] correctly placed and named".format(self.name_rig)
                    else:
                        return False, "[{}] is not an armature".format(self.tree['tree_roots'][0])
                else:
                    # Only one item but with bad name
                    if (bpy.data.objects.get(self.tree['tree_roots'][0]).type == 'ARMATURE'):
                        self.fix_rig = 'NAME'
                        return False, "The collection [{}] should contain only one object named [{}]".format(self.name_rig, self.name_rig)
                    else:
                        return False, "[{}] is not an armature".format(self.tree['tree_roots'][0])
            else:
                if self.name_rig in self.tree['tree_roots']:
                    self.fix_rig = 'TOO MANY'
                    return False, "Too many objects inside [{}] collection. Should only contain an armature named [{}]".format(self.name_rig, self.name_rig)
                else:
                    found = ["["+item+"]" for item in self.tree['tree_roots']]
                    return False, "The collection [{}] should only contain a rig named [{}] * Found {}".format(self.name_rig, self.name_rig, ", ".join(found))
            




    class TestRigStructure(QATest):

        """ Verification of the structure inside [asset_name_rig] """

        rig_collection = None

        def necessary(self):
            return True

        def prerequisites(self, context):
            self.rig_collection = bpy.data.collections.get(context['name_rig_collection'])

            if self.rig_collection is None:
                return False, "[{}] collection missing".format(context['name_rig_collection'])
            else:
                if len(self.rig_collection.objects) == 0:
                    return False, "Can't perform tests on an empty collection ([{}])".format(context['name_rig_collection'])
                else:
                    return True, "Prerequisites met"

        def get_sub_test_types(self):
            return [TestSubRigCollection, TestRigContent]


    class TestVisibilityRig(TestVisibility):

        """ Does the collection [asset_name_rig] respects visibility and selectability conventions ? """

        requirements = {'hide_viewport': False, 'hide_render': False, 'hide_select': False}
        collection_type = "rig"


    class TestRigCollection(QATest):

        """ Is a collection [asset_name_rig] present in the scene ? """

        main_collection = None
        desired_name = ""
        fix_rig_collection = ""
        rig_collection = ""
        flag = ''

        def necessary(self):
            return True


        def prerequisites(self, context):
            if context.get('name_rig_collection') is None:
                return False, "Name of [asset_name_rig] collection not defined"
            else:
                return True, "Prerequisites met"


        def build_context(self, context):
            self.desired_name = context['name_rig_collection']
            self.main_collection = bpy.data.collections.get(context['name_asset'])

            if self.main_collection is None:
                return False, "Failed in building context [{}] collection missing".format(context['name_asset'])

            return True, "Context built"


        def get_sub_test_types(self):
            return [TestRigStructure, TestVisibilityRig]


        def can_fix(self, context):
            names = ['_rig', 'arma']

            status, self.rig_collection = locate_collection(self.desired_name, names)
            
            if status:
                return True, "Can fix"
            else:
                return False, "Can't fix"


        def fix(self, context):
            old_name = correct_collection(self.desired_name, self.rig_collection, self.flag, self.main_collection)
            return True, "[{}] collection is now called [{}] and is child of the [{}] collection".format(old_name, self.desired_name, self.main_collection.name)


        def test(self, context):
            if self.main_collection.children.get(self.desired_name) is not None:
                return True, "[{}] collection correctly placed and named".format(self.desired_name)
            else:
                return False, "[{}] collection is missing in the [{}] collection".format(self.desired_name, self.main_collection.name)

    return [TestRigCollection]
