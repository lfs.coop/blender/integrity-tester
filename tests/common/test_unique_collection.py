from ..sqarf.qatest import QATest
import bpy

def test_unique_collection():

    class TestCleanMainCollection(QATest):

        """ Does the main collection contain only what it should ? """

        main_collection = None
        name_collection = ""

        def build_context(self, context):
            self.name_collection = context['name_asset']
            self.main_collection = bpy.data.collections.get(self.name_collection)

            if self.main_collection is None:
                return False, "Failed in building context. [{}] collection not found".format(self.name_collection)

            return True, "Context built"


        def test(self, context):
            objs = []
            colls = []

            for obj in self.main_collection.objects:
                objs.append("["+obj.name+"]")

            for coll in self.main_collection.children:
                if coll.name not in context['allowed_collections']:
                    colls.append("["+coll.name+"]")

            if (len(objs) == 0) and (len(colls) == 0):
                return True, "[{}] collection looks clean".format(self.name_collection)
            else:
                self.raise_flag('warning')
                return True, "Collections {} should not be in the [{}] collection. Objects {} should not be in there either.".format(", ".join(colls), self.name_collection, ", ".join(objs))

    class TestUniqueCollection(QATest):

        """ Is the [asset_name] collection alone at the top-level of the scene ? """

        names_cols = []
        names_objs = []

        def get_sub_test_types(self):
            return [TestCleanMainCollection]

        def can_fix(self, context):
            if context['name_asset'] in bpy.context.scene.collection.children:
                return True, "Extra stuff will be REMOVED from the scene"
            else:
                return False, "The master collection should just contain the [{}] collection".format(context['name_asset'])

        def fix(self, context):
            main_coll = bpy.data.collections[context['name_asset']]
            extra_colls = []
            extra_objs = []

            for c in bpy.context.scene.collection.children:
                if c is not main_coll:
                    if (context['phase'] == 'BASIC') and (c.name == context['name_draft_collection']):
                        continue
                    else:
                        extra_colls.append("["+c.name+"]")
                        bpy.data.collections.remove(c)

            for o in bpy.context.scene.collection.objects:
                extra_objs.append("["+o.name+"]")
                bpy.data.objects.remove(o)

            names_colls = ", ".join(extra_colls)
            names_objs = ", ".join(extra_objs)
            self.raise_flag('warning')

            return True, "The collection [{}] should be alone in the master collection. * Collection(s) found and deleted: [{}] * Object(s) found and deleted: [{}]".format(context["name_asset"], names_colls, names_objs)

        def test(self, context):

            extra_cols = [c.name for c in bpy.context.scene.collection.children]
            extra_objs = [c.name for c in bpy.context.scene.collection.objects]

            if context['phase'] == 'BASIC':
                if context['name_draft_collection'] in extra_cols:
                    extra_cols.remove(context['name_draft_collection'])

            if ((len(extra_cols) > 1) or (len(extra_objs) > 0)):
                self.names_cols = ["["+c+"]" for c in extra_cols]
                self.names_objs = ["["+c+"]" for c in extra_objs]
                return False, "* The collection [{}] should be alone in the master sollection of the scene * Found collection(s) {} * Found object(s): ".format(context['name_asset'], ", ".join(self.names_cols), ", ".join(self.names_objs))
            else:
                return True, "The collection [{}] is the only one in the master collection of the scene".format(context["name_asset"])

    return [TestUniqueCollection]
