from ..sqarf.qatest import QATest
import bpy

from .test_cs_collection import test_cs_collection
from .test_high_collection import test_high_collection
from .test_rig_collection import test_rig_collection
from .test_unique_collection import test_unique_collection
from .test_deform_collection import test_deform_collection
from .test_low_collection import test_low_collection

from ..utils.general import bfs


def test_main_collection():

    class TestMainCollection(QATest):

        """ Is the main collection ([asset_name]) present in the scene ? """

        fix_main_collec = ""
        closest_main_collection = ""

        def necessary(self):
            return True


        def prerequisites(self, context):
            
            if context["name_asset"] is None:
                return False, "Asset name not defined"
            else:
                return True, "Can proceed to test"


        def build_context(self, context):
            context['main_collection'] = None
            return True, "Context built"


        def get_sub_test_types(self):
            # test_low_collection() should be added
            # test_deform_collection() should be added
            return test_cs_collection() + test_rig_collection() + test_high_collection() + test_deform_collection() + test_unique_collection() + test_low_collection()


        def can_fix(self, context):

            if len(bpy.context.scene.collection.children) == 1:
                self.fix_main_collec = 'COLLEC_ALONE'
                return True, "The collection [{}] can be renamed".format(bpy.context.scene.collection.children[0].name)
            else:
                names_ori = [c.name for c in bpy.context.scene.collection.children]
                names = [c.lower().replace(' ', '_').replace('-', '_').replace(',', '') for c in names_ori]

                if context["name_asset"] in names:
                    self.fix_main_collec = 'CLOSE_NAME'
                    self.closest_main_collection = names_ori[names.index(context["name_asset"])]
                    return True, "Will modify the string case"
                else:
                    return False, "Collection [{}] missing".format(context["name_asset"])


        def fix(self, context):
            if self.fix_main_collec == 'CLOSE_NAME':
                old_name = self.closest_main_collection
                bpy.context.scene.collection.children[self.closest_main_collection].name = context["name_asset"]
                return True, "Renamed collection [{}] into [{}]".format(old_name, context["name_asset"])

            elif self.fix_main_collec == 'COLLEC_ALONE':
                old_name = bpy.context.scene.collection.children[0].name
                bpy.context.scene.collection.children[0].name = context["name_asset"]
                return True, "Renamed collection [{}] into [{}]".format(old_name, context["name_asset"])

            else:
                return False, "Unexpected case"


        def test(self, context):
            if context["name_asset"] in bpy.context.scene.collection.children:
                return True, "Collection {} found in the master collection".format("[{}]".format(context["name_asset"]))
            else:
                return False, "A collection named {} should be present in the master collection".format("[{}]".format(context["name_asset"]))

    return [TestMainCollection]
