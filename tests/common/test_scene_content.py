from ..sqarf.qatest import QATest
import bpy
import re
import os

def clean_path(path):
    path = path.replace('\\', '/')
    s_path = path.split('/')
    c_path = []

    for item in s_path:
        if item == "..":
            c_path.pop()
        else:
            c_path.append(item)

    return "/".join(c_path)

def compare_roots(abs_path_1, abs_path_2):
    splitted_1 = abs_path_1.split('/')
    splitted_2 = abs_path_2.split('/')
    
    if not (('lib' in splitted_1) and ('lib' in splitted_2)):
        return False
    
    index_1 = splitted_1.index('lib')
    index_2 = splitted_2.index('lib')
    
    if (index_1 != index_2):
        return False
    
    # +1 because we want the index to be included
    for i in range(0, index_1+1):
        if splitted_1[i] != splitted_2[i]:
            return False
    
    return True


def asset_name():
    return bpy.context.scene.collection.children[0].name


def name_folder():
    filename = bpy.data.filepath.replace('\\', '/').split('/')[-1]
    filename = filename.split('.')[0]
    name = []
    for atom in filename.split('_'):
        if 'rig' in atom:
            break
        name.append(atom)
    name = '_'.join(name)
    textures = f"{name}_sha_textures"
    proxy = f"{name}_sha_proxy"
    return (textures, proxy)


def last_version(path):
    versions = [v for v in os.listdir(path) if re.fullmatch("(v|V)[0-9]{3}", v)]
    versions.sort()
    
    if len(versions) == 0:
        return None
    else:
        return versions[-1]


def path_textures():
    path = f"//../../../shading/"
    p_textures, p_proxy = name_folder()
    v_proxy = None
    v_textures = None
    p_textures = f"{path}{p_textures}/"
    p_proxy = f"{path}{p_proxy}/"
    
    if not os.path.isdir(bpy.path.abspath(p_proxy)):
        p_proxy = None
    else:
        v = last_version(bpy.path.abspath(p_proxy))
        if v:
            v_proxy = f"{p_proxy}{v}/"
        else:
            v_proxy = None
        
    if not os.path.isdir(bpy.path.abspath(p_textures)):
        p_textures = None
    else:
        v = last_version(bpy.path.abspath(p_textures))
        if v:
            v_textures = f"{p_textures}{v}/"
        else:
            v_textures = None
    
    return (p_proxy, p_textures, v_proxy, v_textures)


def test_scene_content():

    class TestNoCamera(QATest):

        """ Is there any camera remaining in the scene ? """

        safe = []
        cameras = []

        def can_fix(self, context):
            return True, "Will remove all cameras from the scene"

        def fix(self, context):
            for c in self.cameras:
                bpy.data.objects.remove(c)

            return True, "All unecessary cameras have been removed from the scene"

        def test(self, context):
            
            if context['phase'] == 'BASIC':
                draft = bpy.context.scene.collection.children.get(context['name_draft_collection'])
                if draft is not None:
                    self.safe = [o.name for o in draft.objects if (o.type == 'CAMERA')]

            self.safe.append('cam_ui')
            self.cameras = [cam for cam in bpy.data.objects if (cam.type == 'CAMERA') and (cam.name not in self.safe)]

            if(len(self.cameras) > 0):
                return False, "No camera should be present in a prop file"
            else:
                return True, "No camera found in the scene"



    class TestNoLight(QATest):

        """ Is there any light remaining in the scene ? """

        safe = []
        lights = []

        def can_fix(self, context):
            return True, "Will remove all lights from the scene"

        def fix(self, context):
            for l in self.lights:
                bpy.data.objects.remove(l)

            return True, "All unecessary lights have been removed from the scene"

        def test(self, context):

            if context['phase'] == 'BASIC':
                draft = bpy.context.scene.collection.children.get(context['name_draft_collection'])
                if draft is not None:
                    self.safe = [o.name for o in draft.objects if (o.type == 'LIGHT')]

            self.lights = [light for light in bpy.data.objects if (light.type == 'LIGHT') and (light.name not in self.safe)]

            if(len(self.lights) > 0):
                return False, "No light should be present in a prop file"
            else:
                return True, "No light found in the scene"


    class TestSubdivModifier(QATest):

        """ Do [SubSurf] modifiers respect conventions """

        extra_subdiv    = []
        loca_subdiv     = []
        missing_subdiv  = []
        names_extras    = []
        names_locations = []
        names_missing   = []
        present         = []
        present_name    = []
        high_collection = None
        deform_collec   = []
        error_deform    = set()

        def prerequisites(self, context):
            if bpy.data.collections.get(context['name_high_collection']) is None:
                return False, "Missing collection [{}]. Impossible to perform the test".format(context['name_high_collection'])
            else:
                return True, "Prerequisites met"

        def build_context(self, context):
            self.high_collection = bpy.data.collections.get(context['name_high_collection'])
            deform = bpy.data.collections.get(context['name_deform_collection'])
            if deform:
                self.deform_collec = list(deform.all_objects)
            return True, "Context built"


        def can_fix(self, context):
            if (len(self.error_deform) > 0) or (context['category'] == 'props'):
                return False, "Can't fix"
            else:
                return True, "Will change modifiers stack"


        def fix(self, context):
            to_modify = set(self.extra_subdiv).union(set(self.loca_subdiv)).union(set(self.missing_subdiv))

            for name in to_modify:
                obj = bpy.data.objects[name]
                to_remove = [modif for modif in obj.modifiers if modif.type == 'SUBSURF']

                # Final subsurf will get the highest subdiv level met in former modifiers
                try:
                    max_sub_r = max([sub.render_levels for sub in to_remove])
                    max_sub_v = max([sub.levels for sub in to_remove])
                except:
                    max_sub_r = 1
                    max_sub_v = 1

                for modif in to_remove: # Removing all subsurf
                    obj.modifiers.remove(modif)

                m = obj.modifiers.new(name="Subdivision Surface", type='SUBSURF')
                m.render_levels = max_sub_r
                m.levels = max_sub_v

            return True, "New subdivision modifiers added to: {} *Excess subdivision modifiers removed from: {} *Some subdivision modifiers were misplaced on: {}".format(", ".join(self.names_missing), ", ".join(self.names_extras), ", ".join(self.names_locations))


        def test(self, context):

            for obj in self.high_collection.objects:
                if obj.type == 'MESH':
                    count = 0

                    for index, modif in enumerate(obj.modifiers):
                        if modif.type == 'SUBSURF':
                            count += 1
                            if index != len(obj.modifiers)-1:
                                self.loca_subdiv.append(obj.name)
                    if count > 1:
                        self.extra_subdiv.append(obj.name)

                    if count == 0:
                        self.missing_subdiv.append(obj.name)

                    if count > 0:
                        self.present.append(obj.name)

            for obj in self.deform_collec:
                if obj.type == 'MESH':
                    for m in obj.modifiers:
                        if m.type == 'SUBSURF':
                            self.error_deform.add("[{}]".format(obj.name))

            if len(self.error_deform) > 0:
                return False, "[SUBSURF] modifiers are not allowed on objects from [{}] collection. Some have been found on: {}".format(context['name_deform_collection'], ", ".join(self.error_deform))


            if context['category'] == 'chars':
                if (len(self.loca_subdiv) > 0) or (len(self.extra_subdiv) > 0) or (len(self.missing_subdiv) > 0):
                    msg = ""
                    if (len(self.extra_subdiv) > 0):
                        self.names_extras = ["["+o+"]" for o in self.extra_subdiv]
                        msg += "{} objects have too many subdivision surface modifiers".format(", ".join(self.names_extras))

                    if (len(self.loca_subdiv) > 0):
                        self.names_locations = ["["+o+"]" for o in self.loca_subdiv]
                        msg += "*{} objects have a subdivision surface at the wrong place".format(", ".join(self.names_locations))

                    if (len(self.missing_subdiv) > 0):
                        self.names_missing = ["["+o+"]" for o in self.missing_subdiv]
                        msg += "*{} objects should have a subdivision surface modifier".format(", ".join(self.names_missing))

                    return False, msg

                else:
                    return True, "Subdivion surfaces well placed"

            elif context['category'] == 'props':
                if len(self.present) > 0:
                    self.present_name = ["["+o+"]" for o in self.present]
                    return False, "Some subdiv modifiers were found in objects: {}".format(", ".join(self.present_name))
                else:
                    return True, "No subdivision surface found"


    class TestMeshDeform(QATest):

        """ Are all [MeshDeform] modifiers bound ?"""

        objects = []

        def build_context(self, context):
            for obj in bpy.data.objects:
                for m in obj.modifiers:
                    if m.type == 'MESH_DEFORM':
                        self.objects.append((obj, m))

            return True, "Context built"

        def test(self, context):
            errors = {}
            
            for obj, mod in self.objects:
                if not mod.is_bound:
                    errors.setdefault(obj.name, [])
                    errors[obj.name].append(mod.name)

            if len(errors) == 0:
                return True, "All [Mesh Deform] modifiers are bound"
            else:
                formated_errors = []
                for obj, mods in errors.items():
                    m = ["[{}]".format(k) for k in mods]
                    formated_errors.append("{} from [{}]".format(", ".join(m), obj))
                return False, "Some [Mesh Deform] modifiers are not bound: {}".format(", ".join(formated_errors))




    class TestMatchModifiers(QATest):

        """ Do all modifiers activations in viewport match activations in render ? """

        def necessary(self):
            return False

        def get_sub_test_types(self):
            return [TestSubdivModifier, TestMeshDeform]

        def test(self, context):
            errors = []
            warnings = []

            for obj in bpy.data.objects:
                for modif in obj.modifiers:
                    if modif.show_render != modif.show_viewport:
                        errors.append("[{}({})]".format(obj.name, modif.type))

                    if not modif.show_render and not modif.show_viewport:
                        warnings.append("[{}({})]".format(obj.name, modif.type))

            if len(errors) == 0:
                msg = "All modifiers have the activation value in render and viewport"
                if len(warnings) > 0:
                    self.raise_flag('warning')
                    msg += "*Some modifiers are unactivated both in viewport and render [{}]".format(", ".join(warnings))
                return True, msg
            else:
                msg = "Some modifiers have different activation values in render and viewport: {}".format(", ".join(errors))
                if len(warnings) > 0:
                    msg += "* Some modifiers are desactivated in both viewport and render: {}".format(", ".join(warnings))
                return False, msg


    class TestMatchVisibility(QATest):

        """ Individual objects and collections should have the same visibility in viewport and render """

        ignore_obj  = ["_char_name", "_mesh", "label_t", "label_", "rig_ui", "cam_ui"]
        ignore_cols = []

        def test(self, context):
            errors = []

            for item in bpy.data.objects:
                if (item.name not in self.ignore_obj) and ('picker' not in item.name):
                    if(item.hide_viewport != item.hide_render):
                        errors.append("["+item.name+"]")

            for item in bpy.data.collections:
                if item.name not in self.ignore_cols:
                    if(item.hide_viewport != item.hide_render):
                        errors.append("["+item.name+"]")

            if(len(errors) == 0):
                return True, "View port visibility matches the render one for every object and collections"
            else:
                return False, "Individual objects and collections should have the same visibility in viewport and render: {}".format(", ".join(errors))


    class TestTexturesDuplicates(QATest):

        """ Checks that no texture is being duplicated """

        present_instances = {}
        removed = []
        error_names = []

        def build_context(self, context):
            return True, "Not Implemented Yet"

        def can_fix(self, context):
            return True, "Will change"

        def fix(self, context):
            errors = []

            for img, target in self.removed:
                img.user_remap(target)
                errors.append("[{}]".format(img.name))
                bpy.data.images.remove(img)

            return True, "Removed some textures: {}".format(", ".join(errors))


        def test(self, context):
            # Duplication of the collection because original will be modified over iterations
            for img in bpy.data.images[:]:
                path = bpy.path.abspath(img.filepath)
                path = path.replace('\\', '/')

                # Some packaged files had their path deleted
                if len(path) == 0:
                    continue

                path = path.split('/')[-1]
                
                # Check if a similar path has already been met
                if path in self.present_instances:
                    # img.user_remap(self.present_instances[path])
                    self.removed.append((img, self.present_instances[path]))
                    # bpy.data.images.remove(img)
                    
                else:
                    self.present_instances[path] = img

            if len(self.removed) == 0:
                return True, "No texture is being duplicated"
            else:
                errors = ["[{}]".format(img.name) for img, target in self.removed]
                return False, "Some textures are being duplicated: {}".format(", ".join(errors))



    class TestLocationTextures(QATest):

        """ Are all textures: In the correct directory? Saved relative? Not in [current]? """

        unknown  = [] # Unknown path (absolute and from another system)
        relative = [] # Problem of relative path
        low      = [] # Low-res texture misplaced
        v_low    = [] # Problem of version on a low-res texture
        v_reg    = [] # Problem of version on a regular texture
        regular  = [] # Regular texture misplaced
        warnings = [] # Packed textures which are not either the shadow of the jaw or the nose

        regex    = "v[0-9]{3}"
        p_proxy = None
        p_textures = None
        v_proxy = None
        v_textures = None
        used_textures = set()

        def build_context(self, context):
            # Images present in the file used as textures:
            for m in bpy.data.materials:
                if (m.use_nodes) and (m.node_tree is not None):
                    for n in m.node_tree.nodes:
                        if (n.type == 'TEX_IMAGE') and (n.image is not None):
                            self.used_textures.add(n.image)

            # Building theorical path of textures:
            self.p_proxy, self.p_textures, self.v_proxy, self.v_textures = path_textures()
            
            return True, "Context built"


        def necessary(self):
            return True


        def get_sub_test_types(self):
            return [TestTexturesDuplicates]


        def can_fix(self, context):
            if len(self.low) > 0 or len(self.v_low) > 0:
                if self.p_textures and self.p_proxy and self.v_textures and self.v_proxy:
                    return True, "Will try to remap textures"
                else:
                    return False, "Directories not found"
            else:
                if self.p_textures and self.v_textures:
                    return True, "Will try to remap textures"
                else:
                    return False, "Directories not found"



        def fix(self, context):
            msg = ""
            f_rel = []
            ok_rel = []
            f_unk = []
            ok_unk = []
            f_low = []
            ok_low = []
            f_reg = []
            ok_reg = []

            # Fixing relative paths:
            if len(self.relative) > 0:
                for img in self.relative:
                    p = img.filepath.replace('\\', '/')
                    if os.path.isfile(p):
                        img.filepath = bpy.path.relpath(os.path.abspath(bpy.path.abspath(p)))
                        ok_rel.append(f"[{img.name}]")
                    else:
                        name = p.split('/')[-1]
                        if not name.lower().endswith('.png'):
                            f_rel.append(f"[{img.name}]")
                        else:
                            if name in os.listdir(bpy.path.abspath(self.v_textures)):
                                img.filepath = f"{self.v_textures}/{name}"
                                ok_rel.append(f"[{img.name}]")
                            elif name in os.listdir(bpy.path.abspath(self.v_proxy)):
                                img.filepath = f"{self.v_proxy}/{name}"
                                ok_rel.append(f"[{img.name}]")
                            else:
                                f_rel.append(f"[{img.name}]")
                if len(ok_rel) > 0:
                    msg += f"* Some relative paths have been successfully fixed: {', '.join(ok_rel)}"
                if len(f_rel) > 0:
                    msg += f"* Some relative paths failed to be fixed: {', '.join(f_rel)}"

            # Unknown paths discovered:
            if len(self.unknown) > 0:
                for img in self.unknown:
                    p = img.filepath.replace('\\', '/')
                    name = p.split('/')[-1]
                    if not name.lower().endswith('.png'):
                        f_unk.append(f"[{img.name}]")
                    else:
                        if name in os.listdir(bpy.path.abspath(self.v_textures)):
                            img.filepath = f"{self.v_textures}/{name}"
                            ok_rel.append(f"[{img.name}]")
                        elif name in os.listdir(bpy.path.abspath(self.v_proxy)):
                            img.filepath = f"{self.v_proxy}/{name}"
                            ok_rel.append(f"[{img.name}]")


            # Images with _low in the name
            if len(self.low) > 0 or len(self.v_low) > 0:
                for img in self.low + self.v_low:
                    p = img.filepath.replace('\\', '/')
                    name = p.split('/')[-1]
                    if name in os.listdir(bpy.path.abspath(self.v_proxy)):
                        img.filepath = f"{self.v_proxy}/{name}"
                        ok_low.append(f"[{img.name}]")
                    else:
                        f_low.append(f"[{img.name}]")


            # Other textures
            if len(self.regular) > 0 or len(self.v_reg) > 0:
                for img in self.regular + self.v_reg:
                    p = img.filepath.replace('\\', '/')
                    name = p.split('/')[-1]
                    if name in os.listdir(bpy.path.abspath(self.v_textures)):
                        img.filepath = f"{self.v_textures}/{name}"
                        ok_reg.append(f"[{img.name}]")
                    else:
                        f_reg.append(f"[{img.name}]")



            if len(ok_rel) > 0:
                msg += f"* Some textures now have a relative path: {', '.join(ok_rel)}"

            if len(f_rel) > 0:
                msg += f"* Failed to find a relative path for textues: {', '.join(f_rel)}"
            
            if len(ok_unk) > 0:
                msg += f"* Some textures with invalid paths have been relinked: {', '.join(ok_unk)}"

            if len(f_unk) > 0:
                msg += f"* Failed to locate some textures: {', '.join(f_unk)}"

            if len(ok_low) > 0:
                msg += f"* Some low-res textures paths have been remapped to last version: {', '.join(ok_low)}"

            if len(f_low) > 0:
                msg += f"* Some textures have been detected as low-res but couldn't be remapped: {', '.join(f_low)}"

            if len(ok_reg) > 0:
                msg += f"* Some textures paths have been remapped to last version: {', '.join(ok_reg)}"

            if len(f_reg) > 0:
                msg += f"* Some textures couldn't be remapped: {', '.join(f_reg)}"

            if (len(f_rel) > 0) or (len(f_unk) > 0) or (len(f_low) > 0) or (len(f_reg) > 0):
                return False, msg
            else:
                return True, msg


        def test(self, context):
            # It's useless to make the test if no texture is used
            if len(self.used_textures) == 0:
                return True, "No texture is being used on this asset"

            # Some textures are in use but no texture directory was found
            if self.p_textures is None:
                return False, "The textures directory was not found for this asset"

            # For each image being used as texture
            for img in self.used_textures:
                if (img.packed_file is not None):
                    # We tolerate the shadows to be packed
                    if (img.name not in ['shadow_neck', 'shadow_jaw', 'shadow_neck.png', 'shadow_jaw.png']):
                        self.warnings.append(img)
                    continue

                path = img.filepath
                path = path.replace('\\', '/')

                if not path.startswith('//'):
                    try:
                        rel_path = bpy.path.relpath(path)
                    except:
                        self.unknown.append(img)
                    else:
                        self.relative.append(img)
                    continue

                if "_low" in img.name:
                    if (self.p_proxy is None):
                        self.low.append(img)
                        continue
                    if not path.startswith(self.p_proxy):
                        self.low.append(img)
                        continue

                    if (self.v_proxy is None):
                        self.v_low.append(img)
                        continue

                    if not path.startswith(self.v_proxy):
                        self.v_low.append(img)
                        continue

                else:
                    if (self.p_textures is None):
                        self.regular.append(img)
                        continue
                    if not path.startswith(self.p_textures):
                        self.regular.append(img)
                        continue

                    if (self.v_textures is None):
                        self.v_reg.append(img)
                        continue

                    if not path.startswith(self.v_textures):
                        self.v_reg.append(img)
                        continue

                try:
                    abs_p = bpy.path.abspath(path)
                except:
                    self.unknown.append(img)
                else:
                    if not os.path.isfile(abs_p):
                        self.unknown.append(img)

            msg = ""

            if (len(self.unknown) == 0) and (len(self.relative) == 0) and (len(self.low) == 0) and (len(self.regular) == 0) and (len(self.v_low) == 0) and (len(self.v_reg) == 0):
                msg += "All not packed textures look to be at the correct location"
                if len(self.warnings) > 0:
                    self.raise_flag('warning')
                    t_names = [f"[{img.name}]" for img in self.warnings]
                    msg += f"* It looks like some textures are still packed: {', '.join(t_names)}"
                return True, msg
            else:
                if len(self.unknown) > 0:
                    t_un = [f"[{img.name}]" for img in self.unknown]
                    msg += f"* Some textures have a path totally unkown: {', '.join(t_un)}"
                if len(self.relative) > 0:
                    t_rel = [f"[{img.name}]" for img in self.relative]
                    msg += f"* Some textures don't have a relative path: {', '.join(t_rel)}"
                if len(self.low) > 0:
                    t_l = [f"[{img.name}]" for img in self.low]
                    msg += f"* Some textures should be placed in the low res directory: {', '.join(t_l)}"
                if len(self.v_low) > 0:
                    t_vl = [f"[{img.name}]" for img in self.v_low]
                    msg += f"* Some low-res textures are not pointing to their latest version: {', '.join(t_vl)}"
                if len(self.regular) > 0:
                    t_reg = [f"[{img.name}]" for img in self.regular]
                    msg += f"* Some textures should be in the textures directory of this character: {', '.join(t_reg)}"
                if len(self.v_reg) > 0:
                    t_vreg = [f"[{img.name}]" for img in self.v_reg]
                    msg += f"* Some textures  are not pointing to their latest version: {', '.join(t_vreg)}"

                return False, msg
                    

    class TestDriversDuplicates(QATest):

        """ Are some properties carrying several drivers ? """

        drivers_errors = {}

        def can_fix(self, context):
            return True, "Will try to remove useless drivers"

        
        def fix(self, context):
            removed = []
            for id, drivers in self.drivers_errors.items():
                while(len(drivers) > 1):
                    obj = bpy.data.objects.get(id[0])
                    if obj is None:
                        continue
                    d = drivers.pop(0)
                    removed.append("[{}]".format(d.data_path))
                    try:
                        obj.animation_data.drivers.remove(d)
                    except:
                        pass

            return True, "Following drivers have been removed: {}".format(", ".join(removed))

        
        def test(self, context):

            drivers = {}

            for obj in bpy.data.objects:
                if obj.animation_data is not None:
                    for driver in obj.animation_data.drivers:
                        if driver.array_index == -1:
                            ids = [(obj.name, driver.data_path, 0), (obj.name, driver.data_path, 1), (obj.name, driver.data_path, 2)]
                        else:
                            ids = [(obj.name, driver.data_path, driver.array_index)]

                        for id in ids:
                            drivers.setdefault(id, [])
                            drivers[id].append(driver)

            errors = {k: v for k, v in drivers.items() if len(v) > 1}

            if len(errors) == 0:
                return True, "There is no drivers duplicated"
            else:
                enclosed = ["[{}]".format(path) for path in errors.keys()]
                self.drivers_errors = errors
                return False, "Some properties are holding too much drivers: {}".format(", ".join(enclosed))
            


    class TestScaleObjects(QATest):

        """ Do all objects have their scale to 1.0 ? """

        errors = []
        skip = ['char_name', 'label_', 'label_t', 'picker_background', '_char_name', 'eye_l_p', 'eye_r_p']

        def test(self, context):
            for obj in bpy.data.objects:
                if (obj.name.startswith('cs_')) or (obj.name in self.skip) or ('picker' in obj.name) or ('lattice' in obj.name.lower()):
                    continue
                if abs(sum(list(obj.scale)) - 3.0) > 0.01:
                    self.errors.append(obj)
            if len(self.errors) == 0:
                return True, "All objects have their scale correctly set"
            else:
                self.raise_flag('warning')
                return True, "Some objects have a scale different of (1.0, 1.0, 1.0): {}".format(", ".join(["[{}]".format(a.name) for a in self.errors]))


    class TestSceneContent(QATest):
        def get_sub_test_types(self):
            return [TestNoCamera, TestNoLight, TestMatchVisibility, TestLocationTextures, TestMatchModifiers, TestDriversDuplicates, TestScaleObjects]

    return [TestSceneContent]
