from ..sqarf.qatest import QATest
import bpy

from ..utils.general import explore_parents, bfs, tree_from_collection

def test_geometry():

    #class TestBasisDeformer(QATest):
    deformers = [
        'ARMATURE',
        'CAST',
        'CURVE',
        'DISPLACE',
        'HOOK',
        'LAPLACIANDEFORM',
        'LATTICE',
        'MESH_DEFORM',
        'SHRINKWRAP',
        'SIMPLE_DEFORM',
        'SMOOTH',
        'CORRECTIVE_SMOOTH',
        'LAPLACIANSMOOTH',
        'SURFACE_DEFORM',
        'WARP',
        'WAVE'
    ]


    def make_safe(item, content_collec):
        children = []
        bfs(item, children)
        children = set(children)
        content_collec -= children


    def influenced(item, content_collec):
        for m in item.modifiers:
            if m.type in deformers:
                make_safe(item, content_collec)
                return None
            
        for c in item.children:
            influenced(c, content_collec)


    class TestSurfaceDeform(QATest):

        """ Is there a [SURFACE DEFORM] modifier remaining ? """

        meshes = []
        deform_objs = []

        def build_context(self, context):
            deform_collection = bpy.data.collections.get(context['name_deform_collection'])
            if deform_collection:
                self.deform_objs = list(deform_collection.all_objects)

            self.meshes = [o for o in bpy.data.objects if o.type == 'MESH']
            return True, "Context built"

        def test(self, context):
            errors = {}

            for mesh in self.meshes:
                for modif in mesh.modifiers:
                    if modif.type == 'SURFACE_DEFORM':
                        if (modif.target is not None) and (modif.target in self.deform_objs):
                            pass
                        else:
                            errors.setdefault(mesh.name, [])
                            errors[mesh.name].append(modif)

            if len(errors) == 0:
                return True, "No [Surface Deform] modifier was found"
            else:
                formated_errors = ["{} from [{}]".format(", ".join(["[<span title=\"{}\">{}</span>]".format(o.target.name if o.target else "None", o.name) for o in modifs]), mesh) for mesh, modifs in errors.items()]
                return False, "[Surface Deform] modifiers are not allowed. Some have been found: {}".format(", ".join(formated_errors))


    class TestBodyVolume(QATest):

        """ Does the mesh [body] have an armature modifier and preserve volume ? """

        objects = []
        errors_modif = []
        errors_volume = []

        def build_context(self, context):
            self.objects = [o for o in bpy.data.objects if o.name.startswith('body')]
            return True, "Context built"


        def can_fix(self, context):
            if len(self.errors_modif) > 0:
                return False, "Can't fix"
            else:
                return True, "Will activate properties"


        def fix(self, context):
            for obj, modif in self.errors_volume:
                modif.use_deform_preserve_volume = True

            return True, "*Some [Armature] modifier got their [Preserve Volume] property activated: {}".format(", ".join(["[{}] from [{}]".format(m.name, o.name) for o, m in self.errors_volume]))


        def test(self, context):
            
            for obj in self.objects:
                arma = [m for m in obj.modifiers if (m.type == 'ARMATURE')]
                if len(arma) != 1:
                    self.errors_modif.append(obj)
                    continue

                if not arma[0].use_deform_preserve_volume:
                    self.errors_volume.append((obj, arma[0]))

            if (len(self.errors_modif) == 0) and (len(self.errors_volume) == 0):
                return True, "All [body] objects have an Armature modifier and have the [Preserve Volume] property"
            else:
                msg = ""
                if len(self.errors_modif) > 0:
                    msg += "Some [body] objects are missing an [Armature] modifier: {}".format(", ".join(["[{}]".format(o.name) for o in self.errors_modif]))
                if len(self.errors_volume) > 0:
                    msg += "*Some modifiers are missing the [Preserve Volume] property on their [Armature] modifier: {}".format(", ".join(["[{}] from [{}]".format(m.name, o.name) for o, m in self.errors_volume]))

                return False, msg


    class TestGeometryInfluenced(QATest):

        """ Is all the geometry influenced by a rig or a deformer ? """

        high_collection = None
        meshes = []
        tree = None
        errors = []
        top_level = []

        def get_sub_test_types(self):
            return [TestSurfaceDeform, TestBodyVolume]

        def build_context(self, context):
            self.high_collection = bpy.data.collections.get(context['name_high_collection'])

            if self.high_collection is None:
                return False, "Failed to build context. [{}] collection is missing".format(context['name_high_collection'])

            self.meshes = set([obj for obj in self.high_collection.objects if (obj.type == 'MESH')])

            try:
                self.tree = tree_from_collection(self.high_collection)
                self.top_level = set([bpy.data.objects.get(i) for i in self.tree['tree_roots']])

            except:
                return False, "Failed to build context. Impossible to build tree from [{}]".format()

            return True, "Context built"

        
        def test(self, context):
            # The algorithm consists in removing from self.meshes everything we can consider as safe
            # Tests if a mesh has a deformer. If it does, all its children are safe
            for item in self.top_level:
                influenced(item, self.meshes)

            # For the remaining data, we check if it is parented to a bone
            to_remove = set()
            for item in self.meshes:
                if explore_parents(item, 'ARMATURE'):
                    to_remove.add(item)

            self.meshes -= to_remove

            if len(self.meshes) == 0:
                return True, "All geometry is influenced by deformation"
            else:
                names = ["["+i.name+"]" for i in self.meshes]
                return False, "Some geometry is not influenced by any sort of deformation: {}".format(", ".join(names))
            


    return [TestGeometryInfluenced]
