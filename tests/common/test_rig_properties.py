from ..sqarf.qatest import QATest
import bpy

def test_rig_properties():

	class TestPropertiesArmature(QATest):
		
		""" Tests if armatures have their axis hidden and are not in front """

		armatures = []
		errors = []
		names = []

		def build_context(self, context):
			self.armatures = [o for o in bpy.data.objects if (o.type == 'ARMATURE')]
			return True, "Context built"

		def can_fix(self, context):
			return True, "Will toggle axis and in front properties"

		def fix(self, context):
			for o in self.errors:
				o.show_axis = False
				o.show_in_front = False

			return True, "Armatures {} have their axis hidden and are not displayed always in front".format(self.names)

		def test(self, context):
			for a in self.armatures:
				if a.show_axis or a.show_in_front:
					self.errors.append(a)

			if len(self.errors) == 0:
				return True, "Axis hidden and not in front"
			else:
				self.names = ["["+o.name+"]" for o in self.errors]
				return False, "Objects {} are displayed in front or have their axis displayed".format(self.names)

	return [TestPropertiesArmature]