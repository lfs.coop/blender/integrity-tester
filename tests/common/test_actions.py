import bpy
from ..sqarf.qatest import QATest

import re

#RESET POSE

def set_inverse_child(b, cns):          
    # direct inverse matrix method
    if cns.subtarget != "":
        if bpy.context.active_object.data.bones.get(cns.subtarget):           
            cns.inverse_matrix = bpy.context.active_object.pose.bones[cns.subtarget].matrix.inverted()  
    else:
        print("Child Of constraint could not be reset, bone does not exist:", cns.subtarget, cns.name)


def is_reset_bone(bone_name):    
    reset_bones_parent = ["c_foot_ik", "c_hand_ik"]
    for n in reset_bones_parent:
        if n in bone_name:
            return True 

def reset_all_controllers(desired_name):
    """reset all the controllers and siren custom's properties"""
    # the function is run at startup, this allows to exit it
    try:
        bpy.data.objects[desired_name]
    except:
        return
    # Reset Properties
    for bone in bpy.data.objects[desired_name].pose.bones:
        bone_parent = ""
        try:
            bone_parent = bone.parent.name
        except:
            pass

        if (bone.name.startswith('c_') or bone.name.startswith("cc_")) and (bone_parent != "Picker"):
            bone.location = [0,0,0]
            bone.rotation_euler = [0,0,0]
            if bone.rotation_mode == 'QUATERNION':
                bone.rotation_quaternion = [1,0,0,0]
            bone.scale = [1.0,1.0,1.0]
        
        if len(bone.keys()) > 0:
            for key in bone.keys():
                if 'ik_fk_switch' in key:
                    if 'hand' in bone.name:
                        bone['ik_fk_switch'] = 1.0
                    else:
                        bone['ik_fk_switch'] = 0.0

                if 'fly_torso_root' in key:
                    bone['world_fly_root_torso_head']=3

                if 'stretch_length' in key:
                    bone['stretch_length'] = 1.0

                if 'copy_arm_y_rot' in key:
                    bone['copy_arm_y_rot'] = 1.0

                if 'auto_stretch' in key:
                    bone['auto_stretch'] = 0.0

                if 'auto_transform' in key and 'shirt' not in bone.name and 'apron' not in bone.name:
                    bone['auto_transform'] = 0.0

                if 'pin' in key:
                    if 'leg' in key:
                        bone['leg_pin'] = 0.0
                    else:
                        bone['elbow_pin'] = 0.0

                if 'bend_all' in key:
                    bone['bend_all'] = 0.0

                if 'pole_parent' in key:
                    bone['pole_parent'] = 1

                if 'fingers_grasp' in key:
                    bone['fingers_grasp'] = 0.0

                if "arm_spine_rot" in key:
                    bone['arm_spine_rot'] = 1.0

                if "toes_pivot" in key:
                    bone['toes_pivot'] = 0.0

                if "toes_x" in key:
                    bone['toes_x'] = 0.0

                if "toes_y" in key:
                    bone['toes_y'] = 0.0

                if "toes_z" in key:
                    bone['toes_z'] = 0.0

                if "sandal_bend" in key:
                    bone["sandal_bend"]=0.0

                if "foot_roll_z" in key:
                    bone["foot_roll_z"]=0.0

                if "foot_roll_x" in key:
                    bone["foot_roll_x"]=0.0

                if "foot_rot" in key:
                    bone["foot_rot"]=0.0

                if "fly_root_parent" in key:
                    bone["fly_root_parent"]=0

                if "hand_rot_copy" in key:
                    bone["hand_rot_copy"]=0.0

                if "hide_spec" in key:
                    bone["hide_spec"]=0

                if "spec_target" in key:
                    bone["spec_target"]=0.0

                if "world_fly_root_torso_head" in key:
                    bone["world_fly_root_torso_head"]=2



def test_actions():

    class TestActionMainRig(QATest):

        """ Is there an action on the main rig [name_asset_rig] ? """

        name_rig = ""
        armature = None

        def build_context(self, context):
            self.name_rig = context['name_rig_collection']
            self.armature = bpy.data.objects.get(self.name_rig)

            if self.armature is None:
                return False, "Failed to build armature context"

            return True, "Context built"

        def can_fix(self, context):
            return True, "Action will be removed"

        def fix(self, context):
            name = self.armature.animation_data.action.name
            bpy.data.actions.remove(self.armature.animation_data.action)
            reset_all_controllers(self.name_rig)
            return True, "Action [{}] removed from the armature [{}]".format(name, self.name_rig)

        def test(self, context):
            if self.armature.animation_data:
                if self.armature.animation_data.action:
                    return False, "An action named [{}] was found on armature [{}]".format(self.armature.animation_data.action.name, self.name_rig)
                else:
                    return True, "No action found on armature [{}]".format(self.name_rig)
            else:
                return True, "No animation data found on armature [{}]".format(self.name_rig)


    class TestNameActions(QATest):

        """ Tests if no action has a name like [random_name.123] """

        def test(self, context):
            names = []
            regex = ".+\.[0-9]{3}"

            for action in bpy.data.actions:
                res = re.search(regex, action.name)
                if res:
                    names.append("["+res.group(0)+"]")

            if len(names) == 0:
                return True, "All actions have a name looking valid"
            else:
                self.raise_flag('warning')
                return True, "Actions {} have a name betraying a potential problem".format(", ".join(names))

    class TestActionFakeUser(QATest):

        """ Tests if no action is using a Fake User """

        def test(self, context):
            names = []

            for action in bpy.data.actions:
                if action.use_fake_user:
                    names.append("["+action.name+"]")

            if len(names) == 0:
                return True, "No action using a fake user was found in this file"
            else:
                return False, "Actions {} are currently using fake users. They should not".format(", ".join(names))

    class TestActions(QATest):

        """ Performs tests on actions """

        def get_sub_test_types(self):
            return [TestActionMainRig, TestNameActions, TestActionFakeUser]

    return [TestActions]
