from ..sqarf.qatest import QATest
import bpy

from ..utils.general import bfs, tree_from_collection, locate_collection, correct_collection
from .test_geometry import test_geometry
from ..utils.visibility import test_requirements, TestVisibility
from ..chars.test_bfculling_nose import test_bfculling_nose
from ..chars.test_neck_shadow import test_neck_shadow

def test_high_collection():

    class TestGeometryInHigh(QATest):

        """ Is there only geometry inside the [asset_name_high] collection ? """

        desired_name = ""
        name_collection = ""
        high_collection = None
        content = []
        errors = []

        def prerequisites(self, context):
            if bpy.data.collections.get(context['name_high_collection']) is None:
                return False, "[{}] collection not found".format(context['name_high_collection'])

            return True, "Prerequisites met"

        def build_context(self, context):
            self.name_collection = context['name_high_collection']
            self.high_collection = bpy.data.collections.get(self.name_collection)
            try:
                self.content = tree_from_collection(self.high_collection, True)
            except:
                return False, "Failed to build [{}] collection tree".format(self.name_collection)

            return True, "Context built"

        def get_sub_test_types(self):
            return test_geometry()


        def test(self, context):

            if len(self.content) == 0:
                self.raise_flag('warning')
                return True, "The [{}] collection is empty".format(self.name_collection)

            for item in self.content:
                if bpy.data.objects[item].type != 'MESH':
                    if bpy.data.objects[item].type != 'EMPTY':
                        self.errors.append(item)
                    else:
                        if "eye_" not in item:
                            self.errors.append(item)

            if len(self.errors) == 0:
                return True, "[{}] collection contains only geometry"
            else:
                self.raise_flag('warning')
                names = ["["+item+"]" for item in self.errors]
                return True, "Only geometry and empties for eyes should be in the [{}] collection *Found {}".format(self.name_collection, ", ".join(names))




    class TestVisibilityHigh(TestVisibility):
        
        """ Does the collection [asset_name_high] respect visibility and selectability conventions ? """

        requirements = {'hide_viewport': False, 'hide_render': False, 'hide_select': True}
        collection_type = "high"



    class TestHighCollection(QATest):

        """ Is there a [asset_name_high] collection present in the scene ? """

        main_collection = None
        desired_name = ""
        fix_high_collection = ""
        high_collection = None
        flag = ''

        def necessary(self):
            return True


        def prerequisites(self, context):
            if context.get('name_high_collection') is None:
                return False, "Name of [asset_name_high] collection not defined"
            else:
                return True, "Prerequisites met"


        def build_context(self, context):
            self.desired_name = context['name_high_collection']
            self.main_collection = bpy.data.collections.get(context['name_asset'])

            if self.main_collection is None:
                return False, "Failed in building context [{}] collection missing".format(context['name_asset'])

            return True, "Context built"


        def get_sub_test_types(self):
            return [TestGeometryInHigh, TestVisibilityHigh] + test_bfculling_nose() + test_neck_shadow()


        def can_fix(self, context):
            names = ['high', 'geom', 'mesh']

            status, self.high_collection = locate_collection(self.desired_name, names)
            
            if status:
                return True, "Can fix"
            else:
                return False, "Can't fix"


        def fix(self, context):
            old_name = correct_collection(self.desired_name, self.high_collection, self.flag, self.main_collection)
            return True, "[{}] collection is now called [{}] and is child of the [{}] collection".format(old_name, self.desired_name, self.main_collection.name)


        def test(self, context):

            sub_colls = [c.name for c in self.main_collection.children]

            if self.desired_name in sub_colls:
                return True, "High resolution mesh collection [{}] correctly placed and named".format(self.desired_name)
            else:
                return False, "High resolution mesh collection [{}] not found".format(self.desired_name)

    return [TestHighCollection]
