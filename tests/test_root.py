from .sqarf.qatest import QATest
from .sqarf.session import Session
#from .sqarf.html_tree_export import html_tree_export

import os
import sys
import tempfile
import bpy
import importlib
import gazu

from .utils import general as gen_utils
from .HTMLTreeExporter import HtmlTreeExporterCustom

from .common.test_main_collection import test_main_collection
from .common.test_scene_content import test_scene_content
from .common.test_materials import test_materials

from .compo.test_compo_root import test_compo_root

path_export = ""
OPEN_IN_BROWSER = True
data = {}

def fix_space_armature(context):
    name_1 = asset = context['name_asset'] + "_rig"
    name_2 = asset = context['name_asset'] + " _rig"

    obj1 = bpy.data.objects.get(name_1)
    obj2 = bpy.data.objects.get(name_2)
    
    if (obj1 is not None) and (obj2 is not None) and (obj1.type != 'ARMATURE'):
        obj1.name = context['name_asset'] + "_rig_ui"


def test_launch():

    class TestsRoot(QATest):

        """ Are we able to perform tests on this asset ? """

        fixed = []

        def prerequisites(self, context):
            if not context["ok_name"]:
                return False, "Path of file not correct"
            else:
                return True, "Prerequisites met"


        def build_context(self, context):
            asset = context['name_asset']
            context['name_cs_collection']     = asset + "_cs"
            context['name_high_collection']   = asset + "_high"
            context['name_rig_collection']    = asset + "_rig"
            context['name_deform_collection'] = asset + "_deform"
            context['name_low_collection']    = asset + "_low"
            context['name_draft_collection']  = '_TESTING_'
            context['name_texture_picker']    = asset + "_picker"
            context['allowed_collections']    = []

            for key, item in context.to_dict().items():
                if key.startswith("name_") and key.endswith("_collection"):
                    context['allowed_collections'].append(item)

            #fix_space_armature(context)

            return True, "Context built"


        def relevant_for(self, context):
            if context["category"] in ["props", "chars"]:
                return True, "The file passed is a prop or a character"
            else:
                return False, "This file is not a prop or a character"


        def get_sub_test_types(self):
            return test_main_collection() + test_scene_content() + test_materials()

        def can_fix(self, context):
            return True, "Spaces will be removed"

        def fix(self, context):
            for i in range(len(self.fixed)):
                self.fixed[i].name = self.fixed[i].name.lower().replace(' ', '')
                self.fixed[i] = "[{}]".format(self.fixed[i].name)

            return True, "Spaces have been removed from {}".format(", ".join(self.fixed))


        def test(self, context):
            
            for obj in bpy.data.objects:
                if ' ' in obj.name:
                    self.fixed.append(obj)

            for coll in bpy.data.collections:
                if ' ' in coll.name:
                    self.fixed.append(coll)

            if len(self.fixed) == 0:
                return True, ""
            else:
                return False, "Some spaces will be removed"

    return [TestsRoot]



################################################################################

def test_compo_launch():
    return test_compo_root()

################################################################################

def test_root(settings, ope):
    global path_export
    global OPEN_IN_BROWSER
    global data

    if settings['type'] == 'ASSET':
        data = gen_utils.abs_path_to_infos(settings["filepath"])
    else:
        data = gen_utils.infos_from_path_compo(settings["filepath"])
        data['subcategory'] = settings['from_scene']

    OPEN_IN_BROWSER = settings["auto_open"]
    path_export = settings["path_save"]

    session = Session()
    session.set_stop_on_fail(settings["stop_fail"])
    session.set_allow_auto_fix(settings["auto_fix"])
    
    if settings['type'] == 'ASSET':
        session.register_test_types(test_launch())
    else:
        session.register_test_types(test_compo_launch())

    if not data['correct']:
        print("Error in file name")
        return None, None

    addon_prefs = bpy.context.preferences.addons[__name__.split('.')[0]].preferences
    gazu.client.set_host("https://kitsu.thesiren.cloud/api")
    if gazu.client.host_is_up():
        try:
            status = gazu.log_in(addon_prefs.kitsu_uname, addon_prefs.kitsu_pwd)
        except:
            status = {'login': False}
    else:
        ope.report({'ERROR'}, "Kitsu is currently down")
        status = {'login': False}

    if not status['login']:
        ope.report({'ERROR'}, "Failed to connect to Kitsu (check IDs or internet connection)")

    session.context_set(
        name_asset=data['name'],
        category=data['category'],
        type=data['type'],
        ok_name=data['correct'],
        phase=settings['phase'],
        ignored=settings['ignored'],
        subcategory=data['subcategory'],
        kitsu=status,
        prod_id="a56c3c55-7065-42d6-b793-56dc6bf43d59"
    )

    result = session.run()

    if status['login']:
        gazu.log_out()
        settings['kitsu'] = True
    else:
        settings['kitsu'] = False

    return session, result


################################################################################

def test_html_tree_export(session):
    global data
    global path_export
    global OPEN_IN_BROWSER

    #filename = str(tempfile.gettempdir() + os.path.sep + "integrity_report_{}.html".format(data['name']))
    if len(path_export) > 0:
        filename = str(path_export + os.path.sep + "integrity_report_{}.html".format(data['name'].lower().replace(' ', '_')))
    else:
        filename = str(tempfile.gettempdir() + os.path.sep + "integrity_report_{}.html".format(data['name'].lower().replace(' ', '_')))
    print("Exporting to", filename)

    config = data.copy()
    #exporter = test_root.get_exporter("Html Tree", config)
    exporter = session.get_exporter("HTMLTreeCustom", config)
    exporter.export(
        session.to_dict_list(),
        filename,
        allow_overwrite=True,
        also_open_in_browser=OPEN_IN_BROWSER,
    )
