bl_info = {
    "name": "Integrity tester",
    "author": "Les Fées Spéciales (LFS)",
    "version": (1, 67),
    "blender": (2, 91, 0),
    "location": "Properties > Scene > Integrity tester",
    "warning": "",
    "description": "Checks that a file respects all conventions and fixes some issues",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Scene"
}

import os
import sys

import bpy
from bpy.app.handlers import persistent
from .tests.sqarf.qatest import QATest

from .tests.test_root import test_root
from .tests.test_root import test_html_tree_export
from .extracter import get_last_available
from .get_key import get_from_file, get_from_url, get_keys, flush_keys, get_ignored_tests

from . import dance

class IntegrityTesterPreferences(bpy.types.AddonPreferences):
    bl_idname = __name__

    filepath: bpy.props.StringProperty(
        name="Path Key",
        subtype='FILE_PATH',
    )

    url: bpy.props.StringProperty(
        name="URL Key"
    )

    kitsu_uname: bpy.props.StringProperty(
        name="Kitsu user name"
    )

    kitsu_pwd: bpy.props.StringProperty(
        name="Kitsu password",
        subtype='PASSWORD',
        options={'HIDDEN'}
    )

    def draw(self, context):
        layout = self.layout
        layout.label(icon='KEYINGSET', text="Fetch keys from")
        layout.prop(self, "filepath")
        layout.prop(self, "url")
        layout.separator()
        row = layout.row()
        layout.prop(self, "kitsu_uname")
        layout.prop(self, "kitsu_pwd")



################################################################################

class Status():
    def __init__(self):
        self.warnings = set()
        self.result = None
        self.saved = True
        self.kitsu = False

    def add_warning(self, icon, text):
        self.warnings.add((icon, text))

    def set_result(self, result):
        self.result = result

    def reset(self):
        self.warnings.clear()
        self.result = None
        self.saved = True
        self.kitsu = False

################################################################################

status = Status()

def update_key_location(self, context):
    val = bpy.context.scene.integrity_tester.key_from

    if val == "NONE":
        flush_keys()
    elif val == "URL":
        get_from_url()
    elif val == "LOCAL":
        get_from_file()
    else:
        flush_keys()


def key_from_items(self, context):
    liste = []
    liste.append(("NONE", "No key", "Do not use a key", 'KEY_DEHLT', 0))
    liste.append(("URL", "From URL", "Get keys from URL", 'LINK_BLEND', 1))
    liste.append(("LOCAL", "From file", "Get keys from a local file", 'FILE', 2))
    return liste

@persistent
def reset_keys(dummy):
    bpy.context.scene.integrity_tester.key_from = 'NONE'
    bpy.context.scene.integrity_tester.keys = 'NONE'

@persistent
def reset_handler(dummy):
    global status
    status.reset()

@persistent
def check_updates(dummy):
    global status
    ok, version = get_last_available()
    
    if ok:
        if version != bl_info['version']:
            status.add_warning('INFO', "Version {} available".format(version))

def saved():
    return (len(bpy.data.filepath) > 0) and (not bpy.data.is_dirty)

list_steps = [
    ('BASIC', "Basic", "Basic integrity testing", 'VIEWZOOM', 0),
    ('RIG_OK', "Rig OK", "Will this asset be released as rig ok", 'OUTLINER_OB_ARMATURE', 1),
    ('ANIM', "Animation", "Will this asset go in animation right now ?", 'FILE_MOVIE', 2)
]

list_types = [
    ('ASSET', "Asset", "Tests will be performed on an asset (char or prop)", 'CUBE', 0),
    ('SCENE', "Scene", "Tests will be performed on a scene before compositing", 'NODE_COMPOSITING', 1)
]

list_from_scene = [
    ('LAYOUT', "Layout", "The scene to test is from the layout", 'COMMUNITY', 0),
    ('ANIM', "Animation", "The scene to test is from the animation", 'FILE_MOVIE', 1)
]

class SetupIntegrityTester(bpy.types.PropertyGroup):
    open_browser_auto : bpy.props.BoolProperty(name="Open report in browser", default=True, description="Should the report be opened automatically in your browser")
    stop_failure      : bpy.props.BoolProperty(name="Stop after failure", default=False, description="If False, the addon may crash if too many failures occur on the same branch", options={'SKIP_SAVE'})
    report_path       : bpy.props.StringProperty(name="Report path", default="", subtype='DIR_PATH', description="Where the HTML report will be stored. Leave empty to save it in a temp file", options={'SKIP_SAVE'})
    from_scene        : bpy.props.EnumProperty(name='From Scene', description="What is this scene that goes to compositing", items=list_from_scene, default=0)
    type_test         : bpy.props.EnumProperty(name='Type Test', description="What the test will be performed on", items=list_types, default=0)
    key_from          : bpy.props.EnumProperty(name='Key From', description='Source of the keys', items=key_from_items, default=0, update=update_key_location, options={'SKIP_SAVE'})
    auto_fix          : bpy.props.BoolProperty(name="Auto-fix", default=True, description="If active, the tester will try to fix the problems it finds instead of just giving an error", options={'SKIP_SAVE'})
    phase             : bpy.props.EnumProperty(items=list_steps, name="Phase", default=2, description="What will this asset be used for ?")
    keys              : bpy.props.EnumProperty(name='Key', description='Key to use', items=get_keys, default=0, update=None, options={'SKIP_SAVE'})


class IntegrityTesterOperator(bpy.types.Operator):
    """Checks and fixes a scene according conventions"""
    bl_idname = "scene.integrity_tester"
    bl_label = "File Integrity Tester"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        # The file has to exist on the disk and must be saved
        return saved()


    def invoke(self, context, event):
        global status

        status = Status()

        return self.execute(context)

    def execute(self, context):
        global status

        dico_ignored = get_ignored_tests()

        key = bpy.context.scene.integrity_tester.keys
        ignored = []

        if (key != 'NONE') and (key in dico_ignored.keys()):
            ignored = dico_ignored[key]

        settings = {
            "filepath"  : bpy.data.filepath,
            "auto_fix"  : bpy.context.scene.integrity_tester.auto_fix,
            "auto_open" : bpy.context.scene.integrity_tester.open_browser_auto,
            "path_save" : bpy.path.abspath(bpy.context.scene.integrity_tester.report_path),
            "stop_fail" : bpy.context.scene.integrity_tester.stop_failure,
            "type"      : bpy.context.scene.integrity_tester.type_test,
            "from_scene": bpy.context.scene.integrity_tester.from_scene,
            "ignored"   : ignored,
        }

        if bpy.context.scene.integrity_tester.type_test == 'ASSET':
            settings.update({"phase": bpy.context.scene.integrity_tester.phase})
        else:
            settings.update({"phase": bpy.context.scene.integrity_tester.from_scene})

        session, result = test_root(settings, self)
        
        status.kitsu = settings['kitsu']

        if result is not None:
            if result:
                self.report({'INFO'}, "All tests passed or were fixed")
            else:
                self.report({'ERROR'}, "At least one test failed")

            status.set_result(result)
            status.saved = False
            test_html_tree_export(session)
        
        else:
            self.report({'ERROR'}, "Testing session not launched, verify file location")
            return {'CANCELLED'}


        return {'FINISHED'}


class OverrideSaveOperator(bpy.types.Operator):

    """ Save file and reset sanity check context """

    bl_label = "Save the scene"
    bl_idname = "wm.save_mainfile_after"

    def execute(self, context):
        bpy.ops.wm.save_mainfile()
        global status
        status.saved = True
        return {'FINISHED'}


class RefreshKeysOperator(bpy.types.Operator):

    """ Refresh keys list """

    bl_label = "Refresh list of keys"
    bl_idname = "scene.refresh_keys_integrity_tester"

    def execute(self, context):
        val = bpy.context.scene.integrity_tester.key_from

        if val == "NONE":
            flush_keys()
        elif val == "URL":
            get_from_url()
        elif val == "LOCAL":
            get_from_file()
        else:
            flush_keys()

        return {'FINISHED'}


################################################################################

class IntegrityTesterPanel(bpy.types.Panel):

    """ Integrity tester panel """

    bl_label = "Integrity Tester"
    bl_idname = "SCENE_PT_integrity_tester"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"

    def draw(self, context):

        global status
        for item in status.warnings:
            row = self.layout.row()
            row.alert = True
            row.label(icon=item[0], text=item[1])
            

        col = self.layout.column()
        col.prop(bpy.context.scene.integrity_tester, "open_browser_auto")
        col.prop(bpy.context.scene.integrity_tester, "auto_fix")
        col.prop(bpy.context.scene.integrity_tester, "stop_failure")

        row = self.layout.row()
        row.alignment = 'EXPAND'
        row.prop(bpy.context.scene.integrity_tester, "type_test", expand=True)

        self.layout.separator()

        row = self.layout.row()
        row.alignment = 'EXPAND'

        if bpy.context.scene.integrity_tester.type_test == 'ASSET':
            row.prop(bpy.context.scene.integrity_tester, "phase", expand=True)
        else:
            row.prop(bpy.context.scene.integrity_tester, "from_scene", expand=True)

        self.layout.separator()


        if(not bpy.context.scene.integrity_tester.auto_fix) and (not bpy.context.scene.integrity_tester.stop_failure):
            row = self.layout.row()
            row.enabled = False
            row.label(text="(Stop after failure) recommended when (Auto-fix) is off")

        if not bpy.context.scene.integrity_tester.open_browser_auto:
            row = self.layout.row()
            row.prop(bpy.context.scene.integrity_tester, "report_path")
        
        if not saved():
            row = self.layout.row()
            row.enabled = False
            row.label(text="Verify that your file is saved", icon='ERROR')

        try:
            
            if QATest.version != 2:
                raise Exception
        except:
            sqarf_ok = False
        else:
            sqarf_ok = True

        row = self.layout.row()
        if sqarf_ok:
            row.operator("scene.integrity_tester", icon='PREFERENCES')
        else:
            row.alert = True
            row.label(icon='ERROR', text="SQARF is not up to date")

        if not status.saved:
            row = self.layout.row()
            row.operator('wm.save_mainfile_after', text='SAVE', icon='DISK_DRIVE')

        if status.result != None:
            if not status.kitsu:
                row = self.layout.row()
                row.alert = True
                row.label(icon='ERROR', text="Failed to connect to Kitsu")

            row = self.layout.row()
            
            if status.result.failed():
                row.alert = True
                row.label(icon='KEYTYPE_EXTREME_VEC', text="At least one test failed, please check your file")
            else:
                row.active = True
                row.label(icon='FUND', text="All tests passed successfully!")

        self.layout.separator()


        row = self.layout.label(icon='KEYINGSET', text="Fetch keys from:")
        row = self.layout.row()
        row.prop(bpy.context.scene.integrity_tester, "key_from", expand=True)
        row.operator('scene.refresh_keys_integrity_tester', text="", icon='FILE_REFRESH')

        if bpy.context.scene.integrity_tester.key_from != "NONE":
            row = self.layout.row()
            row.alignment = 'EXPAND'
            row.prop(bpy.context.scene.integrity_tester, "keys", text="")



################################################################################



def register():
    bpy.utils.register_class(SetupIntegrityTester)
    bpy.types.Scene.integrity_tester = bpy.props.PointerProperty(type=SetupIntegrityTester)
    bpy.utils.register_class(IntegrityTesterOperator)
    bpy.utils.register_class(RefreshKeysOperator)
    bpy.utils.register_class(IntegrityTesterPanel)
    bpy.utils.register_class(OverrideSaveOperator)
    bpy.utils.register_class(IntegrityTesterPreferences)
    bpy.app.handlers.load_post.append(reset_keys)
    bpy.app.handlers.load_post.append(reset_handler)
    bpy.app.handlers.load_post.append(check_updates)
    bpy.app.handlers.save_pre.append(reset_handler)
    bpy.app.handlers.save_post.append(check_updates)

    dance.register()

def unregister():
    dance.unregister()

    bpy.app.handlers.load_post.remove(reset_keys)
    bpy.app.handlers.load_post.remove(reset_handler)
    bpy.app.handlers.load_post.remove(check_updates)
    bpy.app.handlers.save_pre.remove(reset_handler)
    bpy.app.handlers.save_post.remove(check_updates)
    bpy.utils.unregister_class(IntegrityTesterPreferences)
    bpy.utils.unregister_class(OverrideSaveOperator)
    bpy.utils.unregister_class(IntegrityTesterPanel)
    bpy.utils.unregister_class(RefreshKeysOperator)
    bpy.utils.unregister_class(IntegrityTesterOperator)
    del bpy.types.Scene.integrity_tester
    bpy.utils.unregister_class(SetupIntegrityTester)

if __name__ == "__main__":
    register()
