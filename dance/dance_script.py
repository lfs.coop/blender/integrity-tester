import bpy
import os.path
from mathutils import Vector, Quaternion
import sys

action_path = os.path.join(os.path.dirname(__file__), 'lib_chars_utils_misc_check_anim.blend')
blendFile = sys.argv[3].split('!')[1]

# Cleaning the scene
for obj in bpy.data.objects[:]:
    bpy.data.objects.remove(obj)
master_collection = bpy.context.scene.collection

# Importing the asset to test as a linked library
with bpy.data.libraries.load(blendFile, link=True, relative=True) as (data_from, data_to):
    data_to.collections = data_from.collections

# Calculating which is the master collection in that file (with set operation, the single left collection is the master)
# The only collection that is child of nobody is the master
children = []
for new_coll in data_to.collections:
    children.extend(list(new_coll.children))

children = set(children)
all_colls = set(data_to.collections)

try:
    main = list(all_colls - children)[0]
except:
    print("DEBUG", "Impossible to locate the asset's master collection")
    sys.exit(1)

# Creating an instance of the main collection from the library and linking it
instance = bpy.data.objects.new(main.name, None)
instance.instance_type = 'COLLECTION'
instance.instance_collection = main
master_collection.objects.link(instance)
bpy.context.view_layer.objects.active = instance

# Making a proxy armature of the the new asset's instance
try:
    bpy.ops.object.proxy_make(object=instance.name + "_rig")
except:
    sys.exit(1)

# Loading the dance
with bpy.data.libraries.load(action_path, link=True,
                             relative=True) as (data_from, data_to):
    data_to.actions = data_from.actions

test_action = data_to.actions[0]

# Find the armature object
arm = None
for obj in bpy.data.objects:
    if obj.type == 'ARMATURE' and obj.name.endswith('_proxy'):
        arm = obj
        break

if arm is None:
    print('DEBUG', "Unable to find the proxy armature")
    sys.exit()

# Setup scene
bpy.context.scene.frame_start, bpy.context.scene.frame_end = test_action.frame_range
bpy.context.scene.frame_set(bpy.context.scene.frame_start)
bpy.context.scene.render.use_simplify = True
bpy.context.scene.render.simplify_subdivision = 0

# Setup camera
r3d = None
for area in bpy.context.screen.areas:
    if area.type == 'VIEW_3D':
        r3d = area.spaces[0].region_3d
        r3d.view_perspective = 'PERSP'
        r3d.view_distance = 4.1
        r3d.view_location = Vector((-0.13015997409820557, -0.0011222660541534424, 1.2422499656677246))
        r3d.view_rotation = Quaternion((0.7621176838874817, 0.5257500410079956, -0.21455451846122742, -0.311014324426651))
        break

# Apply dance
anim_data = arm.animation_data_create()
anim_data.action = test_action

# Frame armature and run animation
bpy.ops.screen.animation_play()
