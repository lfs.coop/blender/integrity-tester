import bpy
from time import time
import os.path


class LFS_OT_IntegrityTesterRunDance(bpy.types.Operator):
    """Open new Blender instance and apply the dance to the character"""
    bl_idname = "lfs.integrity_tester_run_dance"
    bl_label = "Let’s Dance"

    def execute(self, context):
        if not bpy.data.filepath:
            self.report({'WARNING'}, "File not saved?")
            return {'CANCELLED'}
        print("Let’s dance")

        import subprocess
        dance_script_path = os.path.join(os.path.dirname(__file__), 'dance_script.py')
        dance_props_script_path = os.path.join(os.path.dirname(__file__), 'dance_props_script.py')

        s_path = bpy.path.abspath('//').replace('\\', '/').split('/')
        chars = s_path.count('chars') > 0
        props = s_path.count('props') > 0
        path = ""

        if (chars == props):
            return {'CANCELLED'}
        elif chars:
            path = dance_script_path
        else:
            path = dance_props_script_path

        # Ripped off from object_edit_linked.py
        subprocess.Popen([bpy.app.binary_path, "--python", path, "path!"+bpy.data.filepath])

        return {'FINISHED'}


class LFS_PT_IntegrityTesterDancePanel(bpy.types.Panel):
    bl_label = "Dance"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"
    bl_parent_id = "SCENE_PT_integrity_tester"

    def draw(self, context):
        layout = self.layout

        t = time() % 1
        if 0.0 <= t < 0.33:
            icon = "OUTLINER_DATA_ARMATURE"
        elif 0.33 <= t < 0.66:
            icon = "ARMATURE_DATA"
        else:
            icon = "MOD_ARMATURE"

        layout.operator(LFS_OT_IntegrityTesterRunDance.bl_idname, icon=icon)

def register():
    bpy.utils.register_class(LFS_OT_IntegrityTesterRunDance)
    bpy.utils.register_class(LFS_PT_IntegrityTesterDancePanel)


def unregister():
    bpy.utils.unregister_class(LFS_OT_IntegrityTesterRunDance)
    bpy.utils.unregister_class(LFS_PT_IntegrityTesterDancePanel)
