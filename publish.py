#!/usr/bin/env python3

import sys
import os

last_version = (1, 67)

def next_minor_version(v):
	v2 = (v[0], v[1]+1)
	return v2

def next_major_version(v):
	v2 = (v[0]+1, v[1])
	return v2

def update_version():
	global last_version
	new_version = next_minor_version(last_version)
	print("Version: ", last_version, "->", new_version)

	bl_info_version = '"version": {}'.format(last_version)
	versionner      = "last_version = {}".format(last_version)
	html_version    = 'version = {}'.format(last_version)

	new_bl_info_version = '"version": {}'.format(new_version)
	new_versionner      = "last_version = {}".format(new_version)
	new_html_version    = 'version = {}'.format(new_version)

	# PUBLISH.PY

	file = open("publish.py", 'r')
	content = file.read()
	file.close()
	content = content.replace(versionner, new_versionner)

	file = open("publish.py", 'w')
	file.write(content)
	file.close()

	# HTMLTREEEXPORTER.PY

	file = open("tests/HTMLTreeExporter.py", 'r')
	content = file.read()
	file.close()
	content = content.replace(html_version, new_html_version)

	file = open("tests/HTMLTreeExporter.py", 'w')
	file.write(content)
	file.close()

	# __INIT__.PY

	file = open("__init__.py", 'r')
	content = file.read()
	file.close()
	content = content.replace(bl_info_version, new_bl_info_version)

	file = open("__init__.py", 'w')
	file.write(content)
	file.close()


update_version()

# if len(sys.argv) < 3:
# 	print(sys.argv[0], " expects a Gitlab message and a branch")
# elif len(sys.argv) > 3:
# 	print(sys.argv[0], " expects a Gitlab message and a branch")
# else:
# 	msg = sys.argv[1]
# 	branch = sys.argv[2]

# 	update_version()

# 	os.system("git add .")
# 	os.system('git commit -m "{}"'.format(msg))
# 	os.system("git push origin {}".format(branch))
