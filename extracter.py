import re
import requests

path = "/tmp/integrity_report_ext_wm_midaged_chador.html"
regex_version = "<div id=\"version\-bl\">(\(.+\))<\/div>"
regex_user = "<div id=\"user\".+>(.+)<\/div>"
regex_tests = "<span class=\"title (.+)\-title\">(.+)<\/span>"
regex_bl_version = "\"version\": (\(.+\))"

keys = {
	'url_key_test': "https://gitlab.com/lfs.coop/blender/integrity-tester/-/raw/dev/key.py"
}

url_init = "https://gitlab.com/lfs.coop/blender/integrity-tester/-/raw/master/__init__.py"


def get_last_available():
	global url_init
	try:
		raw = requests.get(url_init).text
	except:
		return False, None

	match = re.search(regex_bl_version, raw)
	if match:
		literal_version = match[1]
		try:
			version = eval(literal_version)
		except:
			return False, None
		else:
			return True, version
	else:
		return False, None


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#      FUNCTION THAT EXTRACTS THE USER RELATIVE DATA FROM THE REPORT                  #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def meta_data(content):
	user = "Not found"
	version = (-1, -1)

	match = re.search(regex_version, content)

	if match:
		version = eval(match[1])
		print(version)

	match = re.search(regex_user, content)

	if match:
		user = match[1]
		print(user)

	return {'user': user, 'version': version}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#      FUNCTION THAT EXTRACTS TESTS RESULTS FROM THE REPORT                           #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def extract_status(content):
	match = re.findall(regex_tests, content)
	return match


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#      FUNCTION THAT COMPARES A KEY TO A STATUS TO CONFIRM VALIDITY                   #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def compare_key_status(key_name, status):
	global keys
	try:
		raw = requests.get(keys[key_name]).text
	except:
		return False, "Impossible to access the key {}".format(key_name)
	
	key_reference = set(eval(raw))
	#print(key_reference)

	for result, test in status:
		if test in key_reference:
			if result == 'passed':
				key_reference.remove(test)

	#print(key_reference)
	if len(key_reference) == 0:
		return True, []
	else:
		return False, key_reference

		


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#      MISCELLANEOUS TESTTING - TO BE DELETED                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""
file = open(path, 'r')
content = file.read()
file.close()

meta_data = meta_data(content)
result = extract_status(content)
succes, tests = compare_key_status('url_key_test', result)

if succes:
	print("OK.")
else:
	print("Some tests failed but must pass for the file to be valid:", tests)
"""
get_last_available()