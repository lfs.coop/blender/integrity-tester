# Scene Integrity Tester

![Dependencies of tests](misc/diagram-of-dependencies.png )

## What is it?
Scene Integrity Tester works just as a unit-test framework. It controls the validity of a Blender scene according the desired production conventions, and is able to fix some issues by itself. An HTML report is generated to get a visually clear and appealing assessment of the file's state.

![Dependencies of tests](misc/screen_report.png)

### Notes:
- This functionality takes the form of a Blender add-on, scanning the scene and generating an HTML file report.
- Dependencies: `sqarf`
- If an error occurs, please send the HTML file generated or a screenshot of the terminal on The Siren chat or on Gitlab

## How does it work?
### Prerequisites:
- The `.blend` file that you are testing must exist on the disk and **must be saved**.
- The `.blend` file must be in the correct directory (`[*]/lib/props/category/name_asset/...`)
- The `.blend` file must be correctly named

### In Blender:
- After the add-on was activated, its menu is located in the Scene properties panel.
- The file must be saved for the test to be fired.
- This add-on is possibly **destructive** *(it modifies your scene)*, the output is not saved while you don't press the Blender save button manually. So don't save the output while you didn't check it carefully.
- When the auto-fix is activated, the add-on will try to fix things given the desired conventions. However, consider checking what's been fixed just as if it was an error.

![Panel of Integrity Tester](misc/screen_panel.png)
![Panel of Integrity Tester](misc/screen_panel_1.png)

### Buttons:
- **Open report in browser**: If checked, the HTML file generated will automatically pop in your browser. Otherwise, if you keep the box unchecked, you will certainly want to fill the **Report path** field to save the report somewhere on your disk.
- **Auto-fix**: As previously mentioned, this addon is able to fix some issues by itself. Since your file has to be saved to launch this program, it is *strongly recommended* to keep that box checked to see what changes would be applied to the file. If these tweaks don't satisfy you, you can simply re-open the file.
- **Stop after first failure**: If you unchecked the **Auto-fix** box, we recommend to check that box. Since this testing system is shaped as a tree, keeping tests running after a fail without the auto-fix on might cause the program to crash on the remaining part of the branch.
- **Phase**: You have to choose the phase of the production. There are three possibilities (Basic, Rig OK & Animation). In Basic, the minimal amount of tests is performed. In Rig OK, stuff like lights or camera are forbidden. In animation, the presence of `rig_ui` and `c_fist` is mandatory.


### The report:
- By clicking on the eye in the report header, you can show all the tests performed, even the succeeded ones.
- If the box is checked, the report will show up automatically in your favorite web browser. Otherwise, it will be saved at the chosen path (the temporary directory by default)
- Each item of the report **is clickable**. Clicking will display very **useful details** about what's been found in the file, to help you fixing your file.
- **Green** tests are passed, it's the best that can happen.
- **Blue** tests are problems that have been automatically fixed.
- **Red** tests are failed, you have to fix them manually
- **Orange** tests are simply warnings, they are not errors, but consider reading what's told in the details.
- The testing system is shaped as a tree, so your failed tests will often have as details message "Sub test failed". It simply means that a test lower on the branch has failed, and it made the whole branch be considered as failed until the root. To get the actual error, simply scroll down and look for a failed test more **shifted to the right**.

### Basic session:
0. Save your file (at the correct location)
1. Without modifying the default settings, click the button "Integrity Tester"
2. Check the report in your browser (the size of this report might change, some tests are crucial and will prevent other tests to run if they failed)
3. If the fixes made by the addon are wrong, re-open your file.
4. Click on **RED**, **BLUE** and **ORANGE** lines to get details.
5. Fix your scene according what you've seen in the report.
6. Save your scene & relaunch the test.

Repeat these steps until everything comes out green.

### Keys
- A key can be used (from a URL or a local file). These keys enable to skip some tests.

